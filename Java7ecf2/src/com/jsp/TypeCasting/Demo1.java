package com.jsp.TypeCasting;

class Circle
{
	int radius;

	public Circle(int r)
	{
		radius = r;
	}

	@Override
	public String toString() {
		return "Circle[radius="+radius+"]";
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Circle)) return false;
		return radius==((Circle)obj).radius;
	}
	
	
	
}


public class Demo1 {

	public static void main(String[] args) 
	{
		Circle c1=new Circle(10);
		Circle c2=new Circle(10);
		System.out.println(c1);
		System.out.println(c1==c2);
		System.out.println(c1.equals(c2));
	}

}
