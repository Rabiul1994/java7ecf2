package com.jsp.TypeCasting;

class Rectangle
{
	int length; int breadth;


	public Rectangle(int length, int breadth) {
		this.length = length;
		this.breadth = breadth;
	}
	
	@Override
	public boolean equals(Object arg)
	{
		if(!(arg instanceof Rectangle)) return false;
		Rectangle r=(Rectangle)arg;
		return length==r.length && breadth==r.breadth;
	}
	
	
}


public class Demo3 {

	public static void main(String[] args) 
	{
		Rectangle r1=new Rectangle(10, 30);
		Rectangle r2=new Rectangle(10, 30);
		System.out.println(r1.equals(r2));
		System.out.println(r2.equals(r1));
	}

}
