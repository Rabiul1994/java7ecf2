package com.jsp.DataStructure;

class Node2
{
	int key;
	Node2 next;
	public Node2(int key, Node2 next) {
		this.key = key;
		this.next = next;
	}
}

class MyHashSet
{
	private Node2[] a=new Node2[15];
	private int count=0;
	
	public boolean add(int e)
	{
		int index=e%a.length;
		if(a[index]==null)
		{
			a[index]=new Node2(e,null);
			count++;
			return true;
		}
		Node2 prev=null;
		Node2 curr=a[index];
		while(curr!=null)
		{
			if(curr.key==e) return false;
			prev=curr;
			curr=curr.next;
		}
		prev.next=new Node2(e,null);
		count++;
		return true;
	}
	
	public void display()
	{
		for(int i=0;i<a.length;i++)
		{
			Node2 curr=a[i];
			while(curr!=null)
			{
				System.out.println(curr.key);
				curr=curr.next;
			}
		}
	}
	public int size() { return count; }
}

public class TestMyHashSet
{
	public static void main(String[] args) 
	{
		MyHashSet s1=new MyHashSet();
		s1.add(20);
		s1.add(34);
		s1.add(14);
		s1.add(33);
		s1.add(20);
		s1.add(14);
		
		s1.display();
		System.out.println("Size="+s1.size());
	}
	
}


