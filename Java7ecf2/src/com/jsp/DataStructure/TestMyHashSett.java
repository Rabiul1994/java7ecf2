package com.jsp.DataStructure;

class Node3
{
	Object key;
	Node3 next;
	
	public Node3(Object k, Node3 next) {
		this.key = k;
		this.next = next;
	}
	
}

class MyHashSett
{
	private Node3[] a=new Node3[15];
	private int count=0;
	
	public boolean add(Object e)
	{
		int index=Math.abs(e.hashCode())%a.length;
		if(a[index]==null)
		{
			a[index]=new Node3(e,null);
			count++;
			return true;
		}
		Node3 prev=null;
		Node3 curr=a[index];
		while(curr!=null)
		{
			if(curr.key.equals(e)) return false;
			prev=curr;
			curr=curr.next;
		}
		prev.next=new Node3(e,null);
		count++;
		return true;
	}
	
	public void display()
	{
		for(int i=0;i<a.length;i++)
		{
			Node3 curr=a[i];
			while(curr!=null)
			{
				System.out.print("["+curr.key+"]");
				curr=curr.next;
			}
		}
		System.out.println();
	}
	public int size() { return count; }
	
	
	
}

public class TestMyHashSett {

	public static void main(String[] args) {
		MyHashSett s1=new MyHashSett();
		s1.add("rabi");
		s1.add("rakib");
		s1.add("saddam");
		s1.add("rocky");
		s1.add("rakib");
		s1.add("raja");
		
//		String s="saddam";
//		System.out.println(Math.abs(s.hashCode()));
		
		s1.display();
		
		System.out.println("Size="+s1.size());
		
		

	}

}
