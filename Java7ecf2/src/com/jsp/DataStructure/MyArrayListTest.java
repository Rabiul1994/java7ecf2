package com.jsp.DataStructure;

class MyArrayList
{
	private Object[] a;
	private int p;
	
	public MyArrayList() 
	{
		this.a = new Object[5];
		this.p = 0;
	}
	
	public void add(Object e)
	{
		if(p>=a.length) increaseCapacity();
		a[p]=e;
		p++;
	}
	
	public int size() {return p;}
	
	public Object get(int index)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		
		return a[index];
	}
	
	public void add(int index,Object e)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		if(p>=a.length) increaseCapacity();
		
		for(int i=size()-1;i>=index;i--)
		{
			a[i+1]=a[i];
		}
		a[index]=e;
		p++;
	}
	
	public void remove(int index)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		for(int i=index;i<size()-1;i++)
		{
			a[i]=a[i+1];
		}
		p--;
		a[p]=null;
		
	}
	
	public void clear()
	{
		a=new Object[5];
		p=0;
	}
	
	public void reverse()
	{
		Object[] temp=new Object[size()];
		int c=0;
		for(int i=size()-1;i>=0;i--)
		{
			temp[c]=a[i];
			c++;
		}
		a=temp;
	}

	
	private void increaseCapacity()
	{
		Object[] temp=new Object[a.length+3];
		for(int i=0;i<a.length;i++) 
		{
			temp[i]=a[i];
		}
		a=temp;
	}
	
	
}


public class MyArrayListTest {

	public static void main(String[] args) 
	{
		MyArrayList l1=new MyArrayList();
		l1.add(10);
		l1.add(20);
		l1.add(30);
		l1.add(40);
		l1.add(1, 70);
		System.out.println(l1.get(1)); // 20
		System.out.println("size:="+l1.size()); // 5
		for(int i=0;i<l1.size();i++)
		{
			System.out.print(l1.get(i)+" ");
		}
		System.out.println();
		System.out.println("after removing");
		l1.remove(2);
		System.out.println("size:="+l1.size());
		for(int i=0;i<l1.size();i++)
		{
			System.out.print(l1.get(i)+" ");
		}
		System.out.println();
		l1.reverse();
		for(int i=0;i<l1.size();i++)
		{
			System.out.print(l1.get(i)+" ");
		}
		System.out.println();
		System.out.println(l1.get(0));
		
//		l1.clear();
//		l1.add(10);
//		System.out.println(l1.get(0));
	}
		
		
}
