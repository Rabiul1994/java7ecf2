package com.jsp.DataStructure;

import java.util.*;

public class RemoveDupFromArrayList {

	public static void main(String[] args) 
	{
		
		ArrayList<Integer> a1=new ArrayList<>();
		
		a1.add(20);
		a1.add(10);
		a1.add(20);
		a1.add(30);
		a1.add(40);
		a1.add(80);
		
		System.out.println("Original List is:"+a1);
		
		Set s1=new LinkedHashSet<Integer>(a1);
		
		a1.clear();
		
		a1.addAll(s1);
		
		System.out.println("After duplicate removal:"+a1);
		

	}

}
