package com.jsp.DataStructure;

public class Student 
{
	String name;
	int marks;
	public Student(String name, int marks) {
		this.name = name;
		this.marks = marks;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", marks=" + marks + "]";
	}
	
	@Override
	public int hashCode() {
		return marks;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Student)) return false;
		Student s=(Student)obj;
		return name.equalsIgnoreCase(s.name) && marks==s.marks;
	}
	
	
}
