package com.jsp.DataStructure;

class Node
{
	Object ele;
	Node next;
	
	public Node(Object e,Node n)
	{
		ele=e;
		next=n;
	}
}

class MyLinkedList
{
	private Node first;
	private int count;
	
	public void add(Object e)
	{
		if(first==null)
		{
			first=new Node(e,null);
			count++;
			return;
		}
		Node curr=first;
		while(curr.next!=null)
		{
			curr=curr.next;
		}
		curr.next=new Node(e,null);
		count++;
	}
	
	public int size() {return count;}
	
	public Object get(int index)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		Node curr=first;
		for(int i=1;i<=index;i++) { curr=curr.next;}
		return curr.ele;
	}
	
	public void add(int index,Object e)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		if(index==0)
		{
			first=new Node(e,first);
			count++;
			return;
		}
		Node curr=first;
		for(int i=1;i<index;i++)
		{
			curr=curr.next;
		}
		curr.next=new Node(e,curr.next);
		count++;
	}
	
	public void remove(int index)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		if(index==0)
		{
			first=first.next;
			count--;
			return;
		}
		Node curr=first;
		for(int i=1;i<index;i++)
		{
			curr=curr.next;
		}
		curr.next=curr.next.next;
		count--;
	}
	
	public void reverse()
	{
		Node prev=null, curr=first, next=null;
		while(curr!=null)
		{
			next=curr.next;
			curr.next=prev;
			prev=curr;
			curr=next;
		}
		first=prev;
	}
	
	public void clear()
	{
		first=null;
		count=0;
	}
}

public class MyLinkedListTest {

	public static void main(String[] args)
	{
		MyLinkedList l1=new MyLinkedList();
		l1.add(10);
		l1.add(10);
		l1.add(20);
		l1.add(20);
		l1.add(20);
		l1.add(10);
		
		Object[] a=new Object[l1.size()];
		for(int i=0;i<l1.size();i++)
		{
			a[i]=l1.get(i);
		}
		l1.reverse();
		int m=0; int n=a.length-1; boolean flag=true;
		while(m<=n)
		{
			if(a[m]!=l1.get(m))
			{
				System.out.println("Not Palindrome");
				flag=false;
			}
			m++;
			n--;
		}
		if(flag) System.out.println("Palindrome");
	}
}
