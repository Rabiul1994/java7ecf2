package com.jsp.DataStructure;

import java.util.EmptyStackException;

class MyArrayStack
{
	private Object[] a;
	private int p;
	
	public MyArrayStack()
	{
		a=new Object[5];
		p=0;
	}
	
	public void push(Object e)
	{
		if(p>=a.length) increaseCapacity();
		a[p++]=e;
	}
	
	public int size() { return p; }
	
	public Object peek()
	{
		if(p==0) return null;
		return a[p-1];
	}
	
	public Object pop()
	{
		if(p==0) throw new EmptyStackException();
		Object temp=a[p-1];
		a[p-1]=null;
		p--;
		return temp;
	}
	
	public boolean isEmpty()
	{
		if(size()==0) return true;
		return false;
	}
	
	private void increaseCapacity()
	{
		Object[] temp=new Object[a.length+3];
		for(int i=0;i<a.length;i++) 
		{
			temp[i]=a[i];
		}
		a=temp;
	}
}


public class TestMyArrayStack {

	public static void main(String[] args) 
	{
		MyArrayStack s=new MyArrayStack();
		s.push(10);
		s.push(20);
		s.push(40);
		while(s.peek()!=null) {
		System.out.println(s.pop());
		}
	}

}
