package com.jsp.DataStructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CarList {

	public static void main(String[] args) {

		Car c1=new Car("Swift",500000);
		Car c2=new Car("BMW",5000000);
		Car c3=new Car("Hundai",600000);
		Car c4=new Car("Tata",400000);
		Car c5=new Car("Maruti",300000);
		
		ArrayList<Car> a1=new ArrayList<Car>();
		
		a1.add(c1);
		a1.add(c2);
		a1.add(c3);
		a1.add(c4);
		a1.add(c5);
		
		System.out.println(a1);
		
		Collections.sort(a1);
		System.out.println(a1);

	}

}
