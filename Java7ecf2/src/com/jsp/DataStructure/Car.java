package com.jsp.DataStructure;

public class Car implements Comparable<Car>
{
	String name;
	int price;
	
	public Car(String name, int price) {
		this.name = name;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [name=" + name + ", price=" + price + "]";
	}

	@Override
	public int compareTo(Car o) {
		return name.compareTo(o.name);
	}
	
	
	
	
}
