package com.jsp.DataStructure;

class MyLinkedQueue
{
	private Node first;
	private int count;
	
	public void add(Object e)
	{
		if(first==null)
		{
			first=new Node(e,null);
			count++;
			return;
		}
		Node curr=first;
		while(curr.next!=null)
		{
			curr=curr.next;
		}
		curr.next=new Node(e,null);
		count++;
	}
	
	public int size() { return count; }
	
	public Object peek()
	{
		if(first==null) return null;
		return first.ele;
	}
	
	public Object poll()
	{
		Object temp=first.ele;
		if(first==null) return null;
		first=first.next;
		count--;
		return temp;
	}
	
}


public class TestMyLinkedQueue {

	public static void main(String[] args)
	{
		MyLinkedQueue q=new MyLinkedQueue();
		q.add(10);
		q.add(20);
		q.add(30);
		q.add(50);
		System.out.println(q.size());
		while(q.peek()!=null)
		{
			System.out.println(q.poll());
		}
		System.out.println(q.size());
	}

}
