package com.jsp.DataStructure;

import java.util.EmptyStackException;

class MyLinkedStack
{
	private Node first;
	private int count;
	
	public void push(Object e)
	{
		if(first==null)
		{
			first=new Node(e,null);
			count++;
			return;
		}
		Node curr=first;
		while(curr.next!=null)
		{
			curr=curr.next;
		}
		curr.next=new Node(e,null);
		count++;
	}
	
	public int size() { return count; }
	
	public Object peek()
	{
		if(first==null) return null;
		Node curr=first;
		while(curr.next!=null)
		{
			curr=curr.next;
		}
		return curr.ele;
	}
	
	public Object pop()
	{
		if(first==null) return new EmptyStackException();
		Node curr=first;
		while(curr.next!=null)
		{
			curr=curr.next;
		}
		Object temp=curr.ele;
		curr=first;
		for(int i=1;i<size()-1;i++)
		{
			curr=curr.next;
		}
		curr.next=null;
		count--;
		if(count==0) first=first.next;
		return temp;
	}
	
	public boolean isEmpty()
	{
		if(size()==0) return true;
		return false;
	}
}


public class TestMyLinkedStack {

	public static void main(String[] args) 
	{
		MyLinkedStack s=new MyLinkedStack();
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		while(s.peek()!=null)
		{
			System.out.println(s.pop());
		}
	}

}
