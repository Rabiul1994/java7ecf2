package com.jsp.DataStructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReverseArrayList {

	public static void main(String[] args) 
	{
		List<Integer> a1=new ArrayList<Integer>();
		
		a1.add(20);
		a1.add(10);
		a1.add(20);
		a1.add(30);
		a1.add(40);
		a1.add(80);
		
		List<Integer> a2= a1.subList(2, 5);
		System.out.println(a2);
		
		int arr[]=new int[a1.size()];
		int i=0;
		int n=a1.size()-1;
		
		while(n>=0)
		{
			arr[i]=a1.get(n);
			i++;
			n--;
			
		}
		for(int a:arr) System.out.print(a+" ");
		
		
		
		
		
//		Collections.reverse(a1);
//		System.out.println(a1);
		
		

	}

}
