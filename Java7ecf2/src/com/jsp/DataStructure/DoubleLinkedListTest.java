package com.jsp.DataStructure;


class Nodee
{
	Object ele;
	Nodee next;
	Nodee prev;
	
	public Nodee(Nodee p,Object e,Nodee n)
	{
		ele=e;
		next=n;
		prev=p;
	}
}

class DoubleLinkedList
{
	private Nodee first;
	private Nodee last;
	private int count;
	
	public void add(Object e)
	{
		if(first==null)
		{
			first=new Nodee(null,e,null);
			count++;
			last=first;
			return;
		}
		Nodee curr=first;
		while(curr.next!=null) {
		curr=curr.next;
		}
		curr.next=new Nodee(curr,e,null);
		last=curr.next;
		count++;
	}
	
	public int size() { return count;}
	
	public Object get(int index)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		Nodee curr=first;
		for(int i=1;i<=index;i++) { curr=curr.next;}
		return curr.ele;
	}
	
	public void add(int index,Object e)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		if(index==0)
		{
			Nodee temp=first;
			temp=new Nodee(null,e,first);
			first.prev=temp;
			first=temp;
			count++;
			return;
		}
		Nodee curr=first;
		for(int i=1;i<index;i++)
		{
			curr=curr.next;
		}
		curr.next=new Nodee(curr,e,curr.next);
		Nodee temp=curr.next.next;
		temp.prev=curr.next;
		count++;
	}
	
	public void remove(int index)
	{
		if(index<0 || index>=size()) throw new IndexOutOfBoundsException();
		if(index==0)
		{
			first=first.next;
			first.prev=null;
			count--;
			return;
		}
		Nodee curr=first;
		for(int i=1;i<index;i++)
		{
			curr=curr.next;
		}
		curr.next=curr.next.next;
		Nodee temp=curr.next;
		temp.prev=curr;
		count--;
	}
	public void reverse()
	{
		Nodee curr=last;
		while(curr.prev!=null)
		{
			System.out.println(curr.ele);
			curr=curr.prev;
		}
		System.out.println(curr.ele);
	}
}


public class DoubleLinkedListTest {

	public static void main(String[] args)
	{
		DoubleLinkedList l1=new DoubleLinkedList();
		
		l1.add(10);
		l1.add(20);
		l1.add(30);
		l1.add(40);
		l1.reverse();
		System.out.println("--------");
		l1.add(45);
		l1.reverse();
		
	}

}
