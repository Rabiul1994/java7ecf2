package com.jsp.DataStructure;

class MyArrayQueue 
{
	private Object[] a;
	private int p;
	
	public MyArrayQueue()
	{
		a=new Object[5];
		p=0;
	}
	
	public void add(Object e)
	{
		if(p>=a.length) increaseCapacity();
		a[p++]=e;
	}
	
	public Object peek()
	{
		if(p==0) return null;
		return a[0];
	}
	
	public Object poll()
	{
		Object temp=a[0];
		if(p==0) return null;
		for(int i=0;i<=size()-1;i++)
		{
			a[i]=a[i+1];
		}
		a[p-1]=null;
		p--;
		return temp;
	}
	
	public int size() { return p; }
	
	private void increaseCapacity()
	{
		Object[] temp=new Object[a.length+3];
		for(int i=0;i<a.length;i++)
		{
			temp[i]=a[i];
		}
		a=temp;
	}
}

public class TestMyArrayQueue
{
	public static void main(String[] args) 
	{
		MyArrayQueue q=new MyArrayQueue();
		q.add(10);
		q.add(20);
		q.add(30);
		q.add(40);
		System.out.println(q.size());
		while(q.peek()!=null)
		{
			System.out.println(q.poll());
		}
		
	}
}
