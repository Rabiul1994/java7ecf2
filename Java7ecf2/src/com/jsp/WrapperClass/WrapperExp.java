package com.jsp.WrapperClass;

public class WrapperExp {

	public static void main(String[] args) {
		int i=10;
		System.out.println(i);
		
		Integer i1=Integer.valueOf(i);  //Boxing
		
		
		System.out.println(i1);
		
		Integer i2=i;  //Auto-Boxing
		
		System.out.println(i2);
		
		int num=i1.intValue(); // unboxing
		
		System.out.println(num);
		
		int num1=i2;  //Auto-Unboxing
		System.out.println(num1);

	}

}
