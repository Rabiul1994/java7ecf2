package com.jsp.WrapperClass;

import java.util.Scanner;

class Wrap
{
	int count;
	

	public Wrap(int count) { this.count = count; }

	@Override
	public String toString() {
		
		return "Total Days attended="+count;
	}
}

public class StudentAttendance
{
	public static void calculate(Wrap m)
	{
		Scanner sc=new Scanner(System.in);
		
		
		while(m.count>0)
		{
			System.out.println("Enter a for absent.\tEnter s to stop");
			char c=sc.next().charAt(0);
			if(c=='a') m.count--;
			if(c=='s') break;
		}
		System.out.println("Totals Days attended:"+m.count);
	}
	
	public static void main(String[] args) 
	{
		Wrap m1=new Wrap(10);
		
		calculate(m1);
	}
}
