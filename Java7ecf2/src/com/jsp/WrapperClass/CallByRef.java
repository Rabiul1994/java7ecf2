package com.jsp.WrapperClass;

class MyWrap
{
	int n;
	
	public MyWrap(int n) { this.n=n;}
	
	public String toString()
	{
		return n+"";
	}
}

public class CallByRef {

	public static void change(MyWrap m)
	{
		m.n=m.n*7;
		
		System.out.println(m.n);
	}
	
	
	
	public static void main(String[] args) {

		int n=20;
		MyWrap m1=new MyWrap(n);
		
		System.out.println(m1);
		
		change(m1);
		
		System.out.println(m1);
		

	}

}
