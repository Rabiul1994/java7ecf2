package com.jsp.DataStructureTree;

public class Node1 
{
	Object key;
	Node1 left;
	Node1 right;
	
	public Node1(Object key, Node1 left, Node1 right) {
		this.key = key;
		this.left = left;
		this.right = right;
	}
	
}
