package com.jsp.DataStructureTree;


public class TestTree {

	public static void main(String[] args) 
	{
		BinarySearchTree b=new BinarySearchTree();
		b.add(90);
		b.add(70);
		b.add(100);
		b.add(95);
		b.add(50);
		b.add(70);
		b.add(50);
		b.add(105);
		
		System.out.println("Size="+b.size());
		
		b.levelOrderTraverse();
		b.postOrderTraverse();
		b.preOrderTraverse();
		b.inOrderTraverse();
		
		
	}

}
