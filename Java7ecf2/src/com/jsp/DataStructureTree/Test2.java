package com.jsp.DataStructureTree;

import java.util.Comparator;

class LengthComp implements Comparator
{

	@Override
	public int compare(Object o1, Object o2) {
		return ((String)o1).length()-((String)o2).length();
		
	}
	
}



public class Test2 {

	public static void main(String[] args) {
		
//		BST b1=new BST();
		Comparator c=new LengthComp();
		BST b1=new BST(c);
		
		
		b1.add("raja");
		b1.add("rakeshp");
		b1.add("ankita");
		b1.add("raja");
		
//		b1.add(30);
//		b1.add(65);
//		b1.add(85);
//		b1.add(10);
		
		b1.inOrderTraverse();
		
		
		
				

	}

}
