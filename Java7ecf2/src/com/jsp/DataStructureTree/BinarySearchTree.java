package com.jsp.DataStructureTree;

import java.util.*;

public class BinarySearchTree 
{
	private Node root;
	private int count;
	private boolean flag;
	
	public boolean add(int ele)
	{
		flag=true;
		root=addNode(root,ele);
		if(flag) System.out.println(ele+"->Added");
		else System.out.println(ele+"->Duplicate ele not added");
		return flag;
	}
	
	private Node addNode(Node n, int e)
	{
		if(n==null)
		{
			n=new Node(e,null,null);
			count++;
			return n;
		}
		if(e<n.key)  	  n.left=addNode(n.left,e);
		
		else if(e>n.key)  n.right=addNode(n.right,e);
		
		else flag=false;
		
		return n;
	}
	
	public int size() { return count; }
	
	public void levelOrderTraverse()
	{
		Queue<Node> q=new LinkedList<Node>();
		
		if(root!=null) q.add(root);
		{
			while(!q.isEmpty())
			{
				Node n=q.poll();
				System.out.print(n.key+" ");
				if(n.left!=null) q.add(n.left);
				if(n.right!=null) q.add(n.right);
			}
		}
		System.out.println();
		
	}
	public void inOrderTraverse()
	{
		inOrder(root);
		System.out.println();
	}
	
	private void inOrder(Node n)
	{
		if(n==null) return;
		
		inOrder(n.left);
		System.out.print(n.key+" ");
		inOrder(n.right);
	}
	
	public void postOrderTraverse()
	{
		postOrder(root);
		System.out.println();
	}
	
	private void postOrder(Node n)
	{
		if(n==null) return;
		
		postOrder(n.left);
		postOrder(n.right);
		System.out.print(n.key+" ");
	}
	
	public void preOrderTraverse()
	{
		preOrder(root);
		System.out.println();
	}
	
	private void preOrder(Node n)
	{
		if(n==null) return;
		
		System.out.print(n.key+" ");
		preOrder(n.left);
		preOrder(n.right);
		
	}
	
	
	
}
