package com.jsp.DataStructureTree;

import java.util.Comparator;

public class BST 
{
	private Node1 root;
	private int count;
	private boolean flag;
	private Comparator comp;
	
	public BST() { }
	
	public BST(Comparator c) { comp=c; }
	
	public boolean add(Object key)
	{
		flag=true;
		root=addNode(root,key);
		return flag;
	}
	
	private Node1 addNode(Node1 n, Object key)
	{
		if(n==null)
		{
			n=new Node1(key,null,null);
			count++;
			return n;
		}
		
		if(compareElement(key,n.key)<0) n.left=addNode(n.left, key);
		
		if(compareElement(key,n.key)>0) n.right=addNode(n.right, key);
		
		else flag=false;
		
		return n;
	}
	
	private int compareElement(Object arg1, Object arg2)
	{
		if(comp!=null) return comp.compare(arg1, arg2);
		
		Comparable c1=(Comparable)arg1;
		
		return c1.compareTo(arg2);
	}
	
	public void inOrderTraverse()
	{
		inOrder(root);
		System.out.println();
	}
	
	private void inOrder(Node1 n)
	{
		if(n==null) return;
		
		inOrder(n.left);
		System.out.print(n.key+" ");
		inOrder(n.right);
	}
}
