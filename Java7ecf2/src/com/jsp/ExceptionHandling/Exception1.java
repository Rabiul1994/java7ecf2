package com.jsp.ExceptionHandling;

import java.util.Scanner;

public class Exception1 {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		double d=0;
		System.out.println("Enter num1");
		int num1=sc.nextInt();
		System.out.println("Enter num2");
		int num2=sc.nextInt();
		
		int[] a=new int[5];
		
//		System.exit(3);  Forced termination
		
		try
		{
			d=num1/num2;
			System.out.println(d);
			
			System.out.println("Enter num");
			byte b=sc.nextByte();
			System.out.println(b);
			a[6]=24;
			
		}
		catch (Exception e) {
			System.out.println(e);
		}
		finally {
			System.out.println("Thank you");
		}
		
		
		for(int i=1;i<=4;i++) System.out.print(i+" ");

	}

}

