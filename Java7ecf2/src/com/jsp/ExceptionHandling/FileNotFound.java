package com.jsp.ExceptionHandling;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class FileNotFound {

	public static void main(String[] args) throws FileNotFoundException {
		readFile("myFile.txt");

	}
	
	private static void readFile(String fileName) throws FileNotFoundException
	{
		FileReader f1=new FileReader(fileName);
	}

}
