package com.jsp.ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exception5 {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number:");
		
		try {
		if(!(sc.hasNextInt())) throw new InputMismatchException("abc");
		
		int a=sc.nextInt();
		System.out.println(a);
		}
		
//		try
//		{
//			if(sc.nextInt()!=sc.nextInt()) throw new InputMismatchException("abc");
//		}
		catch (Exception e) {
			System.out.println(e);
		}
		
		
		

	}

}
