package com.jsp.ExceptionHandling;

import java.util.Scanner;

public class Exception4 {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter your age:");
		int age=sc.nextInt();
		
		try 
		{
			if(age<18) throw new ArithmeticException("Age should be greater than 18 years");
			
			System.out.println("Your Voter Id Registration is Successfull");
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("Thank you!");
	}

}
