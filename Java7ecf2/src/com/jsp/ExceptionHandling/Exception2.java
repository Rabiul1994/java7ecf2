package com.jsp.ExceptionHandling;

import java.util.Scanner;

public class Exception2 {
	
	public static void division()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num1");
		int num1=sc.nextInt();
		System.out.println("Enter num2");
		int num2=sc.nextInt();
		
		if(num2==0) throw new ArithmeticException("Dont divide by zero");
		
		double d=num1/num2;
		System.out.println(d);
	}

	public static void main(String[] args) 
	{
		
		try
		{
			division();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		

	}

}
