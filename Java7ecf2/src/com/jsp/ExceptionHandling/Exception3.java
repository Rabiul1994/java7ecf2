package com.jsp.ExceptionHandling;

import java.util.Scanner;

public class Exception3 {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter no");
		int n=sc.nextInt();
		int fact=1;
		
		try
		{
			if(n<0) throw new ArithmeticException("Dont use negatives for factorial");
			
			for(int i=1;i<=n;i++)
			{
				fact*=i;
			}
			System.out.println(fact);
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Thank you");
		
	}

}
