package com.jsp.ExceptionHandling;

import java.util.Scanner;

class CheckAge extends RuntimeException
{
	private String message;

	public CheckAge(String message) {
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	
}

public class CustomException {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the age:");
		int age=sc.nextInt();
		
		try
		{
			if(age<18) throw new CheckAge("You are not Eligible");
			System.out.println("You are eligible");
		}
		catch (CheckAge e) {
			e.printStackTrace();
		}
		
		System.out.println("Thank you");

	}

}
