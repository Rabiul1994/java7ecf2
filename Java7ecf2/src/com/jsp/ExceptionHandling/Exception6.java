package com.jsp.ExceptionHandling;

import java.util.Scanner;

public class Exception6 
{
	
	public static void m1() throws InterruptedException
	{
		m2();
	}
	public static void m2() throws InterruptedException
	{
		m3();
	}
	public static void m3() throws InterruptedException
	{
//		Scanner sc=new Scanner(System.in);
//		System.out.println("Enter age:");
//		
//		int age=sc.nextInt();
//		
//		if(age<18) throw new ArithmeticException("Age should be above 18");
//			
//		System.out.println("Voter registration success");
		
		for(int i=1;i<=10;i++)
		{
			System.out.println(i);
			Thread.sleep(1000);
		}
	}

	public static void main(String[] args) throws InterruptedException
	{
		
		m1();
	}

}
