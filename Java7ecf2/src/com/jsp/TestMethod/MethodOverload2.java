package com.jsp.TestMethod;

class Overload
{
	int i,j,k;
	
	public int m1(int i)
	{
		this.i=i;
		int fact=1;
		for(int k=1;k<=this.i;k++)
		{
			fact*=k;
		}
		return fact;
	}
	public int m1(int i,int j)
	{
		this.i=i;
		this.j=j;
		int s=this.i+this.j;
		return s;
	}
	
	public int m1(int i,int j,int k)
	{
		this.i=i;
		this.j=j;
		this.k=k;
		int equal=1;
		if(this.i>this.j)
		{
			if(this.i>this.k) return this.i;
		}
		else if(this.j>this.k) return this.j;
		else if(this.k>this.i) return this.k;
		return equal;
	}
	
}



public class MethodOverload2 {

	public static void main(String[] args) 
	{
		Overload s1=new Overload();
		int res=s1.m1(5);
		System.out.println(res);
		int res2=s1.m1(23, 44);
		System.out.println(res2);
		int res3=s1.m1(2, 4, 2);
		if(res3==1) System.out.println("Equal");
		else System.out.println(res3);
		
		
		

	}

}
