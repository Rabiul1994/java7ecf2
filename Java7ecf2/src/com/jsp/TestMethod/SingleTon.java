package com.jsp.TestMethod;

import java.util.Scanner;

class Theatre
{
	int seats=30;
	private static Theatre m=null;
	
	private Theatre() {	}
	
	public static Theatre getInstance()
	{
		if(m==null) m=new Theatre();
		return m;
	}
	
	public void reserveTickets(int n)
	{
		if(n>seats)
		{
			System.out.println(n+" seats not available");
			return;
		}
		seats=seats-n;
		System.out.println(n+" seats reserved");
		System.out.println(seats+" seats are available");
	}
}

class BookingApp
{
	public static void booking()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("How many tickets?");
		int n=sc.nextInt();
		Theatre t=Theatre.getInstance();
		t.reserveTickets(n);
	}
}

public class SingleTon {

	public static void main(String[] args) {
		BookingApp.booking();
		BookingApp.booking();
		BookingApp.booking();
		BookingApp.booking();
		
	}

}
