package com.jsp.TestMethod;

import java.io.Serializable;

class Student implements Serializable
{
	private int id;
	private int age;
	private String name;
	
	public Student() {
		System.out.println("Welcome To Jspiders");
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	public void setAge(int age)
	{
		this.age=age;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	
	public int getId()
	{
		return id;
	}
	public int getAge()
	{
		return age;
	}
	public String getName()
	{
		return name;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", age=" + age + ", name=" + name + "]";
	}
}


public class JavaBeans {

	public static void main(String[] args) {
		Student s1=new Student();
		s1.setName("Rabiul");
		s1.setAge(27);
		s1.setId(1);
		System.out.println(s1);

	}

}
