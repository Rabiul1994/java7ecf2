package com.jsp.TestMethod;

public class ArgReturn 
{
	public static void addition() //no argument no return type
	{
		int a=20;
		int b=30;
		int res=a+b;
		System.out.println(res);
	}
	
	public static void addition1(int a, int b) //with argument no return type
	{
		int res=a+b;
		System.out.println(res);
	}
	
	public static int addition2()  // no argument with return type
	{
		int a=24;
		int b=32;
		int res=a+b;
		return res;
	}
	
	public static int addition3(int a, int b) //with argument with return type
	{
		int res=a+b;
		return res;
	}
	
	
	public static void main(String[] args) 
	{
		addition();
		addition1(4,5);
		System.out.println(addition2());
		addition3(45,84);
		int result=addition3(45,84);
		System.out.println(result);
	}

}
