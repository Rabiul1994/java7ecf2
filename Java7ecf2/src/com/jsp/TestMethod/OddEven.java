package com.jsp.TestMethod;

import java.util.Scanner;

public class OddEven 
{
	public static void checkOddEven() 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num:");
		int num=sc.nextInt();
		
		if(num%2==0) System.out.println(num+" is even");
		else System.out.println(num+" is odd");
		
	}
	
}
