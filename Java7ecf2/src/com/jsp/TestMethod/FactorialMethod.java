package com.jsp.TestMethod;

import java.util.Scanner;

public class FactorialMethod 
{
		public static void fact() //no argument no return type
		{
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter num:");
			int num=sc.nextInt();
			int fact=1;
			for(int i=1;i<=num;i++) 
			{
				fact=fact*i;
			}
			System.out.println("factorial is " +fact);
		}
		
		public static void fact1(int a) //with argument no return type
		{
			int fact=1;
			for(int i=1;i<=a;i++)
			{
				fact=fact*i;
			}
			System.out.println("factorial is " +fact);
		}
		
		public static int fact2()  // no argument with return type
		{
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter num:");
			int num=sc.nextInt();
			int fact=1;
			for(int i=1;i<=num;i++)
			{
				fact*=i;
			}
			return fact;
		}
		
		public static int fact3(int a) //with argument with return type
		{
			int fact=1;
			for(int i=1;i<=a;i++)
			{
				fact*=i;
			}
			return fact;
		}
		
		public static void main(String[] args)
		{
			Scanner sc=new Scanner(System.in);
			System.out.println("enter Num:");
			int num=sc.nextInt();
			fact();
			fact1(num);
			System.out.println(fact2());
			int factorial=fact3(num);
			System.out.println(factorial);
		}
		
}
