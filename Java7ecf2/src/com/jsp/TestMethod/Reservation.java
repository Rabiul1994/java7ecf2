package com.jsp.TestMethod;

import java.util.Scanner;

class Train
{
	int seats=100;
	int booked=0;
	private static Train m=null;
	
	public Train() { }
	
	public static Train getInstance()
	{
		if(m==null) m=new Train();
		return m;
	}
	
	public void confirmTickets(int n)
	{
		if(n>seats) 
		{
			System.out.println(n+" seats not available");
			return;
		}
		if(n>0&&n<=seats) {
		seats=seats-n;
		booked+=n;
		System.out.println(n+" seats confirmed!");
		System.out.println(seats+" available");
		}
	}
	public void cancelTickets(int n)
	{
		if(n>booked)
		{
			System.out.println("Booked ticket="+booked);
			System.out.println("Cancel ticket must be less than booked ticket");
			return;
		}
		if(n==0) {
			System.out.println("Enter valid input");
			return;
		}
		System.out.println("Cancellation successful");
		booked-=n;
		seats=seats+n;
		System.out.println(seats+" available");
		
	}
	
}

class IRCTC
{
	public static void booking()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("how many ticket booking?");
		int n=sc.nextInt();
		Train t=Train.getInstance();
		t.confirmTickets(n);
	}
	public static void cancel()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("how many ticket cancel?");
		int n=sc.nextInt();
		Train t=Train.getInstance();
		t.cancelTickets(n);
	}
}



public class Reservation {

	public static void main(String[] args) {
		IRCTC.booking();
		IRCTC.booking();
		IRCTC.booking();
		IRCTC.cancel();
		IRCTC.booking();
		IRCTC.cancel();
	}

}
