package com.jsp.Thread;

class Tester
{
	String name="John";
	
	@Override
	public void finalize() throws Throwable {
		System.out.println("Garbage collected");
	}
}

public class GarbageCollector {

	public static void main(String[] args) {
		Tester t1=new Tester();
		Tester t2=new Tester();
		
		t1=t2;
		System.gc();
	}

}
