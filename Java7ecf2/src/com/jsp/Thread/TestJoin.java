package com.jsp.Thread;

class DisplayJoin
{
	 public void display()
	{
		Thread t=Thread.currentThread();
		for(int i=1;i<=5;i++)
		{
			try {
				System.out.println(t.getName()+", "+i);
			} catch (Exception e) {}
		}
	}
}

class Join1 extends Thread
{
	DisplayJoin d;
	
	public Join1(DisplayJoin d) {
		this.d = d;
	}
	
	@Override
	public void run() {
		d.display();
	}
}

public class TestJoin {

	public static void main(String[] args) throws InterruptedException 
	{
		DisplayJoin d=new DisplayJoin();
		Join1 j1=new Join1(d);
		Join1 j2=new Join1(d);
		
		j1.start();
//		j1.join();
		j2.start();
		
		
		

	}

}
