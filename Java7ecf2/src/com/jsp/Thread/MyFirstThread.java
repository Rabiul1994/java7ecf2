package com.jsp.Thread;

class MyThread extends Thread
{
	public void run()
	{
		for(int i=1;i<=100;i++)
		{
			System.out.println(i);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
	}
}


public class MyFirstThread {

	public static void main(String[] args) throws InterruptedException 
	{
		MyThread m1=new MyThread();
		MyThread m2=new MyThread();
		m1.start();
		m2.start();
		
		m1.join();
		m2.join();
		
		System.out.println("count="+Thread.activeCount());
		
	}

}
