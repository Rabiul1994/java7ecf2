package com.jsp.Thread;

class Customer
{
	long amount;
	
	public Customer(long amount) {
		super();
		this.amount = amount;
	}
	synchronized public void withdraw(int amt)
	{
		if(amt>amount) 
		{
			System.out.println("Insufficient balance");
			
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if(amt>amount)
		{
			System.out.println("Insufficient balance");
			return;
		}
		
		}
		amount=amount-amt;
		System.out.println("withdraw Success");
		System.out.println("balance="+amount);
	}
			
	synchronized public void deposit(int amt)
	{
		amount=amount+amt;
		System.out.println("deposit success");
		System.out.println("balance="+amount);
		notify();
	}
}

public class CustomerThread {

	public static void main(String[] args) {
		
		Customer c=new Customer(10000);
		
		new Thread()
		{
			public void run()
			{
				c.withdraw(20000);
			}
		}.start();
		
		new Thread()
		{
			public void run()
			{
				c.deposit(20000);
			}
		}.start();
	}

}
