package com.jsp.Thread;

class A
{
	synchronized public void m1(B b)
	{
		 System.out.println(Thread.currentThread().getName());
		 System.out.println("class A m1 method executed");
		 System.out.println("executing help of class b");
		 b.help();
	}
	
	 synchronized public void help()
	{
		System.out.println("Help A");
	}
}

class B
{
	synchronized public void m2(A a)
	{
		 System.out.println(Thread.currentThread().getName());
		 System.out.println("class B m2 method executed");
		 System.out.println("executing help of class A");
		 a.help();
	}
	
	 synchronized public void help()
	{
		System.out.println("Help B");
	}
}

class DeadLock extends Thread
{
	A a2=new A();
	B b2= new B();
	
	 public void execute()
	{
		start();
//		try {
//			join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		a2.m1(b2);
	}
	
	@Override
	public void run() {
		b2.m2(a2);
	}
}

public class DeadLockThread {

	public static void main(String[] args) throws InterruptedException {
		
		DeadLock t1=new DeadLock();
		
		t1.execute();
		
	

	}

}
