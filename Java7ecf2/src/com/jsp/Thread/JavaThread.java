package com.jsp.Thread;

class Resource
{
	synchronized public void m1()
	{
		for(int i=1;i<=10;i++)
		{
			System.out.println(i);
		}
	}
}

public class JavaThread {

	public static void main(String[] args) {
		Resource r=new Resource();
		
		new Thread()
		{
			public void run()
			{
				r.m1();
			}
		}.start();
		
		new Thread()
		{
			public void run()
			{
				r.m1();
			}
		}.start();
		
		System.out.println("count"+Thread.activeCount());

	}

}
