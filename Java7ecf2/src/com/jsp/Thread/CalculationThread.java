package com.jsp.Thread;

class Addition extends Thread
{
	public void run()
	{
		
		int num1=20;
		int num2=30;
		System.out.println("Addition="+(num1+num2));
	}
}

class Subtraction extends Thread
{
	public void run()
	{
		
		int num1=20;
		int num2=30;
		System.out.println("Subtraction="+(num2-num1));
	} 
}

class Multiplication extends Thread
{
	public void run()
	{
	
		int num1=20;
		int num2=5;
		System.out.println("Multiplication="+(num1*num2));
	}
}

class Division extends Thread
{
	public void run()
	{
		
		int num1=20;
		int num2=4;
		System.out.println("Division="+(num1/num2));
	}
}

public class CalculationThread {

	public static void main(String[] args) throws InterruptedException {
		
		Addition a1=new Addition();
		Subtraction s1=new Subtraction();
		Multiplication m1=new Multiplication();
		Division d1=new Division();
		a1.setPriority(9);
		a1.start();
		s1.start();
		m1.start();
		d1.start();
		
		a1.join();
		
		System.out.println("Thank you");

	}

}
