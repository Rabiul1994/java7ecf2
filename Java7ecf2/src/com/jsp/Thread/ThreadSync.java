package com.jsp.Thread;

class Display
{
	synchronized public void display()
	{
		Thread t=Thread.currentThread();
		for(int i=1;i<=5;i++)
		{
			try {
				System.out.println(t.getName()+", "+i);
			} catch (Exception e) {}
		}
	}
}

class Thd extends Thread
{
	Display d;

	public Thd(Display d) {
		this.d = d;
	}
	
	public void run()
	{
		d.display();
	}
}

public class ThreadSync {

	public static void main(String[] args) throws InterruptedException {
		Display d=new Display();
		Thd t1=new Thd(d);
		Thd t2=new Thd(d);
		Thd t3=new Thd(d);
		
		
		t1.start();
		t2.start();
		t3.start();

	}

}
