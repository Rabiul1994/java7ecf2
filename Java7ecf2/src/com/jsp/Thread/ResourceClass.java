package com.jsp.Thread;

class Operation
{
	 public void cal(int p)
	{
		 synchronized(this){
		for(int i=1;i<=10;i++)
		{
		int res=(int)Math.pow(i, p);
		
		System.out.println(res);
		}
		 }
		System.out.println("Thank you");
	}
	
}

class Square extends Thread
{
	Operation op;
	public Square(Operation op)
	{
		this.op=op;
	}
	
	public void run()
	{
		
		op.cal(2);
	}
	
}

class Cube extends Thread
{
	Operation op;
	public Cube(Operation op)
	{
		this.op=op;
	}
	public void run()
	{
		
		op.cal(3);
		
	}
	
}

public class ResourceClass {

	public static void main(String[] args) throws InterruptedException 
	{
		Operation op1=new Operation();
		
		Square s=new Square(op1);
		Cube c=new Cube(op1);
		
		s.start();
		c.start();
		
//		System.out.println(Thread.activeCount());

	}

}
