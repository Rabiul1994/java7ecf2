package com.jsp.Interface;


interface Drivable
{
	void start();
	void accelerate();
	void stop();
	static void userManual()
	{
		System.out.println("read user manual before use");
	}
	default void blutooth() {
		System.out.println("blutooth port installed");
	}
}
class Swift implements Drivable
{
	public void start()
	{
		System.out.println("key Starts..");
	}
	public void accelerate()
	{
		System.out.println("half clutch drive");
	}
	public void stop()
	{
		System.out.println("key stops");
	}
}

class BMW implements Drivable
{
	public void start()
	{
		System.out.println("button Starts..");
	}
	public void accelerate()
	{
		System.out.println("full clutch drive");
	}
	public void stop()
	{
		System.out.println("button stop");
	}
	
}

class Driver
{
	
	
	public void drive(Drivable d)
	{
		d.start();
		d.accelerate();
		d.stop();
		if(d instanceof BMW) d.blutooth();
		Drivable.userManual();
		
		
	}
}


public class CarCast {

	public static void main(String[] args) {
		Driver d=new Driver();
		d.drive(new BMW());
		System.out.println("--------------");
		d.drive(new Swift());
	}

}
