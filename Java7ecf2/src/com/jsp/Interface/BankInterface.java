package com.jsp.Interface;

interface Bank
{
	
	void interset();
	
}

class SBI implements Bank
{

	@Override
	public void interset() {
		System.out.println("Loan Interset=5%");
		
	}
}

class HDFC implements Bank
{

	@Override
	public void interset() {
		System.out.println("Loan Interest=4%");
		
	}
}

class Customer
{
	Bank b;
	public Customer(Bank b) {
		this.b=b;
	}
	
	public void loan()
	{
		b.interset();
	}
	
}


public class BankInterface {

	public static void main(String[] args) {
		Customer c1=new Customer(new SBI());
		c1.loan();
		
		Customer c2= new Customer(new HDFC());
		c2.loan();

	}

}
