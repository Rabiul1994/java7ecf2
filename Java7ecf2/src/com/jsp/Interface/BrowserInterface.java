package com.jsp.Interface;

import java.util.Scanner;

interface Browser
{
	void browse();
}

class Edge implements Browser
{

	@Override
	public void browse() {
		System.out.println("Welcome to Edge!");
		
	}
	
}

class Chrome implements Browser
{

	@Override
	public void browse() {
		System.out.println("Welcome to Chrome!");
		
	}
	
}

class Mozilla implements Browser
{

	@Override
	public void browse() {
		System.out.println("Welcome to Mozilla!");
		
	}
	
	public void operate()
	{
		System.out.println("Helps you operate government sites");
	}
	
}

class User
{
	
	public Browser run(String b)
	{
		
		if(b.equalsIgnoreCase("chrome")) return new Chrome();
		if(b.equalsIgnoreCase("edge")) return new Edge();
		if(b.equalsIgnoreCase("firefox")) return new Mozilla();
		System.out.println("Invalid browser entered");
		return null;
	}
	
}

public class BrowserInterface {

	public static void main(String[] args) {
		
		User a=new User();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter browser:");
		String b=sc.next();
		
		Browser b1=a.run(b);
		
		if(b1 instanceof Chrome) ((Chrome)b1).browse();
		if(b1 instanceof Edge) ((Edge)b1).browse();
		if(b1 instanceof Mozilla) ((Mozilla)b1).browse();
		
		
		
		
		
	}

}
