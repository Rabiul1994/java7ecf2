package com.jsp.Interface;

interface Animal
{
	void sound();
	void eat();
	void movement();
}

class Lion implements Animal
{

	@Override
	public void sound() {
		System.out.println("Lion Roars");
		
	}

	@Override
	public void eat() {
		System.out.println("Eats Non veg");
		
	}

	@Override
	public void movement() {
		System.out.println("runs");
		
	}
	
}

class Snake implements Animal
{

	@Override
	public void sound() {
		System.out.println("Hissshhhh");
		
	}

	@Override
	public void eat() {
		System.out.println("drinks milk");
		
	}

	@Override
	public void movement() {
		System.out.println("snake crawls");
		
	}
	
}


public class AnimalInterface {

	public static void main(String[] args) {
		Animal a;
		a=new Lion();
		a.sound();
		a.eat();
		a.movement();
		System.out.println("---------------");
		a=new Snake();
		a.sound();
		a.eat();
		a.movement();
		
	}

}
