package com.jsp.Regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumValidation {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter phone number:");
		String phn=sc.next();
		
		Pattern p=Pattern.compile("[6-9][0-9]{9}");
		Matcher m=p.matcher(phn);
		
		System.out.println(m.matches());
	}

}
