package com.jsp.Regex;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateValidation {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter date:");
		String dt=sc.next();
		String ptrn="(0[1-9])|([1-2][0-9])|(3[0-1])";
		Pattern p=Pattern.compile(ptrn);
		Matcher m=p.matcher(dt);
		
		System.out.println(m.matches());

	}

}
