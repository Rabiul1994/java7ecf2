package com.jsp.Regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidation {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("password:");
		String pwd=sc.next();
		String ptrn="(?=.*[!@#$%^&?*])(?=.*[A-Z])(?=.*[0-9]).{5,10}";
		Pattern p=Pattern.compile(ptrn);
		Matcher m=p.matcher(pwd);
		
		System.out.println(m.matches());

	}

}
