package com.jsp.DataAbstraction;

abstract class Bank
{
	abstract public void interest();
	abstract public void loan();
}

class HomeLoan extends Bank
{

	@Override
	public void interest() {
		System.out.println("Interest is 5%");
		
	}

	@Override
	public void loan() {
		System.out.println("Maximum loan amount is 1000000");
		
	}
	
}

class GoldLoan extends Bank
{

	@Override
	public void interest() {
		System.out.println("Interest is 4%");
		
	}

	@Override
	public void loan() {
		System.out.println("Maximum loan amount is 200000");
		
	}
	
}




public class BankAbstraction {

	public static void main(String[] args) {
		Bank b1=new HomeLoan();
		Bank b2=new GoldLoan();
		
		b1.interest();
		b1.loan();
		b2.interest();
		b2.loan();
		
	}

}
