package com.jsp.DataAbstraction;

 abstract class Animal
{
	abstract public void sound();
	abstract public void eat();
	abstract public void move();
	public void drink()
	{
		System.out.println("Animal drinks water");
	}
}

abstract class Lion extends Animal
{
	@Override
	public void sound() 
	{
		System.out.println("Roars");
	}
	
	@Override
	public void eat() {
		System.out.println("Eats non-veg");
	}
	
}

class BabyLion extends Lion
{

	@Override
	public void move() {
		System.out.println("Lion Runs");
	}
	
}


public class AbstractClass {

	public static void main(String[] args) 
	{
		BabyLion b= new BabyLion();
		b.drink();
		b.eat();
		b.move();
		b.sound();
	}

}
