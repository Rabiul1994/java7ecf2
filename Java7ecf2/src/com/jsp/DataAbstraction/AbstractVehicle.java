package com.jsp.DataAbstraction;

abstract class Vehicle
{
	abstract public void start();
	abstract public void stop();
	abstract public void accelerate();
	abstract public void NoOfWheels();
	abstract public void fuel();
	final int a=0;
	
	 
	
	
	public Vehicle(int n) {
		// TODO Auto-generated constructor stub
	}
	
}
abstract class TwoWheeler extends Vehicle
{

	public TwoWheeler(int n) {
		super(n);
		// TODO Auto-generated constructor stub
	}

	@Override
	final public void NoOfWheels() {
		System.out.println("Two wheels are used here");
	}
	
//	abstract public static void check();
}

class Cycle extends TwoWheeler
{
	public Cycle(int n) {
		super(n);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fuel() {
		System.out.println("No fuel required");
		
	}

	@Override
	public void start() {
		System.out.println("starts by paddling");
		
	}

	@Override
	public void stop() {
		System.out.println("stops by brake");
		
	}

	@Override
	public void accelerate() {
		System.out.println("increase paddling..");
		
	}
}

class Bike extends TwoWheeler
{
	public Bike(int n) {
		super(n);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fuel() {
		System.out.println("petrol required");
		
	}

	@Override
	public void start() {
		System.out.println("starts by key");
		
	}

	@Override
	public void stop() {
		System.out.println("stops by key");
		
	}

	@Override
	public void accelerate() {
		System.out.println("half clutch");
		
	}
}





abstract class FourWheeler extends Vehicle
{
	public FourWheeler(int n) {
		super(n);
		// TODO Auto-generated constructor stub
	}

	@Override
	final public void NoOfWheels() {
		System.out.println("Uses four wheels");
		
	}
}
class Van extends FourWheeler
{

	public Van(int n) {
		super(n);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fuel() {
		System.out.println("Van uses petrol");
		
	}

	@Override
	public void start() {
		System.out.println("Uses key to start");
		
	}

	@Override
	public void stop() {
		System.out.println("Uses key to stop");
		
	}

	@Override
	public void accelerate() {
		System.out.println("half/full clutch");
		
	}
	
}
class Car extends FourWheeler
{

	public Car(int n) {
		super(n);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void fuel() {
		System.out.println("car uses petrol/diesel");
		
	}

	@Override
	public void start() {
		System.out.println("car uses button to start");
		
	}

	@Override
	public void stop() {
		System.out.println("car uses button to stop");
		
	}

	@Override
	public void accelerate() {
		System.out.println("Full clutch required");
		
	}
	
}


public class AbstractVehicle {

	public static void main(String[] args) 
	{
		Vehicle vehicle1 =new Cycle(10);
		Vehicle vehicle2 =new Bike(20);
		Vehicle vehicle3 =new Van(30);
		Vehicle vehicle4 =new Car(40);
		
		vehicle1.NoOfWheels();
		vehicle1.start();
		vehicle1.stop();
		vehicle1.accelerate();
		vehicle1.fuel();
		
	}

}
