package com.jsp.Inheritance;

interface A
{
	public void m1();
	
}

interface B
{
	public void m1();
}

class C implements A,B
{

	@Override
	public void m1() {
		System.out.println("m1 method of class c is called");
		
	}
	
}


public class InterfaceExp {

	public static void main(String[] args) {
		C c1=new C();
		c1.m1();
	}

}
