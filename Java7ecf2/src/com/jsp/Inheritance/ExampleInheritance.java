package com.jsp.Inheritance;

class Hospital
{
	String name="Care 24X7";
	long e_no=121;
	
	public void emNo()
	{
		System.out.println("Emergency Call"+e_no);
	}
	public void hospitalName()
	{
		System.out.println("Hospital Name"+name);
	}
}

class People extends Hospital
{
	String p_name="ABCD";
	
	public void callTo()
	{
		System.out.println("Call to emergency:"+e_no);
	}
}

public class ExampleInheritance {

	public static void main(String[] args) 
	{
		People p1=new People();
		p1.hospitalName();
		p1.emNo();
		p1.callTo();
		
	}

}
