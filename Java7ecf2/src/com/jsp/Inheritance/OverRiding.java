package com.jsp.Inheritance;

class Fathr
{
	String b="TvS Classic";
	long ph=12345;
	public void bike()
	{
		System.out.println("Father bike: "+b);
	}
	public void call()
	{
		System.out.println(ph);
	}
	
}

class Son extends Fathr
{
	String bb="TVS RTR";
	
	public void bike()
	{
//		super.bike();
		System.out.println("My bike: "+bb);
	}
	
}
class Daughter extends Fathr
{
	long ph=76463;
	
	public void call()
	{
		System.out.println(ph);
	}
	
	
}

public class OverRiding {

	public static void main(String[] args) {
		Son s1=new Son();
		Daughter d1=new Daughter();
		s1.bike();
		d1.call();
	}

}
