package com.jsp.Inheritance;

class Grandfather
{
	String eyecolor="blue";
	
	public void display()
	{
		System.out.println("My :");
		System.out.println("Eyecolor: "+eyecolor);
	}
}

class Father extends Grandfather
{
	String haircolor="Black";
	
	public void displayH()
	{
		System.out.println("haircolor: "+haircolor);
	}
}

class Child extends Father
{
	long ph=764637;
	
	public void displayPh()
	{
		
		System.out.println("phone no: "+ph);
	}
}


public class MultiLevel {

	public static void main(String[] args) 
	{
		Child c1=new Child();
		c1.display();
		c1.displayH();
		c1.displayPh();
	}

}
