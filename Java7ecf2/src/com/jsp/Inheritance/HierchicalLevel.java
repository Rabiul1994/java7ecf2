package com.jsp.Inheritance;

class Fatherr
{
	long ph=12345;
	public void phone()
	{
		System.out.println("phone No.: "+ph);
	}
}

class Child1 extends Fatherr
{
	String school="DPS";
	public void school1()
	{
		System.out.println("School: "+school);
	}
}

class Child2 extends Fatherr
{
	String school="City Public School";
	public void school2()
	{
		System.out.println("School: "+school);
	}
}

public class HierchicalLevel {

	public static void main(String[] args) 
	{
		Child1 c1=new Child1();
		Child2 c2=new Child2();
		
		c1.school1();
		c1.phone();
		c2.school2();
		c2.phone();
	}

}
