package com.jsp.Inheritance;

import java.util.Scanner;

class Veg
{
	
}
class Carrot extends Veg
{
	public void prepareHalwa()
	{
		System.out.println("Carrot halwa");
	}
}
class Potato extends Veg
{
	public void fries()
	{
		System.out.println("French fries");
	}
}

class Shop
{
	public Veg sell(String vn)
	{
		if(vn.equalsIgnoreCase("carrot")) return new Carrot();
		if(vn.equalsIgnoreCase("Potato")) return new Potato();
		System.out.println(vn+" is not available");
		return null;
	}
}

public class VegCasting {

	public static void main(String[] args) {
		
		Shop s=new Shop();
		Scanner sc=new Scanner(System.in);
		System.out.println("Vegetable?");
		String vn=sc.next();
		
		Veg v= s.sell(vn);
		
		if(v instanceof Carrot)
			((Carrot)v).prepareHalwa();
		if(v instanceof Potato)
			((Potato)v).fries();
			
	}

}
