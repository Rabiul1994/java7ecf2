package com.jsp.Inheritance;

class Demo
{
	int a=65;
	public void displayDemo()
	{
		System.out.println("DisplayDemo");
	}
}
class Memo extends Demo
{
	int a=25;
	public void displayMemo()
	{
		this.displayDemo();
		System.out.println(this.a);
		System.out.println(super.a);
		System.out.println("DisplayMemo");
	}
}



public class NewTest 
{

	public static void main(String[] args) 
	{
		Memo m1=new Memo();
		m1.displayMemo();
		
	}

}
