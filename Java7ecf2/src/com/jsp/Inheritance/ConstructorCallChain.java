package com.jsp.Inheritance;

class Demo1
{
	public Demo1() 
	{
		
		System.out.println("Demo1 no arg Constructor");
	}
	public Demo1(int a)
	{
		this();
		System.out.println("demo1 Parameters const");
	}
}

class Demo2 extends Demo1
{
	public Demo2() {
		super(); //by default, super is called..if you write super or not
		System.out.println("Demo2 no arg Constructor");
	}
	public Demo2(int a)
	{
		this();
		System.out.println("parameterized sub class");
	}
			
}
public class ConstructorCallChain {

	public static void main(String[] args) {
		new Demo2(4);
	}

}
