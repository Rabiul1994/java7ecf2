package com.jsp.Inheritance;

class Car
{
	public void start() {}
	public void accelerate() {}
	public void stop() {}

}

class Maruti extends Car
{
	@Override
	public void start()
	{
		System.out.println("key Starts..");
	}
	@Override
	public void accelerate()
	{
		System.out.println("half clutch drive");
	}
	@Override
	public void stop()
	{
		System.out.println("key stops");
	}
	public void bluetooth()
	{
			System.out.println("Bluetooth is on for maruti");
	}
}

class Honda extends Car
{
	@Override
	public void start()
	{
		System.out.println("button Starts..");
	}
	@Override
	public void accelerate()
	{
		System.out.println("full clutch drive");
	}
	@Override
	public void stop()
	{
		System.out.println("button stop");
	}
	public void music()
	{
		System.out.println("honda songs playing");
	}
	public void rooftop()
	{
		System.out.println("honda Roof opens");
	}
}

class RangeRover extends Car
{
	@Override
	public void start()
	{
		System.out.println("button/key Starts..");
	}
	@Override
	public void accelerate()
	{
		System.out.println("full/half clutch drive");
	}
	@Override
	public void stop()
	{
		System.out.println("button/key stop");
	}
	public void bluetooth()
	{
			System.out.println("Bluetooth is on for RR");
	}
	
}
class Driver
{
	public void drive(Car c)
	{
		c.start();
		c.accelerate();
		c.stop();
	}
	public void accessBluetooth(Car c)
	{
		if(c instanceof Maruti) {
		Maruti m=(Maruti)c; // downcasting
		m.bluetooth();
		}
		if(c instanceof RangeRover) {
		RangeRover r=(RangeRover)c; //downcasting
		r.bluetooth();
		}
	}
	public void accessMusic(Car c)
	{
		Honda h=(Honda)c;
		h.music();
		h.rooftop();
	}
	
}


public class CarCasting {

	public static void main(String[] args) 
	{
		
		Driver d1=new Driver();
		d1.drive(new Maruti()); // upcasting
		d1.accessBluetooth(new Maruti());
		System.out.println("========================");
		d1.drive(new Honda()); //upcating
		d1.accessMusic(new Honda());
		System.out.println("========================");
		d1.drive(new RangeRover()); //upcasting
		d1.accessBluetooth(new RangeRover());
		System.out.println("========================");
		
		
	}

}
