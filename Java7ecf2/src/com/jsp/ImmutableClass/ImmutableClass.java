package com.jsp.ImmutableClass;

class Immute
{
	private int i;

	public Immute(int i) {
		this.i = i;
	}
	
	public Immute modify(int x)
	{
		if(this.i==x) return this;
		
		else return new Immute(x);
	}
}


public class ImmutableClass {

	public static void main(String[] args) {
		Immute t = new Immute(10);
		Immute t1 = t.modify(10);
		Immute t2 = t.modify(100);
		
		System.out.println(t==t1);
		System.out.println(t==t2);
		
	}

}
