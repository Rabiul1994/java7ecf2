package com.jsp.ImmutableClass;

final class Employee
{
	private final String panNum;

	public Employee(String panNum) {
		this.panNum = panNum;
	}
	
	public String getPanNum()
	{
		return panNum;
	}
}


public class ImmutableDemo 
{
	public static void main(String[] args) {
		Employee employee = new Employee("AJN76657B");
		String panNum = employee.getPanNum();
		System.out.println(panNum);
	}
}
