package com.jsp.Practice;

import java.util.Arrays;

class Employee implements Comparable<Employee>
{
	int id;
	String name;
	int age;
	
	public Employee(int id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age + "]";
	}

	@Override
	public int compareTo(Employee e) {
		return this.id-e.id;
	}
}


public class EmployeeSort {

	public static void main(String[] args) {
		
		Employee e1=new Employee(121, "Sukumar", 36);
		Employee e2=new Employee(102, "Rajkumar", 28);
		Employee e3=new Employee(111, "Pawankumar", 26);
		Employee e4=new Employee(124, "Surya", 33);
		
		Employee[] a=new Employee[4];
		a[0]=e1;
		a[1]=e2;
		a[2]=e3;
		a[3]=e4;
		
		Arrays.sort(a);
		for(Employee e:a) System.out.println(e);
	}

}
