package com.jsp.Practice;

import java.util.LinkedList;
import java.util.List;

class EmployeeComp
{
	int id;
	String name;
	int age;
	double salary;
	
	public EmployeeComp(int id, String name, int age, double salary) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	
	public void compare()
	{
		if(this.age<25) this.salary=this.salary+10000;
	}
	
	

	@Override
	public String toString() {
		return "EmployeeComp [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + "]";
	}
	
}


public class EmployeeComparison {

	public static void main(String[] args) {
		EmployeeComp e1=new EmployeeComp(121, "Rakib", 24, 30000);
		EmployeeComp e2=new EmployeeComp(122, "Rabiul", 28, 100000);
		EmployeeComp e3=new EmployeeComp(121, "Saddam", 31, 50000);
		EmployeeComp e4=new EmployeeComp(121, "Miraj", 23, 50000);
		
		List<EmployeeComp> list = new LinkedList<EmployeeComp>();
		
		list.add(e1);
		list.add(e2);
		list.add(e3);
		list.add(e4);
		
		System.out.println(list);
		
//		e1.compare();
//		e2.compare();
//		e3.compare();
//		e4.compare();
		
		for(EmployeeComp e:list)
		{
			if(e.age<25)
			{
				e.salary=e.salary*(6.0/5.0);
			}
		}
		
		System.out.println(list);
		
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}

}
