package com.jsp.Practice;


public class Recursion {
	
	
	public static int sum(int n)
	{
		int sum=0;
		if(n<3) return 0;
		
		if(n%3==0 || n%5==0)
		{
			sum=n;
		}
		return sum+ sum(n-1);
		
	}

	public static void main(String[] args) {
		int number=10;
		System.out.println(sum(number-1));
		

	}

}
