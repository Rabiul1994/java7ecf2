package com.jsp.Practice;

import java.util.Scanner;

public class Practice6 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter range:");
		int n=sc.nextInt();
		int spaces=n-1;
		for(int i=1;i<=n;i++)
		{
			int t=n-1;
			int k=i;
			for(int j=1;j<=spaces;j++)
			{
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++)
			{
				System.out.print(k+"\t");
				k+=t;
				t--;
			}
			System.out.println();
			spaces--;
		}

	}

}
