package com.jsp.Practice;

import java.util.LinkedList;

public class DecimalToBinary {

	public static void main(String[] args) {
		
		long n=3;
		long bin=0;
		long i=1;
		while(n>0)
		{
			long mod=n%2;
			bin=bin+(mod*i);
			i*=10;
			n/=2;
		}
		
		System.out.println(bin);
		
		System.out.println("-------In-built method to find binary of a number--------");
		long dec=2539873;
		String b=Long.toBinaryString(dec);
//		String c=Integer.toHexString(dec);
//		String d=Integer.toOctalString(dec);
		System.out.println(b);
//		System.out.println(c);
//		System.out.println(d);
		

	}

}
