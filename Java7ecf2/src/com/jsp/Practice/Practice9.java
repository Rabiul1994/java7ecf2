package com.jsp.Practice;

import java.util.Scanner;

 /*	1 2 3 4 5
  	 2 3 4 5
   	  3 4 5
       4 5
     	5
       4 5
      3 4 5
     2 3 4 5
    1 2 3 4 5   */


public class Practice9 {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter range:");
		int n=sc.nextInt();
		int t=0;
		int spaces=0;
		for(int i=1;i<=2*n-1;i++)
		{
			if(i<=2*n/2) 
			{
				t++;
				spaces++;
			}
			else
			{
				spaces--;
				t--;
			}
			for(int j=2;j<=spaces;j++) 
			{
				System.out.print(" ");
			}
			for(int j=t;j<=n;j++)
			{
				System.out.print(" "+j);
			}
			System.out.println();
			
		}

	}

}
