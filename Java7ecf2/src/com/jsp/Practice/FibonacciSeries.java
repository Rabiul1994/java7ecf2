package com.jsp.Practice;

public class FibonacciSeries 
{
	long n1,n2,s;
	
	
	
	public FibonacciSeries(long n1, long n2, long s) {
		this.n1 = n1;
		this.n2 = n2;
		this.s = s;
	}

	public long fibonacci(int n)
	{
		if(n<=1) return n;
		s=n1+n2;
		n1=n2;
		n2=s;
		fibonacci(n-1);
		return s;
	}

	public static void main(String[] args) 
	{
		for(int i=1;i<=10;i++) {
		FibonacciSeries f=new FibonacciSeries(0, 1, 0);
		System.out.println(f.fibonacci(i));
		}

	}

}
