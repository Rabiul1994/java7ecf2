package com.jsp.Practice;

public class Practice2 {// malayalam

	public static void main(String[] args) 
	{
		String s1="malayalam";
		s1=s1.toLowerCase();
		StringBuilder sb=new StringBuilder(s1);
		for(int i=0;i<sb.length()-1;i++)
		{
			for(int j=sb.length()-2;j>=0;j--)
			{
				String s2=sb.substring(j);
				if(isPalindrome(s2)) System.out.println(s2);
			}
			sb.deleteCharAt(sb.length()-1);
		}
	}
	
	public static boolean isPalindrome(String s)
	{
		s=s.toLowerCase();
		char[] a=s.toCharArray();
		int i=0; int j=s.length()-1;
		while(i<j)
		{
			if(a[i]!=a[j]) return false;
			i++;
			j--;
		}
		return true;
	}

}
