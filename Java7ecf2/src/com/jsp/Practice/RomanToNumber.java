package com.jsp.Practice;

import java.util.HashMap;


public class RomanToNumber 
{
	public static int romanToNumber(String s)
	{
		int value=0;
		HashMap<Character, Integer> hm=new HashMap();
		
		hm.put('I', 1);
		hm.put('V', 5);
		hm.put('X', 10);
		hm.put('L', 50);
		hm.put('C', 100);
		hm.put('D', 500);
		hm.put('M', 1000);
		
		char[] a=s.toCharArray();
		
		int i=0; int j=a.length-1;
		while(i<=j)
		{
			if(i<j && a[i]=='I' && ( a[i+1]== 'V' || a[i+1]=='X'))
			{
				value+=hm.get(a[i+1])-hm.get(a[i]);
				i++;
			}
			else if(i<j && a[i]=='X' && ( a[i+1]== 'L' || a[i+1]=='C'))
			{
				value+=hm.get(a[i+1])-hm.get(a[i]);
				i++;
			}
			else if(i<j && a[i]=='C' && ( a[i+1]== 'D' || a[i+1]=='M'))
			{
				value+=hm.get(a[i+1])-hm.get(a[i]);
				i++;
			}
			else
			{
				value+=hm.get(a[i]);
			}
			i++;
		}
		
		return value;
		
	}
	
	public static void main(String[] args) {
		
		System.out.println(romanToNumber("MCMXCIV"));
	}

}
