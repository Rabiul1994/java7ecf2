package com.jsp.Practice;

import java.util.*;

public class Practice14 
{
	
	public static int check(int[] arr)
	{
		Arrays.sort(arr);
		for(int n:arr) System.out.print(n+" ");
		System.out.println();
		int temp=arr[0];
		int count=0;
		
		for(int i=1;i<arr.length;i++)
		{
			if(temp==arr[i]) continue;
			if(temp+1==arr[i]) count++;
			if((temp+1)!=arr[i]) break;
			temp++;
		}
		if(count==arr.length-1) return 1;
		return 0;
		
	}

	public static void main(String[] args) 
	{
		
		int n=6;
		int[] a=new int[n];
		a[0]=87;
		a[1]=91;
		a[2]=89;
		a[3]=88;
		a[4]=86;
		a[5]=90;
		
		System.out.println(check(a));
		
	}

}
