package com.jsp.Practice;

class BookingApp
{
	int seats;
	private static BookingApp b=null;
	
	private BookingApp()
	{
		seats=30;
	}
	
	
	public static BookingApp getInstance()
	{
		if(b==null) b=new BookingApp();
		return b;
	}
	
	public void reserveTicket(int n)
	{
		if(n>seats) {
			System.out.println(n+" seats not available");
			return;
		}
		
		seats=seats-n;
		System.out.println(n+" seats are reserved");
		System.out.println(seats+ " are available");
		
		
	}
	
}

public class MovieHall {

	public static void main(String[] args) {
		BookingApp b2=BookingApp.getInstance();
		b2.reserveTicket(4);
		BookingApp b3=BookingApp.getInstance();
		b3.reserveTicket(26);
		BookingApp b4=BookingApp.getInstance();
		b4.reserveTicket(5);
		
		
		

	}

}
