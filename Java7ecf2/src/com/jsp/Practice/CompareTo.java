package com.jsp.Practice;

public class CompareTo {

	public static void main(String[] args) 
	{
		String s1="abcd";
		String s2="cdef";
		System.out.println(s1.compareTo(s2));
		
		System.out.println(compare("abc","abc"));

	}
	
	private static int compare(String s1, String s2)
	{
		int len1=s1.length();
		int len2=s2.length();
		int min=len1<len2?len1:len2;  // int min=Math.min(len1,len2);
		
		char[] ch1=s1.toCharArray();
		char[] ch2=s2.toCharArray();
		for(int i=0;i<min;i++)
		{
			char c1=ch1[i];
			char c2=ch2[i];
			if(c1!=c2) return c1-c2;
		}
		return len1-len2;
	}

}
