package com.jsp.Practice;


// #class which allows to create only one object is called singleton class
class Test
{
	int i;
	private static Test m=null;
	
//	private Test() {
//		
//	}
	
	public static Test getInstance()  //Factory method
	{
		if(m==null) m=new Test();
			return m;
	}
	public void display()
	{
		System.out.println("i="+i);
	}
}


public class SingleTonExp {

	public static void main(String[] args) {
		Test t1=Test.getInstance();
		Test t2=Test.getInstance();
		System.out.println(t1==t2);


	}

}
