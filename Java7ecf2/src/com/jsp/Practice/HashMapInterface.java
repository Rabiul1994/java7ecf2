package com.jsp.Practice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class HashMapInterface {

	public static void main(String[] args) {
		
		PriorityQueue<Integer> pq= new PriorityQueue<>(3, new Comparing());
		
		pq.add(87);
		pq.add(48);
		pq.add(10);
		pq.add(24);
		pq.add(33);
		
		System.out.println(pq);
		System.out.println(pq.comparator());

	}
	
}
class Comparing implements Comparator<Integer>
{

	@Override
	public int compare(Integer o1, Integer o2) {
		return o1-o2;
	}
	
}
