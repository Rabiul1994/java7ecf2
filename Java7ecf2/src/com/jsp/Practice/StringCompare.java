package com.jsp.Practice;

public class StringCompare {
	
	String str1;

	public StringCompare(String str1) {
		super();
		this.str1 = str1;
	}
	
	
	
	@Override
	public String toString() {
		return str1;
	}



	public int compareTo(String str2)
	{
		int l1=str1.length();
		int l2=str2.length();
		int min= Math.min(l1, l2);
		
		char[] ch1= str1.toCharArray();
		char[] ch2= str2.toCharArray();
		
		for(int i=0;i<min;i++)
		{
			char c1= str1.charAt(i);
			char c2= str2.charAt(i);
			if(c1!=c2) return c1-c2;
		}
		
		return l1-l2;
	}
}
