package com.jsp.Practice;

import java.util.Scanner;

class Magic
{
	private int n;
	
	public void setN(int n)
	{
		this.n=n;
	}
	private int reverse(int n)
	{
		int rev=0;
		while(n>0)
		{
			int mod=n%10;
			rev=(rev*10)+mod;
			n/=10;
		}
		return rev;
	}
	
	private boolean magic()
	{
		int sum=0;
		int temp=this.n;
		while(n>0)
		{
			int mod=n%10;
			sum+=mod;
			n/=10;
		}
		int rev=reverse(sum);
		if(sum*rev==temp) return true;
		return false;
	}
	public boolean getResult()
	{
		if(magic()) return true;
		return false;
	}
}


public class MagicNo {

	public static void main(String[] args) {
		
		Magic m=new Magic();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
		for(int i=1;i<=n;i++)
		{
			m.setN(i);
			if(m.getResult()) System.out.println(i);
		}
	}
}
