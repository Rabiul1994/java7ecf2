package com.jsp.pack2;

import java.util.Scanner;

public class PrimeNumRange 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number ");
		int num=sc.nextInt();
		for(int i=1;i<=num;i++)
		{
			if(i<=1) continue;
			boolean flag=true;
			for(int j=2;j<=i/2;j++)
			{
				if(i%j==0)
				{
					flag=false;
					break;
				}
			}
				if(flag) System.out.println(i);
		}
	}

}
