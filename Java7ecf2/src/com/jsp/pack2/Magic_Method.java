package com.jsp.pack2;

public class Magic_Method 
{
	
	static boolean isMagic(int n)
	{
		int s=0;
		while(n>0||s>9)
		{
			if(n==0)
			{
				n=s;
				s=0;
			}
			int mod=n%10;
			s=s+(int)Math.pow(mod, 2);
			n/=10;
		}
		if(s==1) return true;
		return false;
	}

	public static void main(String[] args) 
	{
		System.out.println(isMagic(19));
	}

}
