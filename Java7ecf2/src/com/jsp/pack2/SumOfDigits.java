package com.jsp.pack2;

import java.util.Scanner;

public class SumOfDigits // until single digit
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number ");
		int n=sc.nextInt();
		int s=0;
		while(n>0 || s>9)
		{
			if(n==0)
			{
				n=s;
				s=0;
			}
			int mod=n%10;
			s=s+mod;
			n/=10;
		}
		System.out.println("Sum "+s);
	}

}
