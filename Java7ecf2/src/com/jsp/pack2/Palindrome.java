package com.jsp.pack2;

import java.util.Scanner;

public class Palindrome 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number ");
		int num=sc.nextInt();
		int pal=num;
		int mod;
		int rev=0;
		while(num>0)
		{
			mod=num%10;
			rev=(rev*10)+mod;
			num=num/10;
		}
		if(pal==rev)
		{
			System.out.println(pal+ " is Palindrome");
		}
		else
		{
			System.out.println(pal+ " is not Palindrome");
		}
	}

}
