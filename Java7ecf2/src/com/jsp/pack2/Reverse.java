package com.jsp.pack2;

import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num:");
		int n=sc.nextInt();
		int rev=0;
		while(n>0)
		{
			int mod=n%10;
			rev=(rev*10)+mod;
			n/=10;
		}
		System.out.println(rev);

	}

}
