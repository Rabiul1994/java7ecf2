package com.jsp.pack2;

import java.util.Scanner;

public class FiboRange {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter lower limit:");
		int s=sc.nextInt();
		System.out.println("enter upper limit:");
		int e=sc.nextInt();
		int n1=0; int n2=1; int n3=0;
		if(s==0)
		{
			System.out.print(n1+" ");
			System.out.print(n2+" ");
		}
		
		while(true)
		{
			n3=n1+n2;
			if(n3>=s && n3<=e)
			{
				System.out.print(n3+" ");
			}
			if(n3>e) break;
			n1=n2;
			n2=n3;
		}

	}

}
