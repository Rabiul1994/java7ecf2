package com.jsp.pack2;

import java.util.Scanner;

public class MultiplicationTable 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number");
		int num=sc.nextInt();
		for(int i=1;i<11;i++)
		{
			int m=num*i;
			System.out.println(num+ " x " +i+ "= " +m);
		}
	}

}
