package com.jsp.pack2;

import java.util.Scanner;

public class PerfectRange
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Starting Range: ");
		int a=sc.nextInt();
		System.out.println("Enter Stop Range: ");
		int b=sc.nextInt();
		for(int i=a; i<=b;i++)
		{
			int sum=0;
			for(int j=1;j<=i/2;j++)
			{
			if(i%j==0)
			{
				sum=sum+j;
			}
			}
			if(sum==i)
			{
				System.out.println(i);
			}
		}
	}

}
