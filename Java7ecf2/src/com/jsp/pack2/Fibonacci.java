package com.jsp.pack2;

import java.util.Scanner;

public class Fibonacci 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Range:");
		int n=sc.nextInt();
		int n1=0;
		int n2=1;
		System.out.print(n1+" ");
		System.out.print(n2+" ");
		while(n-2>0)
		{
			int sum=n1+n2;
			System.out.print(sum+" ");
			n1=n2;
			n2=sum;
			n--;
		}
	}

}
