package com.jsp.pack2;

import java.util.Scanner;

public class PrimeNo 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number ");
		int num=sc.nextInt();
		if(num<0)
		{
			System.out.println(num+" is not prime");
		}
		boolean flag=true;
		for(int i=2;i<num/2;i++)
		{
			if(num%i==0)
			{
				flag=false;
				break;
			}
		}
		if(flag) System.out.println(num+" is Prime");
		else System.out.println(num+" is not prime");
	}

}
