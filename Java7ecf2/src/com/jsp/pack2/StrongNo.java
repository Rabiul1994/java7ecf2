package com.jsp.pack2;

import java.util.Scanner;

public class StrongNo 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number to check for Strong number ");
		int num=sc.nextInt();
		int temp=num;
		int i;
		int mod;
		int sum=0;
		while(num>0)
		{
			int fac=1;
			mod=num%10;
			for(i=mod;i>0;i--)
			{
				fac=fac*i;
			}
			sum=sum+fac;
			num=num/10;
		}
		System.out.println("Sum of factorial of each digit is "+sum);
		if(temp==sum)
		{
			System.out.println(temp+" is a Strong Number");
		}
		else
		{
			System.out.println(temp+" is not a Strong Number");
		}

	}

}
