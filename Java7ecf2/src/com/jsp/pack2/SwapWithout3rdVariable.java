package com.jsp.pack2;

import java.util.Scanner;

public class SwapWithout3rdVariable 
{

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num1:");
		int i=sc.nextInt();
		System.out.print("Enter num2:");
		int j=sc.nextInt();
		i=i+j;
		j=i-j;
		i=i-j;
		System.out.println("Num1= "+i);
		System.out.println("Num2= "+j);
	}

}
