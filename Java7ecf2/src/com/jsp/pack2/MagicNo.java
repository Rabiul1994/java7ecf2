package com.jsp.pack2;

import java.util.Scanner;

public class MagicNo {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter no");
		int n=sc.nextInt();
		int s=0;
		while(n>0||s>9)
		{
			if(n==0)
			{
				n=s;
				s=0;
			}
			int mod=n%10;
			s=s+(int)Math.pow(mod, 2);
			n/=10;
		}
		System.out.println(s);
		if(s==1) System.out.println("yes");
		else System.out.println("no");

	}

}
