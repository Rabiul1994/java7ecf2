package com.jsp.pack2;

import java.util.Scanner;

public class AutomorphicNo1 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number ");
		int num=sc.nextInt();
		int mod,p;
		int i=1;
		int temp=num;
		while(num>0)
		{
			num=num/10;
			i=i*10;
		}
		p=temp*temp;
		mod=p%i;
		if(temp==mod)
		{
			System.out.println(temp+" is an Automorphic Number");
		}
		else
		{
			System.out.println(temp+" is not an Automorphic Number");
		}

	}

}
