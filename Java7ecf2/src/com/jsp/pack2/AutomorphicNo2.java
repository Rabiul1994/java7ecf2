package com.jsp.pack2;

import java.util.Scanner;

public class AutomorphicNo2 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number ");
		int num=sc.nextInt();
		int temp=num;
		int length=0;
		int sqrNum= (int)Math.pow(num,2);
		while(num>0)
		{
			length++;
			num=num/10;
		}
		int rem= sqrNum % (int)Math.pow(10,length);
		if(temp==rem) 
		{
			System.out.println(temp+" is automorphic");
		}
		else
		{
			System.out.println(temp+ " is not automorphic");
		}
	}

}
