package com.jsp.pack2;

import java.util.Scanner;

public class LCM {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num1:");
		int a=sc.nextInt();
		System.out.print("Enter num2:");
		int b=sc.nextInt();
		
		int lcm=(a>b)?a:b;
		
		while(true)
		{
			if(lcm%a==0 && lcm%b==0)
			{
				System.out.println("LCM="+lcm);
				break;
			}
			lcm++;
		}
	}

}
