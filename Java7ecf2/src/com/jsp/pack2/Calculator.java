package com.jsp.pack2;

import java.util.Scanner;

public class Calculator 
{
	public static void main(String[] args) 
	{
		System.out.println("\nI am Calculator.....Welcome!\n");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Num1 :");
		int num1=sc.nextInt();
		System.out.println("Enter Num2 :");
		int num2=sc.nextInt();
		System.out.println("Enter Choice");
		System.out.println(" + : Addition \n - : Substraction \n * : Multiplication \n / : Division");
		char choice= sc.next().charAt(0);
		switch(choice)
		{
			case '+' : System.out.println(num1+" "+choice+" "+num2+" = "+(num1+num2)); break;
			case '-' : System.out.println(num1+" "+choice+" "+num2+" = "+(num1-num2)); break;
			case '*' : System.out.println(num1+" "+choice+" "+num2+" = "+(num1*num2)); break;
			case '/' :
			{
				if(num2==0)
				{
					System.out.println("Denominator cannot be Zero\nPlease Try Again...");
				}
				else
				{
					System.out.println(num1+" "+choice+" "+num2+" = "+(num1/num2));
				}
				break;
			}
			default : System.out.println("You have entered an invalid choice\nPlease try again...");

		}

	}

}
