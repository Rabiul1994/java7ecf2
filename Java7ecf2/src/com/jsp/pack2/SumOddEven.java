package com.jsp.pack2;

import java.util.Scanner;

public class SumOddEven 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int num=sc.nextInt();
		int mod;
		int sum=0;
		int sum1=0;
		while(num>0)
		{
			mod=num%10;
			if(mod%2==0)
			{
				sum=sum+mod;
			}
			if(mod%2==1)
			{
				sum1=sum1+mod;
			}
			num=num/10;
		}
		System.out.println("Sum of even nos "+sum);
		System.out.println("Sum of odd nos "+sum1);
	}

}
