package com.jsp.HashMap2;

public class Node 
{
	Object key;
	Object value;
	Node next;
	
	
	public Node(Object key, Object value, Node next) {
		super();
		this.key = key;
		this.value = value;
		this.next = next;
	}
}
