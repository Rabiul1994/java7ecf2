package com.jsp.HashMap2;

import java.util.Objects;

public class MyHashMap 
{
	private Node[] a=new Node[16];
	private int count=0;
	
	public void put(Object key, Object value)
	{
		int index= Math.abs(Objects.hashCode(key)) & a.length-1;
		System.out.println(index);
		
		
		if(a[index]==null)
		{
			a[index] = new Node(key, value, null);
			count++;
			return;
		}
		Node curr=a[index];
		Node prev=null;
		while(curr!=null)
		{
			if(curr.key==null || curr.key.equals(key))
			{
				curr.value=value;
				return;
			}
			prev=curr;
			curr=curr.next;
		}
		prev.next=new Node(key, value, null);
		count++;
		
	}
	
	public Object get(Object key)
	{
		int index= Math.abs(Objects.hashCode(key)) & a.length-1;
		Node curr=a[index];
		while(curr!=null)
		{
			if(curr.key==null || curr.key.equals(key))
			{
				return curr.value;
			}
			curr=curr.next;
		}
		return null;
	}
	
	public boolean containsKey(Object key)
	{
		int index= Math.abs(Objects.hashCode(key)) & a.length-1;
		Node curr=a[index];
		while(curr!=null)
		{
			if(curr.key==null || curr.key.equals(key))
			{
				return true;
			}
			curr=curr.next;
		}
		return false;
	}
	
	public boolean containsValue(Object value)
	{
		if(a!=null && size()>0)
		{
			for(int i=0;i<a.length;i++)
			{
				for(Node curr=a[i]; curr!=null; curr=curr.next)
				{
					if(curr.value==value || (curr.value!=null && curr.value.equals(value)))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public void remove(Object key)
	{
		int index= Math.abs(Objects.hashCode(key)) & a.length-1;
		if(a[index]==null) return;
		
		if(a[index].key==null || a[index].key.equals(key)) 
		{
			a[index]=a[index].next;
			count--;
			return;
		}
		
		Node curr=a[index];
		Node prev=null;
		while(curr!=null)
		{
			
			if(curr.key==null || curr.key.equals(key))
			{
				prev.next=curr.next;
				count--;
				return;
			}
			prev=curr;
			curr=curr.next;
		}
	}

	public int size() { return count; }

	
	
	
	
	
	
}
