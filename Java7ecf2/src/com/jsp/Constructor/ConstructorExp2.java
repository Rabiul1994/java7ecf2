package com.jsp.Constructor;

class DemoC2
{
	int i,j;

	public DemoC2(int i, int j) 
	{
		this.i = i;
		this.j = j;
		System.out.println(i+" "+j);
	}
	
}
public class ConstructorExp2 {

	public static void main(String[] args) 
	{
		new DemoC2(25,36);
		
	}

}
