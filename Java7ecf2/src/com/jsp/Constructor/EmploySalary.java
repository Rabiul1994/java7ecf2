package com.jsp.Constructor;

class ESalary
{
	String name;
	int age;
	int id;
	double sal;
	static String c_name="Jspiders";
	
	public ESalary(String name, int age, int id, double sal) 
	{
		this.name = name;
		this.age = age;
		this.id = id;
		this.sal=sal;
	}
	public ESalary(ESalary e1, ESalary e2, ESalary e3, ESalary e4, ESalary e5) 
	{
		if(e1.age>25)	
			{
			this.sal=e1.sal*1.1;
			this.name=e1.name;
			this.age=e1.age;
			this.id=e1.id;
			display();
			}
		if(e2.age>25)	
			{
			this.sal=e2.sal*1.1;
			this.name=e2.name;
			this.age=e2.age;
			this.id=e2.id;
			display();
			}
		if(e3.age>25)	
			{
			this.sal=e3.sal*1.1;
			this.name=e3.name;
			this.age=e3.age;
			this.id=e3.id;
			display();
			}
		if(e4.age>25)	
			{
			this.sal=e4.sal*1.1;
			this.name=e4.name;
			this.age=e4.age;
			this.id=e4.id;
			display();
			}
		if(e5.age>25)	{
			this.sal=e5.sal*1.1;
			this.name=e5.name;
			this.age=e5.age;
			this.id=e5.id;
			display();
		}
		
		
	}
	public void display()
	{
//		if(this.age>25) this.sal=sal*1.1;
			
		System.out.println("Employee Id: "+id+"\n"+"Employee Name: "+name+"\n"
				+"Employee age: "+age+"\n"
				+"Employee Company: "+c_name+"\n"
				+"Salary: "+sal);
		System.out.println("--------------------------");
	}
	
}



public class EmploySalary {

	public static void main(String[] args) 
	{
		ESalary e1=new ESalary("ravi",23,101,10000);
		ESalary e2=new ESalary("rakesh",25,102,10000);
		ESalary e3=new ESalary("saddam",26,103,10000);
		ESalary e4=new ESalary("rocky",28,104,10000);
		ESalary e5=new ESalary("ritik",29,105,10000);
		e1.display();
		e2.display();
		e3.display();
		e4.display();
		e5.display();
		System.err.println("If age>25, Increase Salary by 10%");
		System.out.println("--------------------------");
		new ESalary(e1,e2,e3,e4,e5);
	}

}
