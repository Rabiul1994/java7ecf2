package com.jsp.Constructor;



class Languages1
{
	int s,e;

	public Languages1(int s, int e)
	{
		this.s = s;
		this.e = e;
	}
}

class PCMB1
{
	int p,c,m,b;

	public PCMB1(int p, int c, int m, int b) {
		this.p = p;
		this.c = c;
		this.m = m;
		this.b = b;
		
	}
	public void calculate(Languages1 l)
	{
		int t_marks=this.p+this.c+this.m+this.b+l.e+l.s;
		System.out.println("Total="+t_marks);
		
	}
	public void display(Languages1 l)
	{
		System.out.println("Physics="+this.p+"\n"
				+"Chemistry="+this.c+"\n"
				+"Maths="+this.m+"\n"
				+"Biology="+this.b+"\n"
				+"English"+l.e+"\n"
				+"sanskrit"+l.s);
	}
	public void average(Languages1 l)
	{
		int avg=(this.p+this.c+this.m+this.b+l.e+l.s)/6;
		System.out.println("Average="+avg);
	}
}

public class Markcard {

	public static void main(String[] args) 
	{
		PCMB1 s1=new PCMB1(70,80,90,77);
		Languages1 l=new Languages1(74,84);

		s1.display(l);
		s1.calculate(l);
		s1.average(l);
		
		
		
	}

}
