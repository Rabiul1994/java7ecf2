package com.jsp.Constructor;

class CcRectangle
{
	int length;
	int breadth;
	
	public CcRectangle(int length,int breadth)
	{
		this.length=length;
		this.breadth=breadth;
	}
	
	public void area()
	{
		System.out.println(length*breadth);
	}
	
	public CcRectangle(CcRectangle r)
	{
		this.length=r.length;
		this.breadth=r.breadth;
	}
	public CcRectangle(CcRectangle rx,CcRectangle ry)
	{
		this.length=rx.length*ry.breadth;
		this.breadth=rx.breadth*ry.length;
		
	}

}

public class RectangleCc {

	public static void main(String[] args)
	{
		CcRectangle r1=new CcRectangle(4,5);
		r1.area();
		CcRectangle r2=new CcRectangle(11,10);
		r2.area();
		CcRectangle r3=new CcRectangle(40,2);
		r3.area();
		CcRectangle r4=new CcRectangle(r1);
		r4.area();
		
		CcRectangle r5=new CcRectangle(r1,r3);
		r5.area();
		CcRectangle r6=new CcRectangle(r1,r2);
		r6.area();
	}

}
