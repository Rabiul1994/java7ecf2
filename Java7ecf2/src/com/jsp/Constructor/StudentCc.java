package com.jsp.Constructor;

class StudentC
{
	int id,age;
	String name;
	public StudentC(int id, int age, String name) 
	{
		this.id = id;
		this.age = age;
		this.name = name;
	}
	public void display()
	{
		System.out.println("S Id: "+id+"\n"
				+"S age: "+age+"\n"
				+"S name: "+name);
		System.out.println("-----------------");
		
	}
	public StudentC(int id,StudentC a,String name)
	{
		this.id=id;
		this.age=a.age;
		this.name=name;
	}
	
	
	
	
}

public class StudentCc {

	public static void main(String[] args) 
	{
		StudentC s1=new StudentC(1,23,"ravi");
		s1.display();
		StudentC s2=new StudentC(2,s1,"ramu");
		s2.display();
		
		
	}

}
