package com.jsp.Constructor;

class Employ
{
	String name;
	int age;
	int id;
	static String c_name="Jspiders";
	
	public Employ(String name, int age, int id) 
	{
		this.name = name;
		this.age = age;
		this.id = id;
	}
	public void display()
	{
		System.out.println("Employee Id: "+id+"\n"+"Employee Name: "+name+"\n"
				+"Employee age: "+age+"\n"
				+"Employee Company: "+c_name);
		System.out.println("--------------------------");
	}
	public Employ(String n,Employ a,int idd)
	{
		name=n;
		age=a.age;
		id=idd;
	}
}
public class CopyCEmployee {

	public static void main(String[] args) 
	{
		Employ e1=new Employ("Radha",24,101);
		Employ e2=new Employ("sharukh",e1,102);
		e1.display();
		e2.display();
		
	}

}
