package com.jsp.Constructor;

import java.util.Scanner;

class Fibonacci
{
	int n1;
	int n2;
	int sum;
	
	public Fibonacci(int n)
	{
		n1=0;n2=1;
		System.out.print(n1+" "+n2+" ");
		while(n-2>0)
		{
			sum=n1+n2;
			System.out.print(sum+" ");
			n1=n2;
			n2=sum;
			n--;
		}
		
	}
}

public class FibonacciS {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter range: ");
		int n=sc.nextInt();
		new Fibonacci(n);
	}

}
