package com.jsp.Constructor;

class DemoC
{
	int i;
	
	public DemoC()
	{
		i=45;
		System.out.println(i);
	}
}

public class ConstructorExp 
{
	public static void main(String[] args)
	{
		new DemoC();
	}

}
