package com.jsp.Constructor;

class Ccexp
{
	int age;
	String name1;
	int id;
	
	
	public Ccexp(String name1,int id,int age)
	{
		this.age=age;
		this.name1=name1;
		this.id=id;
	}

	public Ccexp(String name1,int id,Ccexp a)
	{
		this.age=a.age;
		this.name1=name1;
		this.id=id;
		
	}
	public void display()
	{
		System.out.println("Id: "+id);
		System.out.println("Student name: "+name1);
		System.out.println("Age: "+age);
		System.out.println("--------------");
	}
	
}


public class CopyConstructor1 {

	public static void main(String[] args) 
	{
		Ccexp a1=new Ccexp("ravi",1,20);
		Ccexp a3=new Ccexp("chacha",3,23);
		Ccexp a2=new Ccexp("ramu",2,a1);
		a1.display();
		a2.display();
		a3.display();
	}

}
