package com.jsp.Constructor;

import java.util.Scanner;

class Fact
{
	int n,fact;
	public Fact(int n,int fact)
	{
		this.fact=fact;
		this.n=n;
		for(int i=1;i<=this.n;i++)
		{
			this.fact*=i;
		}
		System.out.println("The Factorial of "+this.n+" is "+this.fact);
	}
	
	public void display()
	{
		new Fact(4,1);
	}
}


public class Factorial 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number: ");
		int n=sc.nextInt();
		Fact s1=new Fact(n,1);
		s1.display();
		
	}

}
