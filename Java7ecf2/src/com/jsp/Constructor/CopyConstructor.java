package com.jsp.Constructor;

class Cc
{
	int i;
	
	public Cc(int i)
	{
		this.i=i;
	}
	
	public Cc(Cc s)
	{
		this.i=s.i;
	}
}

public class CopyConstructor {

	public static void main(String[] args) 
	{
		Cc s1=new Cc(20);
		Cc s2=new Cc(s1);
		System.out.println(s1.i);
		System.out.println(s2.i);
		
		System.out.println(s1==s2);
	}

}
