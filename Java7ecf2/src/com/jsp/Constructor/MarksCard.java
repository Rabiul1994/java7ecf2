package com.jsp.Constructor;


class PCMB
{
	int p,c,m,b;

	public PCMB(int p, int c, int m, int b) {
		this.p = p;
		this.c = c;
		this.m = m;
		this.b = b;
		
	}
	public int calculate()
	{
		Languages l=new Languages();
		int res=l.lang(74,84);
		int t_marks=this.p+this.c+this.m+this.b+res;
		return t_marks;
		
	}
	public void display()
	{
		System.out.println("Physics="+this.p+"\n"
				+"Chemistry="+this.c+"\n"
				+"Maths="+this.m+"\n"
				+"Biology="+this.b);
		int f=calculate();
		System.out.println("Total Marks="+f);
		
		
		
	}
}
class Languages
{
	int s,e;

	public int lang(int s, int e) {
		this.s = s;
		this.e = e;
		int sum=this.s+this.e;
		System.out.println("Sanskrit="+this.s+"\n"
				+"English="+this.e);
		
		return sum;
	}
	
}


public class MarksCard {

	public static void main(String[] args) 
	{
		PCMB s1=new PCMB(70,80,90,77);
		
		s1.display();
		
	}

}
