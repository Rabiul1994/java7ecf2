package com.jsp.Constructor;

class Call
{
	
	public Call()
	{
		this(10);
		System.out.println("no arg constructor");
	}
	public Call(int i)
	{
		this(2,5);
		System.out.println("1 arg constructor "+i);
	}
	public Call(int i,int j)
	{
		System.out.println("2 arg constructor "+i+" "+j);
		
	}
}

public class ThisCallToCon {

	public static void main(String[] args) 
	{
		new Call();

	}

}
