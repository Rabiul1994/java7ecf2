package com.jsp.pack3;

import java.util.Scanner;

public class TestBill {

	public static void main(String[] args) 
	{
		System.out.println("WELCOME TO FOODIE JAVA\n");
		String s1="Coffee"; String s2="Tea"; String s3="Milk"; String s4="Dosa"; String s5="Masala Dosa";
		int amt1=30; int amt2=20; int amt3=15; int amt4=40; int amt5=60;
		int t_away=0;
		double amt=0;
		double c_gst,s_gst,f_amt,bal;
		Scanner sc=new Scanner(System.in);
		while(true) {
		System.out.println("Menu Card:\n\n1. Coffee------30.00\n2. Tea---------20.00\n3. Milk--------15.00\n4. Dosa--------40.00\n5. Masala Dosa-60.00");
		
		System.out.println("Please place your order...");
		
		
		int n=sc.nextInt();
		if(n==1)
		{
			System.out.print("Cups of Coffee?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s1+" "+n1+" Amount: Rs."+(n1*amt1));
			amt=amt+(n1*amt1);
			System.out.println("Total: "+amt);
		}
		if(n==2)
		{
			System.out.print("Cups of Tea?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s2+" "+n1+" Amount: Rs."+(n1*amt2));
			amt=amt+(n1*amt2);
			System.out.println("Total: "+amt);
		}
		if(n==3)
		{
			System.out.print("Glass of Milk?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s3+" "+n1+" Amount: Rs."+(n1*amt3));
			amt=amt+(n1*amt3);
			System.out.println("Total: "+amt);
		}
		if(n==4)
		{
			System.out.print("Plates of Dosa?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s4+" "+n1+" Amount: Rs."+(n1*amt4));
			amt=amt+(n1*amt4);
			System.out.println("Total: "+amt);
		}
		if(n==5)
		{
			System.out.print("Plates of Masala Dosa?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s5+" "+n1+" Amount: Rs."+(n1*amt5));
			amt=amt+(n1*amt5);
			System.out.println("Total: "+amt);
		}

		System.out.println("\n\nWould you like any other item?\nPress Y to continue\nPress N to exit");
		char ch=sc.next().charAt(0);
		if(ch=='Y'||ch=='y') continue;
		else break;
		}
		System.out.println("Would you like to DINW-IN or TAKE-AWAY?");
		System.out.println("Press 1: DINE-IN\nPress 2: TAKE-AWAY");
		int n2=sc.nextInt();
		if(n2==2)
		{
			t_away=t_away+15;
		}
		
		System.out.println("Order Total:\n");
		c_gst=(6.0/100.0)*amt;
		s_gst=(6.0/100.0)*amt;
		f_amt=amt+c_gst+s_gst+t_away;
		System.out.println("Total:     - Rs."+amt+"\nC_GST @ 6% - Rs."+c_gst+"\ns_gst @ 6% - Rs."+s_gst+"\nT_awy chg: - Rs."+t_away+"\nFinal Amt: - Rs."+f_amt);
		System.out.println("--------------------------------");
		System.out.println("Round-off  - Rs."+(int)f_amt);
		
		System.out.print("Enter the cash amount: Rs.");
		int c_amt=sc.nextInt();
		if(c_amt>=(int)f_amt)
		{
			bal=c_amt-(int)f_amt;
			System.out.println("Please collect your balance: Rs."+(int)bal);
			System.out.println("Thank You...Visit Again");
		}
		else {
			bal=(int)f_amt-c_amt;
			System.out.println("Kindly pay Rs."+(int)bal+" more.");
			System.out.print("Pay remaining amount: Rs.");
			int n3=sc.nextInt();
			if(n3>=(int)bal) System.out.println("Thank You...Visit Again");
			else System.err.println("You are a bad Customer..Don't Visit Again");
		}
		
		
	}

}
