package com.jsp.pack3;

import java.util.Scanner;

public class RestBill {

	public static void main(String[] args) 
	{
		System.out.println("WELCOME TO FOODIE JAVA\n");
		String s1="Coffee"; String s2="Tea"; String s3="Badam Milk"; String s4="Dosa"; String s5="Masala Dosa"; String s6="Chicken Kabab";
		String s7="Chicken Biryani"; String s8="Mutton Kabab"; String s9="Mutton Biryani"; String s10="Egg Biryani";
		int amt1=30; int amt2=20; int amt3=35; int amt4=40; int amt5=60; int amt6=100; int amt7=120; int amt8=130; int amt9=150; int amt10=80;
		int qty1=0,qty2=0,qty3=0,qty4=0,qty5=0,qty6=0,qty7=0,qty8=0,qty9=0,qty10=0; int t1=0,t2=0,t3=0,t4=0,t5=0,t6=0,t7=0,t8=0,t9=0,t10=0; int rf=0;
		int t_away=0;
		double amt=0;
		double gst,f_amt,bal;
		Scanner sc=new Scanner(System.in);
		while(true) {
			System.out.println("1. Vegetarian\n2. Non-Vegetarian");
			int c=sc.nextInt();
			if(c==1) {
		System.out.println("Menu Card:\n\n1. "+s1+"------30.00\n2. "+s2+"---------20.00\n3. "+s3+"--35.00\n4. "+s4+"--------40.00\n5. "+s5+"-60.00");
		
		System.out.print("Select number to place your order:"); //veg ordering section
		
		
		int n=sc.nextInt();
		if(n==1)
		{
			System.out.print("Cups of Coffee?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s1+" "+n1+" Amount: Rs."+(n1*amt1));
			amt=amt+(n1*amt1);
			qty1=qty1+n1;
			t1=amt1*qty1;
			System.out.println("Total: "+amt);
		}
		else if(n==2)
		{
			System.out.print("Cups of Tea?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s2+" "+n1+" Amount: Rs."+(n1*amt2));
			amt=amt+(n1*amt2);
			qty2=qty2+n1;
			t2=amt2*qty2;
			System.out.println("Total: "+amt);
		}
		else if(n==3)
		{
			System.out.print("Glasses of Badam Milk?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s3+" "+n1+" Amount: Rs."+(n1*amt3));
			amt=amt+(n1*amt3);
			qty3=qty3+n1;
			t3=amt3*qty3;
			System.out.println("Total: "+amt);
		}
		else if(n==4)
		{
			System.out.print("Plates of Dosa?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s4+" "+n1+" Amount: Rs."+(n1*amt4));
			amt=amt+(n1*amt4);
			qty4=qty4+n1;
			t4=amt4*qty4;
			System.out.println("Total: "+amt);
		}
		else if(n==5)
		{
			System.out.print("Plates of Masala Dosa?");
			int n1=sc.nextInt();
			System.out.println("Order:"+s5+" "+n1+" Amount: Rs."+(n1*amt5));
			amt=amt+(n1*amt5);
			qty5=qty5+n1;
			t5=amt5*qty5;
			System.out.println("Total: "+amt);
		}
		else System.out.println("Invalid Choice");
			}
			else if(c==2)
			{
				System.out.println("Menu Card:\n\n1. "+s6+"-----------100.00\n2. "+s7+"---------120.00\n3. "+s8+"------------130.00\n4. "+s9+"----------150.00\n5. "+s10+"--------------80.00");
				
				System.out.print("Select number to place your order:"); //non-veg ordering section
				
				
				int n=sc.nextInt();
				if(n==1)
				{
					System.out.print("Plates?");
					int n1=sc.nextInt();
					System.out.println("Order:"+s6+" "+n1+" Amount: Rs."+(n1*amt6));
					amt=amt+(n1*amt6);
					qty6=qty6+n1;
					t6=amt6*qty6;
					System.out.println("Total: "+amt);
				}
				else if(n==2)
				{
					System.out.print("Plates?");
					int n1=sc.nextInt();
					System.out.println("Order:"+s7+" "+n1+" Amount: Rs."+(n1*amt7));
					amt=amt+(n1*amt7);
					qty7=qty7+n1;
					t7=amt7*qty7;
					System.out.println("Total: "+amt);
				}
				else if(n==3)
				{
					System.out.print("Plates?");
					int n1=sc.nextInt();
					System.out.println("Order:"+s8+" "+n1+" Amount: Rs."+(n1*amt8));
					amt=amt+(n1*amt8);
					qty8=qty8+n1;
					t8=amt8*qty8;
					System.out.println("Total: "+amt);
				}
				else if(n==4)
				{
					System.out.print("Plates?");
					int n1=sc.nextInt();
					System.out.println("Order:"+s9+" "+n1+" Amount: Rs."+(n1*amt9));
					amt=amt+(n1*amt9);
					qty9=qty9+n1;
					t9=amt9*qty9;
					System.out.println("Total: "+amt);
				}
				else if(n==5)
				{
					System.out.print("Plates?");
					int n1=sc.nextInt();
					System.out.println("Order:"+s10+" "+n1+" Amount: Rs."+(n1*amt10));
					amt=amt+(n1*amt10);
					qty10=qty10+n1;
					t10=amt10*qty10;
					System.out.println("Total: "+amt);
				}
				else System.out.println("Invalid Choice");
			}
			else System.out.println("Invalid Choice");
		System.out.println("\n\nWould you like to order again?\nPress Y to confirm\nPress N to exit");
		char ch=sc.next().charAt(0);
		if(ch=='Y'||ch=='y') continue;
		else break;
		}
		while(true) {
		if(amt==0.0) {
			System.out.println("You have not ordered!\nThank You..Visit Again");
			break;
		}
		System.out.println("Would you like to DINE-IN or TAKE-AWAY?");
		System.out.println("Press 1: DINE-IN\nPress 2: TAKE-AWAY");
		int n2=sc.nextInt();
		if(n2==2)
		{
			t_away=t_away+15;
		}
		
		System.out.println("\t\t<<<Bill>>>\n");  //Billing Section
		gst=Math.round((0.06*amt)*100)/100.0;
		
		f_amt=Math.round((amt+gst+gst+t_away)*100)/100.0;
		System.out.println("Items\t Qty\t Rate/itm\t  Amnt\n________________________________________");
		
		if(qty1>0) System.out.println("Coffe\t  "+qty1+"\t    "+amt1+"\t\t  "+t1);
		if(qty2>0) System.out.println("Tea  \t  "+qty2+"\t    "+amt2+"\t\t  "+t2);
		if(qty3>0) System.out.println("B Mlk\t  "+qty3+"\t    "+amt3+"\t\t  "+t3);
		if(qty4>0) System.out.println("Dosa \t  "+qty4+"\t    "+amt4+"\t\t  "+t4);
		if(qty5>0) System.out.println("M Dos\t  "+qty5+"\t    "+amt5+"\t\t  "+t5);
		if(qty6>0) System.out.println("C Kbb\t  "+qty6+"\t    "+amt6+"\t\t  "+t6);
		if(qty7>0) System.out.println("C Bir\t  "+qty7+"\t    "+amt7+"\t\t  "+t7);
		if(qty8>0) System.out.println("M Kbb\t  "+qty8+"\t    "+amt8+"\t\t  "+t8);
		if(qty9>0) System.out.println("M Bir \t  "+qty9+"\t    "+amt9+"\t\t  "+t9);
		if(qty10>0) System.out.println("E Bir\t  "+qty10+"\t    "+amt10+"\t\t  "+t10);

		System.out.println("----------------------------------------");
		System.out.println("Total\t\t\t\t  "+amt);
		System.out.println("C_GST@6%\t\t\t   "+gst+"\nS_GST@6%\t\t\t   "+gst);
		if(t_away!=0) System.out.println("T_Away Chrg\t\t\t   "+(double)t_away);
		System.out.println("----------------------------------------");
		System.out.println("T. Amt\t\t\t\tRs."+f_amt);
		
		if(f_amt>=(float)((int)f_amt+0.5)) {
			rf=(int)f_amt+1;
			System.out.println("Rnd-off\t\t\t        Rs."+rf);
		}
		else {
			rf=(int)f_amt;
			System.out.println("Rnd-off\t\t\t        Rs."+rf);
		}
		
		System.out.print("Enter the cash amount: Rs.");
		int c_amt=sc.nextInt();
		if(c_amt==rf) {
			System.out.println("Thank You...Visit Again"); break;
		}
		
		else if(c_amt>rf)
		{
			bal=c_amt-rf;
			System.out.println("Please collect your balance: Rs."+(int)bal);
			System.out.println("Thank You...Visit Again"); 
			break;
		}
		else {
			bal=rf-c_amt;
			System.out.println("Kindly pay Rs."+(int)bal+" more.");
			System.out.print("Pay remaining amount: Rs.");
			int n3=sc.nextInt();
			if(n3>=(int)bal) {
				System.out.println("Thank You...Visit Again"); 
				break;
			}
			else {
				System.err.println("You are a bad Customer..Don't Visit Again");
				break;
				}
			
			}
		
				}
		
	}

}
