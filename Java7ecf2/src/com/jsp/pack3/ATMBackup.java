package com.jsp.pack3;

import java.util.Scanner;

public class ATMBackup
{
	public static void main(String[] args) 
	{
		int pin=1234;
		int bal=40000;
		int c_bal=100000;
		int temp_amt=0;
		int limit_dep=0;
		
		System.err.println("WELCOME TO MY BANK ATM");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter your 4-digit PIN to continue");
		int p=sc.nextInt();
		while(true)
			{
			if(p==pin) 
			{
				System.out.println("Welcome Mr. Joyson");
				System.out.println("Select any one number to proceed:\n1.Balance Enquiry\n2.Cash Withdrawal\n3.Cash Deposit\n4.Transfer");
				int n=sc.nextInt();
				
				if(n==1)
				{
					System.out.println("Saving Balance: Rs."+bal+"\n"
							+"Current Balance: Rs."+c_bal);
				}
				
				if(n==2)
				{	
					
					while(true) {
					System.out.println("Please Enter the amount you want to withdraw");
					int amt=sc.nextInt();
					
					if(amt<=bal)
					{
						if(amt<100) System.err.println("Please enter amount more than 100");
							
						else if(amt>100 && amt%100!=0) System.err.println("Please enter amount in multiples of x100");
						
						else if(amt>100 && amt%100==0 && amt>30000) System.err.println("Daily limit exceeded! Enter amount less than 30000");
						
						else if(amt>100 && amt%100==0 && amt<=30000) 
							{
								temp_amt=temp_amt+amt;
								if(temp_amt>30000) {
									System.err.println("You are exceeding daily withdrawl limit.\nPlease enter lesser amount");
									break;
								}
								System.out.println("Your transaction is successful..Please collect the cash.");
								bal=bal-amt;
								System.out.println("Your remaining balance is: Rs."+bal);
								
							}	
					}
					else
					{
						System.err.println("Insufficient fund!");
					}
				
				System.out.println("Do you want to withdraw again? Press Y to contiue\nPress N to end the transaction");
				int ch=sc.next().charAt(0);
				if(ch=='y'||ch=='Y') continue;
					
				else break;
				}
				}
				
				if(n==3)
				{
					while(true) {
					
					System.out.println("Enter the amount you want to deposit");
					int dep=sc.nextInt();
					
					if(dep>50000) System.err.println("Daily deposit limit exceeded.");
					
					else if(dep<100) System.err.println("Minimum deposit amount is Rs.100");
					
					else if(dep>100 && dep%100!=0) System.err.println("Please enter amount in multiples of x100");
					
					else if(dep>100 && dep%100==0 && dep<=50000) 
						{
						System.out.println("You want to deposit: Rs."+dep+"\nEnter the Denomination:");
						System.out.print("2000X");
						int x2000=sc.nextInt();
						System.out.print("500X");
						int x500=sc.nextInt();
						System.out.print("200X");
						int x200=sc.nextInt();
						System.out.print("100X");
						int x100=sc.nextInt();
						
						limit_dep=limit_dep+dep;
						int tempdep=(x100*100) + (x200*200) + (x500*500) + (x2000*2000);
						if(tempdep!=dep) {
							System.err.println("Denomination entered doesnot match deposit amount.");
							limit_dep=limit_dep-dep;
						}
						else if(limit_dep>50000) {
							System.err.println("You are exceeding daily deposit limit.\nEnter lesser amount");
							break;
						}
						else
							{
								System.out.println("Your Deposit is successful.");
								bal=bal+dep;
								System.out.println("Your updated balance is: Rs."+bal);
							}
						}
					System.out.println("Do you want to deposit again? Press Y to contiue\nPress N to end the transaction");
					int ch=sc.next().charAt(0);
					if(ch=='y'||ch=='Y') continue;
					else break;
					}
				}
				if(n==4)
				{
					while(true) {
					System.out.print("Enter Beneficiary A/C No:\n");
					long ac=sc.nextLong();
					System.out.print("Enter Amount:");
					int t_amt=sc.nextInt();
					
					if(t_amt>50000) System.out.println("Daily Transfer Limit is upto Rs.50000");
					else if(t_amt>bal) System.out.println("You dont have enough balance to transfer");
					
					else
					{
						System.out.println("Please check the details before proceeding:\n");
						System.out.println("Beneficiary A/c:"+ac+"\n"
								+"Transfer Amount: Rs."+t_amt);
						
					System.out.println("\nTo Proceed: Press Y\nTo Re-enter: Press R\nTo Exit: Press N");
					char ch=sc.next().charAt(0);
					if(ch=='Y'||ch=='y')
					{
						System.out.println("Select the account:\n1.Saving A/C\n2.Current A/C");
						int ta=sc.nextInt();
						if(ta==1) {
						System.out.println("Transfer Successful");
						bal=bal-t_amt;
						System.out.println("Updated Saving A/C Balance:"+bal);
						break;
						}
						if(ta==2) {
							System.out.println("Transfer Successful");
							c_bal=c_bal-t_amt;
							System.out.println("Updated Current A/C Balance:"+c_bal);
							break;
						}
					}
					else if(ch=='R'||ch=='r') continue;
					else break;
					}
						
					}
					
				}
			}
			else {
				System.err.println("Incorrect PIN"); 
				break;
				}
			System.out.println("Do you want another transaction?..Press Y\nTo end transaction..Press N");
			char ch=sc.next().charAt(0);
			if(ch=='Y'||ch=='y') continue;
			else System.out.println("Thank you for using MY BANK ATM Services..\n\nVisit Again.."); break;
		}
	}
}
