package com.jsp.Serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainClass {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		//Serialize
		
		Student std = new Student();
		
		std.setId(1);
		std.setName("Rabiul");
		std.setAge(28);
		
		
		FileOutputStream fileOutput=new FileOutputStream("D:\\SerializationDemo\\studentInfo.ser");
		ObjectOutputStream oos=new ObjectOutputStream(fileOutput);
		oos.writeObject(std);
		oos.close();
		fileOutput.close();
		
		
		//Deserialize
		
		FileInputStream fileInput=new FileInputStream("D:\\SerializationDemo\\studentInfo.ser");
		ObjectInputStream ois=new ObjectInputStream(fileInput);
		Student readObject = (Student) ois.readObject();
		System.out.println(readObject.getName());
		ois.close();
		fileInput.close();
	}

}
