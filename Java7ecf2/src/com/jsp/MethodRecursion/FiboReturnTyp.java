package com.jsp.MethodRecursion;

class FiboReturn
{
	long n1;
	long n2;
	long s;
	
	
	public FiboReturn(long n1, long n2, long s) 
	{
		this.n1 = n1;
		this.n2 = n2;
		this.s = s;
	}
	public  long fibonacci(int c)
	{
		if(c<=1)return c;
		s=n1+n2;
		n1=n2;
		n2=s;
		fibonacci(c-1);
		return s;
	}
}


public class FiboReturnTyp {

	public static void main(String[] args)
	{
		for(int i=0;i<50;i++) 
		{
			FiboReturn s1=new FiboReturn(0,1,0);
//			FiboReturn.n1=0;FiboReturn.n2=1;FiboReturn.s=0;
			System.out.println(s1.fibonacci(i));
		}
	}
}

