package com.jsp.MethodRecursion;

class RevRec
{
	static int rev=0;
	public int reverse(int n)
	{
		if(n>0)
		{
			int mod=n%10;
			rev=(rev*10)+mod;
			reverse(n/10);
		}
		return rev;
	}
}

public class ReverseRecursion {

	public static void main(String[] args) 
	{
		RevRec r1=new RevRec();
		int res=r1.reverse(123);
		System.out.println(res);
		
		
	}

}
