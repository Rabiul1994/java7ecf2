package com.jsp.MethodRecursion;

import java.util.Scanner;

class DecToBinRec
{
	static int bin=0;
	public int binary(int i,int d)
	{
		
		if(d>0)
		{
			int mod=d%2;
			bin=(mod*i)+bin;
			binary(i*10,d/2);
		}
		return bin;
	}
	
}

class BinToDec
{
	static int dec=0;
	public int decimal(int i,int bin)
	{
		if(bin>0)
		{
			int mod=bin%10;
			dec=(mod*i)+dec;
			decimal(i*2,bin/10);
		}
		return dec;
	}
}

public class Conversion {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter decimal no: ");
		int dec=sc.nextInt();
		DecToBinRec b1=new DecToBinRec();
		int res=b1.binary(1, dec);
		System.out.println(res);
		
		System.out.println("-----------------------");
		BinToDec d1=new BinToDec();
		System.out.println("Enter Binary No: ");
		int bin=sc.nextInt();
		int res1=d1.decimal(1, bin);
		System.out.println(res1);
		
	}

}
