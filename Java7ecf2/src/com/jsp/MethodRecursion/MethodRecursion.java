package com.jsp.MethodRecursion;


class Recursion
{

	public int fact(int n)
	{
		if(n==0||n==1) return 1;
		return n*fact(n-1);
	}
	
	public int sum(int n)
	
	{
		if(n==0) return 0;
		return n+sum(n-1);
	}
	
	public int power(int b,int p)
	{
		if(p==0) return 1;
		return b*power(b,p-1);
	}
	
	public void fibonacci(int n1,int n2,int c)
	{
		
		if(c>0){
		int	s=n1+n2;
			n1=n2;
			n2=s;
			System.out.println(s);
			fibonacci(n1,n2,c-1);
		}
			
	}
	
}

public class MethodRecursion {

	public static void main(String[] args) 
	{
		Recursion r1= new Recursion();
		int res=r1.fact(5);
		System.out.println(res);
		Recursion r2= new Recursion();
		int res2=r2.sum(10);
		System.out.println(res2);
		Recursion r3= new Recursion();
		int res3=r3.power(2,3);
		System.out.println(res3);
		Recursion r4= new Recursion();
		int n1=0,n2=1;
		System.out.println(n1);
		System.out.println(n2);
		int c=10;
		r4.fibonacci(n1,n2,c-2);
		
	}

}
