package com.jsp.MethodRecursion;

import java.util.Scanner;

class Prime
{
	public boolean isPrime(int n,int i)
	{
		if(i==1) return true;
		if(n%i==0) return false;
		return isPrime(n,i-1);
		
	}
}

class PrimeRange
{
	public boolean isPrime(int n,int i)
	{
		if(i==1) return true;
		if(n%i==0) return false;
		return isPrime(n,i-1);
		
	}
}

public class PrimeNo {

	public static void main(String[] args) 
	{
		Prime p1=new Prime();
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num: ");
		int n=sc.nextInt();
		if(n==0||n==1) System.out.println("Prime");
		else {
		
		if(p1.isPrime(n,n/2)) System.out.println("Prime");
		else System.out.println("Not prime");
	}
		PrimeRange p=new PrimeRange();
		System.out.println("Enter Range: ");
		int r=sc.nextInt();
		for(int i=2;i<=r;i++)
		{
//			if(i==1) System.out.println(i);
			if(p.isPrime(i,i/2)) System.out.println(i);
		}
	}
}
