package com.jsp.MethodRecursion;

class NewFibo
{
	public int fibo(int n)
	{
		if(n<=1) return n;
		return fibo(n-1)+fibo(n-2);
	}
}

public class NewTestFibo {

	public static void main(String[] args) 
	{
		NewFibo s1=new NewFibo();
		for(int i=0;i<=50;i++)
		{
		System.out.println(s1.fibo(i));
	}
}
}