package com.jsp.HashMap;

public class Value 
{
	private Object value;

	public Value(Object value) {
		this.value = value;
	}
	
//	public int getValue() { return value; }
	
	@Override
	public boolean equals(Object arg) {
		if(!(arg instanceof Value)) return false;
		Value v1=(Value)arg;
		return value.equals(v1.value);
	}

	@Override
	public String toString() {
		return "Value [value=" + value + "]";
	}
	
	
	
}
