package com.jsp.HashMap;

import java.util.LinkedList;

public class MyHashMap 
{
	LinkedList<Node>[] hashMap= new LinkedList[4];
	
	int count=0;
	
	public MyHashMap() { }
	
	public void put(Key key, Value value)
	{
//		if(count>=hashMap.length) resize();
		
		System.out.println(key);  //checking
		System.out.println(key.hashCode());  //checking
		
		int index= getIndex(key) & hashMap.length-1;
		System.out.println(index);  //checking
		
		if(hashMap[index]==null)
		{
			hashMap[index]=new LinkedList<>();
			
			hashMap[index].add(new Node(key,value));
			count++;
			return;
		}
		else
		{
			for(Node n:hashMap[index])
			{
				if(n.key.equals(key))
				{
					n.value=value;
					return;
				}
			}
			
			hashMap[index].add(new Node(key,value));
			count++;
		}
		
	}
	
	public int size() { return count; }
	
	public Value get(Key key)
	{
		int index= getIndex(key) & hashMap.length-1;
		
		if(hashMap[index]==null) return null;
		
		for(Node n:hashMap[index])
		{
			if(n.key.equals(key)) return n.value;
		}
		return null;
	}
	
	public void remove(Key key)
	{
		if(key==null) return;
		
		int index= getIndex(key) & hashMap.length-1;
		
		if(hashMap[index]==null) return;
		
		Node curr=null;
		for(Node n:hashMap[index])
		{
			if(n.key.equals(key))
			{
				curr=n;
				break;
			}
		}
		if(curr==null) return;
		hashMap[index].remove(curr);
		count--;
	}
	
	public boolean containsKey(Key key)
	{
		if(key==null) return false;
		
		int index= getIndex(key) & hashMap.length-1;
		
		if(hashMap[index]==null) return false;
		
		for(Node n: hashMap[index])
		{
			if(n.key.equals(key)) return true;
		}
		return false;
	}
	
	private void resize()
	{
		LinkedList<Node>[] temp= hashMap;
		hashMap=new LinkedList[count+2];
		count=0;
		
		for(int i=0;i<temp.length;i++)
		{
			if(temp[i]==null) continue;
			
			for(Node n:temp[i])
			{
				put(n.key, n.value);
			}
		}
	}
	
	private int getIndex(Key key) { return Math.abs(key.hashCode()); }
	
}

