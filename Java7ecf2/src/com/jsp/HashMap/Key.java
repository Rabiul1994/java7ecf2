package com.jsp.HashMap;

import java.util.Objects;

public class Key 
{
	private Object key;

	public Key(Object key) {
		this.key = key;
	}
	
	@Override
	public boolean equals(Object arg) {
		if(!(arg instanceof Key)) return false;
		Key k1=(Key)arg;
		return Objects.equals(key, k1.key);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(key);     //or key.hashCode()
	}
	
}
