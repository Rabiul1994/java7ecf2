package com.jsp.HashMap;

public class Node 
{
	public Key key;
	public Value value;
	
	public Node(Key key, Value value) {
		this.key = key;
		this.value = value;
	}
	
}
