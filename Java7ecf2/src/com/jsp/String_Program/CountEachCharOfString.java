package com.jsp.String_Program;

import java.util.Scanner;

public class CountEachCharOfString {

	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String:");  //developer
		String s1=sc.next();
		s1=s1.toLowerCase();
		while(s1.length()>0)
		{
			char c=s1.charAt(0);
			String s2=s1.replace(c+"", "");
			int n=s1.length()-s2.length();
			System.out.println(c+" -> "+n);
			s1=s2;
		}
	}
}
