package com.jsp.String_Program;

public class Demo2 
{

	public static void main(String[] args) 
	{
		String s1="ravi";
		char[] a=s1.toCharArray();
		for(int i=0;i<s1.length();i++)
		{
			System.out.println(s1.charAt(i)); //r a v i
		}
		System.out.println("-----------------");
		for(int i=s1.length()-1;i>=0;i--)
		{
			System.out.println(s1.charAt(i));// i v a r
		}
		System.out.println("-----------------");
		for(char ch:a)
		{
			System.out.println(ch); //ravi  (always from 1st to last)
		}
	}

}
