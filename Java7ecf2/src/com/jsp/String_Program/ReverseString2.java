package com.jsp.String_Program;

public class ReverseString2 //reverse string using toCharArray()
{
	public static String reverse(String arg)
	{
		char[] a=arg.toCharArray();
		int i=0, j=a.length-1;
		while(i<j)
		{
			char temp=a[i];
			a[i]=a[j];
			a[j]=temp;
			i++;
			j--;
		}
		String s1=new String(a);
		return s1;
	}

	public static void main(String[] args) 
	{
		System.out.println(reverse("ravi"));
	}

}
