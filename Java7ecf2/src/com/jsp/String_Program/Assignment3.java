package com.jsp.String_Program;

public class Assignment3 { //i/p java is easy  o/p Java Is Easy

	public static String properCase(String s)
	{
		return s.substring(0, 1).toUpperCase()+s.substring(1).toLowerCase();
	}
	
	public static void main(String[] args) 
	{
		String s1="java is easy";
		String[] a=s1.split(" ");
		String s2="";
		for(String s:a)
		{
			s2+=properCase(s);
			s2+=" ";
		}
		System.out.println(s2);
		
	}

}
