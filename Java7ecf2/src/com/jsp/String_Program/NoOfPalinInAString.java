package com.jsp.String_Program;

/**
 * For finding the biggest palindrome in a string,
 * uncomment the commented lines
 *
 */
public class NoOfPalinInAString {

	public static void main(String[] args) 
	{
		String s1="chocolate";
//		boolean flag=false;
		s1=s1.toLowerCase();
		for(int i=0;i<s1.length()-1;i++)
		{
			for(int j=i+2;j<=s1.length();j++)
			{
				String s2=s1.substring(i,j);
				if(isPallindrome(s2))
				{
					System.out.println(s2);
//					flag=true;
//					break;
				}
				
			}
//			if(flag) break;
		}
	}
	
	public static boolean isPallindrome(String s)
	{
		int i=0, j=s.length()-1;
		while(i<j)
		{
			if(s.charAt(i)!=s.charAt(j)) return false;
			i++;
			j--;
		}
		return true;
	}
}
