package com.jsp.String_Program;

import java.util.Stack;

public class BalancedString 
{
	public static boolean isBalanced(String s)
	{
		Stack<Character> st=new Stack<Character>();
		for(int i=0;i<s.length();i++)
		{
			char c=s.charAt(i);
			if(c=='['||c=='{'||c=='(') st.push(c);
			else if(c==']'||c=='}'||c==')')
			{
				if(st.isEmpty()||!pair(st.pop(),c)) return false;
					
			}
		}
		 return st.isEmpty();
	}
	
	public static boolean pair(char c1,char c2)
	{
		if(c1=='['&&c2==']') return true;
		if(c1=='{'&&c2=='}') return true;
		if(c1=='('&&c2==')') return true;
		return false;
	}
	public static void main(String[] args) 
	{
		System.out.println(isBalanced("{sd[saa]asda(saa[aas{asfa}saa]s)}"));
	}

}
