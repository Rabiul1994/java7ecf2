package com.jsp.String_Program;

public class StringProgram1 //I/P- aaabbcccdee o/p- 3a2b3c1d2e
{
	public static void main(String[] args) 
	{
		String s1="aaabbcccdee";
		String t="";
		while(s1.length()>0) 
		{
			char c=s1.charAt(0);
			String s2=s1.replace(c+"", "");
			int n=s1.length()-s2.length();
			t+=n+""+c;
			s1=s2;
		}
		System.out.println(t);
	}

}
