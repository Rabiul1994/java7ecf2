package com.jsp.String_Program;

import java.util.Scanner;

public class CountPerticularLetterOfString //count 'e' in string
{

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String:");
		String s1=sc.next();
		s1=s1.toLowerCase();
		System.out.println("Enter the character you want to count:");
		String c=sc.next();
		String s2=s1.replace(c, "");
		int n=s1.length()-s2.length();
		System.out.println(c+"-> "+n);
		
		//OR
		int n1=s1.length()-s1.replace(c, "").length();
		System.out.println(c+"-> "+n1);
	}

}
