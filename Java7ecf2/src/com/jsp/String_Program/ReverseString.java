package com.jsp.String_Program;

public class ReverseString //reverse string using loop
{
	public static String reverse(String arg)
	{
		String s1="";
		for(int i=arg.length()-1; i>=0;i--) {
			s1=s1+arg.charAt(i);
		}
		return s1;
	}

	public static void main(String[] args) 
	{
		System.out.println(reverse("Ravi"));
	}

}
