package com.jsp.String_Program;

public class IndexOf 
{
	public static void main(String[] args)
	{
		String s1="jspiders";
		System.out.println(s1.indexOf('r')); //6
		System.out.println(s1.indexOf("pid")); //2
		System.out.println(s1.indexOf('s')); //1
		System.out.println(s1.indexOf('a')); //-1
		
		String s2="developer";
		System.out.println(s2.indexOf('e', 2)); //3
		System.out.println(s2.indexOf('o', 6)); //-1
		
		//Find index dynamically of the second 'c' in a string
		
		String s3="success";
		int i=s3.indexOf('c'); 
		int j=s3.indexOf('c', i+1);
		System.out.println(j);
		
		//OR//
		
		int n=s3.indexOf('c', s3.indexOf('c')+1);
		System.out.println(n);
		
		String s4=s1+s2;
		System.out.println(s4.indexOf(s2)); //8
		
		
		
		
	}

}
