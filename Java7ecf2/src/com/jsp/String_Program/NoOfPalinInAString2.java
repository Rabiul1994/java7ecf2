package com.jsp.String_Program;

public class NoOfPalinInAString2 
{
	public static void main(String[] args) 
	{
		String s1="malayalam";
		StringBuilder s3=new StringBuilder(s1);
		while(s3.length()>0) {
		for(int j=s3.length()-2;j>=0;j--)
			{
				String s2=s3.substring(j);
				if(isPallindrome(s2))
				System.out.println(s2);
			}
		s3.deleteCharAt(s3.length()-1);
		}
	}
	
	public static boolean isPallindrome(String s)
	{
		char[] c=s.toCharArray();
		int i=0, j=c.length-1;
		while(i<j)
		{
			if(c[i]!=c[j]) return false;
			i++;
			j--;
		}
		return true;
	}
}
