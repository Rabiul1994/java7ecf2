package com.jsp.String_Program;

public class Demo1 
{
	public static void main(String[] args) 
	{
		Demo1 s=new Demo1();
		String s1=new String("RAVI");
		String s2="ravi";
		String x="";
		char[] a= {'a','b','c'};
		String s3=new String(a);
		System.out.println(s1); //Ravi
		System.out.println(s2); //Ravi
		System.out.println(s1==s2); //false
		System.out.println(s); //address value
		System.out.println(s3); //array to string
		int c='a';
		System.out.println(c); //ASCII value
		System.out.println(s2.length());
		System.out.println(s2.charAt(2));
		System.out.println(s1.equals(s2));
		System.out.println(s1.equalsIgnoreCase(s2));
		System.out.println(s1.toLowerCase());
		System.out.println(s2.toUpperCase());
		System.out.println(s1.codePointAt(2)+" jj");
		System.out.println(s2.codePointBefore(2));
		System.out.println(s1.codePointCount(0, s1.length()-1));
		System.out.println(s2.compareTo(s1));
		System.out.println(s1.concat(s2));
		System.out.println(x.isEmpty());
		System.out.println(s1.replace('R', 'Z'));
		
		
		
		char[] ar= {'b','c','d'};
		String ss=new String(ar);
//		String ss=ar.toString();
		
		System.out.println(ss);
		
		
	}

}
