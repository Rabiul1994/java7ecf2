package com.jsp.String_Program;

import java.util.Scanner;

public class ReplaceWord {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter sentence:");
		String s1=sc.nextLine();
		int n=s1.indexOf("llo", s1.indexOf("llo")+1);
		String s2=s1.substring(n);
		s2=s2.replace("llo", "xx");
		s1=s1.substring(0, n);
		System.out.println(s1+s2);
	}

}
