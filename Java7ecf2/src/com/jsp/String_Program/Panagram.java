package com.jsp.String_Program;

public class Panagram 
{
	public static boolean isPanagram(String s)
	{
		s=s.toLowerCase();
		for(char c='a';c<='z';c++)
		{
			if(s.indexOf(c)==-1) return false;
		}
		return true;
	}

	public static void main(String[] args) 
	{
		System.out.println(isPanagram("aBcdefghijklmnopqrstuvwxYZ"));
	}

}
