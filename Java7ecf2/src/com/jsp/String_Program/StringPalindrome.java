package com.jsp.String_Program;

public class StringPalindrome 
{
	public static boolean isPalindrome(String arg)
	{
		char[] a=arg.toCharArray();
		int i=0, j=a.length-1;
		while(i<j)
		{
			if(a[i]!=a[j]) return false;
			i++;
			j--;
		}
		return true;
	}

	public static void main(String[] args) 
	{
		System.out.println(isPalindrome("malayalam"));
	}

}
