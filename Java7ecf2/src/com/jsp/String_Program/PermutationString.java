package com.jsp.String_Program;

public class PermutationString 
{
	public static String swap(String s1,int i,int j)
	{
		char[] a=s1.toCharArray();
		char temp=a[i];
		a[i]=a[j];
		a[j]=temp;
		return new String(a);
	}
	
	public static void permutation(String s,int start,int end)
	{
		if(start>=end)
		{
			System.out.println(s);
			return;
		}
		for(int i=start;i<=end;i++)
		{
			String s1=swap(s,start,i);
			permutation(s1,start+1,end);
		}
	}
	public static void main(String[] args) 
	{
		permutation("ab",0,1);  
	}

}
