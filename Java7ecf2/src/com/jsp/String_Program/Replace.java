package com.jsp.String_Program;

public class Replace {

	public static void main(String[] args)
	{
		String s1="developer";
		System.out.println(s1.replace('e', '#')); //d#v#lop#r
		System.out.println(s1.replace("ve", "mo")); //demoloper
		System.out.println(s1.replace("e", " ")); // d v lop r
		System.out.println(s1.replace("e", "")); //dvlopr
		System.out.println(s1); // String is immutable..That is why original string remained same
	}

}
