package com.jsp.String_Program;

public class CountCharacters {

	public static void main(String[] args) {


		String str="abcab";
		char[] a=str.toCharArray();
		
		for(int i=0;i<a.length;i++)
		{
			int count=0;
			boolean flag=false;
			for(int j=0;j<a.length;j++)
			{
				if(j<i && a[i]==a[j])
					{
						flag=true;
						break;
					}
				
				if(a[i]==a[j] && j>=i) count++;
			}
			if(flag) continue;
			System.out.print(count+""+a[i]);
		}
		

	}

}
