package com.jsp.String_Program;

public class MinDistBetTwoWords {

	public static int distance(String s, String w1, String w2)
	{
		if(w1.equals(w2)) return 0;
		
		
		String[] str=s.split(" ");
		int minDist= str.length+1;
		for(int i=0;i<str.length;i++)
		{
			if(str[i].equals(w1))
			{
				for(int j=0;j<str.length;j++)
				{
					if(str[j].equals(w2))
					{
						int d=Math.abs(i-j)-1;
						if(d<minDist) minDist=d;
					}
				}
			}
		}
		return minDist;
	}
	public static void main(String[] args) {

		String s="cloudtech is good for java";
		String w1="good";
		String w2="for";
		System.out.println(distance(s, w1, w2));
	}

}
