package com.jsp.String_Program;

public class CountVowelInString 
{
	public static void main(String[] args) 
	{
		String s1="malaYalam";
		s1=s1.toLowerCase();
		int count=0;
		for(int i=0;i<s1.length();i++)
		{
			char c=s1.charAt(i);
			if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u') count++;
		}
		if(count!=0) System.out.println("Number of vowels="+count);
		else System.out.println("No vowel");
	}
}
