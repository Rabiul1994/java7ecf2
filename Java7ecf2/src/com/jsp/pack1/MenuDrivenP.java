package com.jsp.pack1;
import java.util.Scanner;
public class MenuDrivenP
{
	public static void main(String[] args) 
	{
		while(true)
		{
		System.out.println("Program Menu...");
		Scanner sc=new Scanner(System.in);
		System.out.println(" 1: ArmStrong Number \n 2: Strong Number \n 3: Niven Number \n 4: Automorphic Number \n 5: Palindrome \n 6: Reverse of Number \n 7: Perfect Number \n 8: Prime Number \n 9: Calculator \n 10: Marks Card");
		System.out.print("Enter your choice:");
		int n=sc.nextInt();	
		switch(n)
		{
			case 1 :
			{
				System.out.println("Enter Number to check for Armstrong Number ");
				int num=sc.nextInt();
				int temp=num,num1=num;
				int length=0;
				int sum=0;
				while(num>0)
				{
					length++;
					num/=10;
				}
				while(temp>0)
				{
					int mod=temp%10;
					sum=sum + (int)Math.pow(mod,length);
					temp=temp/10;
				}
				if(num1==sum) System.out.println(num1+ " is ArmStrong Number");
				else System.err.println(num1+ " is not ArmStrong Number");
				System.out.println("Thank You...");
				break;
			}
			case 2 :
			{
				System.out.println("Enter Number to check for Strong number ");
				int num=sc.nextInt();
				int temp=num;
				int i;
				int mod;
				int sum=0;
				while(num>0)
				{
					int fac=1;
					mod=num%10;
					for(i=mod;i>0;i--)
					{
						fac=fac*i;
					}
					sum=sum+fac;
					num=num/10;
				}
				System.out.println("Sum of factorial of each digit is "+sum);
				if(temp==sum) System.out.println(temp+" is a Strong Number");
				else System.err.println(temp+" is not a Strong Number");
				System.out.println("Thank You...");
				break;
			}
			case 3 :
			{
				System.out.println("Enter number to check for Niven Number");
				int num=sc.nextInt();
				int sum=0;
				int temp=num;
				while(num>0)
				{
					int rem=num%10;
					sum+=rem;
					num/=10;
				}
				System.out.println("The Sum is "+sum);
				if(temp%sum==0) System.out.println(temp+" is Niven Number");
				else System.err.println(temp+" is not Niven number");
				System.out.println("Thank You...");
				break;
			}
			case 4 :
			{
				System.out.println("Enter Number to check for Automorphic Number ");
				int num=sc.nextInt();
				int temp=num;
				int length=0;
				int sqrNum= (int)Math.pow(num,2);
				while(num>0)
				{
					length++;
					num=num/10;
				}
				int rem= sqrNum % (int)Math.pow(10,length);
				if(temp==rem)System.out.println(temp+" is automorphic");
				else System.err.println(temp+ " is not automorphic");
				System.out.println("Thank you...");
				break;
			}
			case 5 :
			{
				System.out.println("Enter the Number to check for Palindrome ");
				int num=sc.nextInt();
				int pal=num;
				int mod;
				int rev=0;
				while(num>0)
				{
					mod=num%10;
					rev=(rev*10)+mod;
					num=num/10;
				}
				if(pal==rev) System.out.println(pal+ " is Palindrome");
				else System.err.println(pal+ " is not Palindrome");
				System.out.println("Thank You...");
				break;
			}
			case 6:
			{
				System.out.println("Enter the Number to reverse ");
				int num=sc.nextInt();
				int mod;
				int rev=0;
				while(num>0)
				{
					mod=num%10;
					rev=(rev*10)+mod;
					num=num/10;
				}
				System.out.println("The reverse of the number is "+rev);
				System.out.println("Thank You...");
				break;
			}
			case 7:
			{
				System.out.println("Enter Number to check for perfect");
				int num=sc.nextInt();
				int sum=0;
				for(int i=1;i<=num/2;i++)
				{
					if(num%i==0)
					{
						sum=sum+i;
					}
				}
				if(num==sum) System.out.println(num+ " is a perfect number");
				else System.err.println(num+ " is not a perfect number");
				System.out.println("Thank You...");
				break;
			}
			case 8:
			{
				System.out.println("Enter Number ");
				int num=sc.nextInt();
				if(num<0)
				{
					System.err.println(num+" is not prime");
				}
				boolean flag=true;
				for(int i=2;i<num/2;i++)
				{
					if(num%i==0)
					{
						flag=false;
						break;
					}
				}
				if(flag) System.out.println(num+" is Prime");
				else System.out.println(num+" is not Prime");
				System.out.println("Thank You...");
				break;
			}
			case 9:
			{
				while(true) {
				System.out.println("\nI am Calculator.....Welcome!\n");
				System.out.println("Enter Num1 :");
				int num1=sc.nextInt();
				System.out.println("Enter Num2 :");
				int num2=sc.nextInt();
				System.out.println("Enter Choice");
				System.out.println(" + : Addition \n - : Substraction \n * : Multiplication \n / : Division");
				char choice= sc.next().charAt(0);
				switch(choice)
				{
					case '+' : System.out.println(num1+" "+choice+" "+num2+" = "+(num1+num2)); break;
					case '-' : System.out.println(num1+" "+choice+" "+num2+" = "+(num1-num2)); break;
					case '*' : System.out.println(num1+" "+choice+" "+num2+" = "+(num1*num2)); break;
					case '/' :
					{
						if(num2==0)
						{
							System.err.println("Denominator cannot be Zero\nPlease Try Again...");
						}
						else
						{
							System.out.println(num1+" "+choice+" "+num2+" = "+(num1/num2));
						}
						break;
					}
					default : System.err.println("You have entered an invalid choice\nPlease try again...");
				}
				System.out.println("Thank You...");
				System.err.println("Do you want to continue? Press Y \nTo exit press any key");
				char ch=sc.next().charAt(0);
				if(ch=='Y'||ch=='y') continue;
				else break;
				}
				break;
			}
			case 10 :
			{
				while(true) {
				System.out.println(" Please Enter the Marks of subjects: ");
				System.out.print(" Enter Subject1 :");
				int s1=sc.nextInt();
				System.out.print(" Enter Subject2 :");
				int s2=sc.nextInt();
				System.out.print(" Enter Subject3 :");
				int s3=sc.nextInt();
				System.out.print(" Enter Subject4 :");
				int s4=sc.nextInt();
				int total,avg;
				if(s1>=35 && s2>=35 && s3>=35 && s4>=35)
				{
					total=s1+s2+s3+s4;
					avg=total/4;

					if(avg>=80) System.out.println("Your average marks is "+avg+" ->Excellent! You got First class");

					else if(avg>=60 || avg<80) System.out.println("Your average marks is "+avg+" ->Good! You got Second class");

					else if(avg>=50 || avg<60) System.out.println("Your average marks is "+avg+" ->You got Third class");

					else if(avg>=35 || avg<50) System.out.println("Your average marks is "+avg+" ->You just passed");
				}
				else
				{
					System.err.println("You have failed in one or more subjects..\nTry again... ThANK yOU");
				}
				System.err.println("Do you want to check another result? Press Y\nTo exit press N");
				char ch=sc.next().charAt(0);
				if(ch=='y'||ch=='Y') continue;
				else break;
			}
			break;
				
			}
			default : System.err.println("You have entered an invalid choice\nPlease try again...");
		}
		System.err.println("Do you want to go to main menu?? Press Y \nDo you want to exit? Press N");
		char ch=sc.next().charAt(0);
		if(ch=='Y'||ch=='y') continue;
		 else System.out.println("Thank you for using our service..\nPlease visit again"); break;
			
	
		}
		
		
	}

}
