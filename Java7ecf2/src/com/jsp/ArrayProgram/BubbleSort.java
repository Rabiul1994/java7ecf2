package com.jsp.ArrayProgram;

public class BubbleSort {
	
	public static void bubSort(int[] a)
	{
		for(int i=0;i<a.length-1;i++)
		{
			for(int j=0;j<a.length-1-i;j++)
			{
				if(a[j]>a[j+1])
				{
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
		}
	}

	public static void main(String[] args) 
	{
		int a[]= {3,6,5,2,0,1};
		bubSort(a);
		for(int n:a)
		{
			System.out.print(n+" ");
		}
	}

}
