package com.jsp.ArrayProgram;

public class InsertionSort 
{
	public static void insertionSort(int[] a)
	{
		for(int i=1;i<a.length;i++)
		{
			int key=a[i];
			int j=i-1;
			while(j>-1&&a[j]>key)
			{
				a[j+1]=a[j];
				j--;
			}
			a[j+1]=key;
		}
	}

	public static void main(String[] args) 
	{
		int[] a= {6,4,9,2,0,1};
		insertionSort(a);
		for(int n:a)
		{
			System.out.print(n+" ");
		}
	}

}
