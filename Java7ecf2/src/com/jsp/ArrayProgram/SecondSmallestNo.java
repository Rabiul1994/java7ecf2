package com.jsp.ArrayProgram;

public class SecondSmallestNo {

	public static void main(String[] args) 
	{
		int a[]= {6,6,7,2,1,8,2,9};
		int s1=a[0];
		int s2=a[0];
		for(int i=1;i<a.length;i++)
		{
			if(a[i]==s1) continue;
			
			if(a[i]<s1)
			{
				s2=s1;
				s1=a[i];
			}
			else if(s1==s2 || a[i]<s2)
			{
				s2=a[i];
			}
		}
		System.out.println("Second smallest: "+s2);
	}

}
