package com.jsp.ArrayProgram;

public class Assignment1 { // i/p {2,3,5,8,9}   o/p 2+3+5+8+9=27

	public static void main(String[] args) 
	{
		int[] a= {2,3,5,8,9};
		String s1="";
		String s2="";
		int sum=0;
		for(int n:a)
		{
			sum+=n;
			s1+=n;
			s1+="+";
		}
		s1=s1.substring(0,s1.length()-1);
		s2+=s1+"="+sum;
		System.out.println(s2);
	}

}
