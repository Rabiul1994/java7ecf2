package com.jsp.ArrayProgram;

public class LinearSearch 
{
	public static int search(int[] a,int s)
	{
		for(int i=0;i<a.length;i++)
		{
			if(s==a[i]) return i;
		}
		return -1;
	}

	public static void main(String[] args) {
		int[] a= {5,7,3,8,1,2,9};
		System.out.println(search(a,2));
		System.out.println(search(a,4));
	}

}
