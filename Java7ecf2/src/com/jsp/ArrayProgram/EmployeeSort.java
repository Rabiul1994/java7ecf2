package com.jsp.ArrayProgram;

import java.util.Arrays;

class Employee implements Comparable
{
	String name;
	int age;
	int salary;
	
	public Employee(String name, int age, int salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	
	public String toString() {
	return "name:"+name+" Age:"+age+" Salary:"+salary;
	}

	@Override
	public int compareTo(Object o) {
//		String n1=name;
//		String n2=((Employee)o).name;
//		if(n1.compareTo(n2)>0) return 1;
//		if(n1.compareTo(n2)<0) return -1;
//		return 0;
//		return age-((Employee)o).age;
		return salary-((Employee)o).salary;
	}
	
}


public class EmployeeSort {

	public static void main(String[] args) {
		Employee[] a=new Employee[3];
		a[0]=new Employee("ravi",28,200000);
		a[1]=new Employee("rocky", 33, 25000);
		a[2]=new Employee("sami", 23, 15000);
		
		Arrays.sort(a);
		for(Employee e:a) System.out.println(e);

	}

}
