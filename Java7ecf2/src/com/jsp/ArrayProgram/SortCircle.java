package com.jsp.ArrayProgram;

import java.util.Arrays;

class Circle implements Comparable<Circle>
{
	int radius;
	
	
	public Circle(int r)
	{
		radius=r;
	}
	public String toString()
	{
		return "radius="+radius;
	}

	@Override
	public int compareTo(Circle o) 
	{
		return radius-o.radius;
	}
	
}

public class SortCircle {

	public static void main(String[] args) {
		Circle[] a=new Circle[5];
		a[0]=new Circle(60);
		a[1]=new Circle(20);
		a[2]=new Circle(10);
		a[3]=new Circle(30);
		a[4]=new Circle(50);
		
		Arrays.sort(a);
		for(Circle c:a) System.out.println(c);
			
		

	}

}
