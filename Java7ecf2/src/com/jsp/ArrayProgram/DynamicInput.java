package com.jsp.ArrayProgram;

import java.util.Scanner;

public class DynamicInput {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size:");
		int[] a=new int[sc.nextInt()];
		for(int i=0;i<a.length;i++)
		{
			System.out.print("enter element "+i+": ");
			a[i]=sc.nextInt();
		}
		for(int n:a)
		{
			System.out.print(n+" ");
		}
		
	}

}
