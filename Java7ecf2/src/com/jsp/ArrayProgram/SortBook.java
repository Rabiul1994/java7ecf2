package com.jsp.ArrayProgram;

import java.util.Arrays;
import java.util.Comparator;

class Book implements Comparable
{
	String title;
	int pages;
	
	public Book(String t,int p)
	{
		title=t;
		pages=p;
	}
	
	public String toString()
	{
		return "Title="+title+"\tPages="+pages;
	}

	@Override
	public int compareTo(Object o) {
		return title.compareTo(((Book)o).title);
	}
}

class PageComp implements Comparator
{

	@Override
	public int compare(Object o1, Object o2) {
		return ((Book)o1).pages-((Book)o2).pages;
	}
	
}

public class SortBook {

	public static void main(String[] args) {
		Book[] a=new Book[3];
//		Comparator c=new PageComp();
		a[0]=new Book("java",350);
		a[1]=new Book("css",400);
		a[2]=new Book("html",35);
		Arrays.sort(a);
		Arrays.sort(a,new PageComp());
		
		for(Book b:a) System.out.println(b);
		

	}

}
