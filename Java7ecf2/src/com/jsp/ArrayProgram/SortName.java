package com.jsp.ArrayProgram;

import java.util.Arrays;
import java.util.Comparator;

class LengthComp implements Comparator
{

	@Override
	public int compare(Object o1, Object o2) {
		return ((String)o1).length()-((String)o2).length();
	}
	
}

public class SortName {

	public static void main(String[] args) {
		String[] a= {"humayun","akbar","babar","shahjahan","jahangir","aurangajeb"};
		Arrays.sort(a);
		Arrays.sort(a,new LengthComp());
		
		for(String s:a) System.out.println(s);
	}

}
