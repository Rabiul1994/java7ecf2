package com.jsp.ArrayProgram;


public class MissingNum {

	public static void main(String[] args) 
	{
		int n=9;
		int sum=0;
		int[] a= {6,3,1,9,5,2,7,8};
		int sumOfNum=n*(n+1)/2;
		
		for(int e:a)
		{
			sum+=e;
		}
		int misNum=sumOfNum-sum;
		System.out.println(misNum);
		
		
	}

}
