package com.jsp.ArrayProgram;

public class SortArrayMinSwap {

	public static void sort(int[] a)
	{
		int i=0;
		int j=a.length-1;
		while(i<j)
		{
			while(a[i]==0) i++;
			while(a[j]==1) j--;
			if(i<j)
			{
				int temp=a[i];
				a[i]=a[j];
				a[j]=temp;
				i++;
				j--;
			}
		}
	}
	
	public static void main(String[] args) 
	{
		int[] a= {1,0,1,1,0,1,0,0,0,1,0,1,1,0};
		sort(a);
		String str="";
		for(int n:a) 
		{
			str+=n;
			str+=",";
		}
		System.out.println(str);
			

	}

}
