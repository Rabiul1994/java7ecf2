package com.jsp.ArrayProgram;

public class Permutation {
	
	public static int[] swap(int[] a,int i,int j)
	{
		int temp=a[i];
		a[i]=a[j];
		a[j]=temp;
		
		return a;
	}
	public static void permutation(int[] a,int start,int end)
	{
		if(start>=end)
		{
			for(int n:a) System.out.print(n);
			return;
		}
		for(int i=start;i<=end;i++)
		{
			int[] b=swap(a,start,i);
			permutation(b, start+1, end);
		}
	}
	

	public static void main(String[] args) 
	{
		int a[]	= {1,2};
		permutation(a,0,1);

	}

}
