package com.jsp.ArrayProgram;

public class SelectionSort 
{
	public static void selectsort(int[] a)
	{
		for(int i=0;i<a.length-1;i++)
		{
			int index=i;
			for(int j=i+1;j<a.length;j++)
			{
				if(a[j]<a[index]) index=j;
			}
			if(i!=index)
			{
				int temp=a[i];
				a[i]=a[index];
				a[index]=temp;
			}
		}
	}

	public static void main(String[] args) 
	{
		int[] a= {8,10,2,5,1,12};
		selectsort(a);
		for(int n:a)
		{
			System.out.print(n+" ");
		}
	}

}
