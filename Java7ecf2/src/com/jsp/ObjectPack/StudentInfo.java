package com.jsp.ObjectPack;

class Student
{
	int id;
	String name;
	public Student(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + "]";
	}
	
	public boolean equals(Object arg)
	{
		if(!(arg instanceof Student)) return false;
		Student s=(Student)arg;
		return this.id==s.id && this.name.equals(s.name);
			
	}
	
}


public class StudentInfo {

	public static void main(String[] args) {
		Student s1=new Student(1, "Raju");
		System.out.println(s1);
		Student s2=new Student(1,"Raju");
		System.out.println(s2);
		System.out.println(s1.equals(s2));
		

	}

}
