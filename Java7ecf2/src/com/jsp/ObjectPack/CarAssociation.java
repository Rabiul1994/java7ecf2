package com.jsp.ObjectPack;

class Honk
{
	String pop;

	public Honk(String pop) {
		super();
		this.pop = pop;
	}

	@Override
	public String toString() {
		return "Honk [pop=" + pop + "]";
	}
}

class Enginee
{
	String eng;

	public Enginee(String eng) {
		super();
		this.eng = eng;
	}

	@Override
	public String toString() {
		return "Engine [eng=" + eng + "]";
	}
}

class Carr
{
	String model;
	Enginee eng;
	public Carr(String model, Enginee e2) {
		super();
		this.model = model;
		this.eng = e2;
	}
	@Override
	public String toString() {
		return "Car [model=" + model + ", eng=" + eng + "]";
	}
	
	public void useHonk(Honk h)
	{
		System.out.println(h);
	}
	
}
public class CarAssociation {

	public static void main(String[] args) {
		Enginee e1=new Enginee("4-Stroke Engine");
		Carr c=new Carr("AUDI", e1);
		
		System.out.println(c);
		
		c.useHonk(new Honk("poppop"));

	}

}
