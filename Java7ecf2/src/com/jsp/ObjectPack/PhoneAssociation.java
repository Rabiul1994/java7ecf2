package com.jsp.ObjectPack;

class SIM
{
	String operator;
	
	

	public SIM(String operator) {
		super();
		this.operator = operator;
	}



	@Override
	public String toString() {
		return "SIM [operator=" + operator + "]";
	}
	
}

class Battery
{
	String battery;

	public Battery(String battery) {
		super();
		this.battery = battery;
	}

	@Override
	public String toString() {
		return "Battery [battery=" + battery + "]";
	}
	
	
}

class Phone
{
	String model;
	Battery bt;
	
	public Phone(String model, Battery bt) {
		super();
		this.model = model;
		this.bt = bt;
	}
	
	
	
	@Override
	public String toString() {
		return "Phone [model=" + model + ", bt=" + bt + "]";
	}



	public void useSim(SIM op)
	{
		
		System.out.println(op);
	}
	
	
	
}
public class PhoneAssociation {

	public static void main(String[] args) 
	{
		Battery br=new Battery("Li-ion");
		Phone ph=new Phone("Samsung", br);
		
		System.out.println(ph);
		
		ph.useSim(new SIM("airtel"));
	}
}
