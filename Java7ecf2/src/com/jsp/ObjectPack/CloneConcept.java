package com.jsp.ObjectPack;

class Employee implements Cloneable
{
	int id;
	String name;
	String desg;
	public Employee(int id, String name, String desg) {
		super();
		this.id = id;
		this.name = name;
		this.desg = desg;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", desg=" + desg + "]";
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	
}

public class CloneConcept {

	public static void main(String[] args) throws CloneNotSupportedException {
		Employee e1=new Employee(1, "Ravi", "Developer");
		
		Employee e2=(Employee)e1.clone();
		
		
		System.out.println(e1);
		System.out.println(e2);

	}

}
