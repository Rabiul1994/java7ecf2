package com.jsp.ObjectPack;

public class Vehicle {

	public static void main(String[] args) {
		Engine e1=new Engine("petrol", 4);
		Engine e2= new Engine("petrol", 4);
		
		Car c1=new Car("Swift", 70000, e1);
		Car c2=new Car("Swift", 70000, e2);
		
		System.out.println(c1==c2);
		System.out.println(c1.equals(c2));
		

	}

}
