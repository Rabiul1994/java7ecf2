package com.jsp.ObjectPack;

class Employee2 implements Cloneable
{
	int id;
	String name;
	String desg;
	Address addr;
	public Employee2(int id, String name, String desg, Address addr) {
		super();
		this.id = id;
		this.name = name;
		this.desg = desg;
		this.addr=addr;
	}
	
	
	@Override
	public String toString() {
		return "Employees [id=" + id + ", name=" + name + ", desg=" + desg + ", addr=" + addr + "]";
	}


	@Override
	protected Object clone()
	{	
		Employee2 e=null;
		try {
			e=(Employee2) super.clone();
			e.addr=(Address)addr.clone();
		}
		catch (Exception e1) {
		System.out.println(e1);
		}
	
	return e;
				
	}
}


public class DeepClone {

	public static void main(String[] args) throws CloneNotSupportedException {
		
		Address ad=new Address("Bangalore");
		
		Employee2 e1 =new Employee2(1, "Ravi", "Developer", ad);
		
	
		
		Employee2 e2=(Employee2)e1.clone();
		
		Employee2 e3=(Employee2)e1.clone();
		
		System.out.println(e1);
		System.out.println(e2);
		
		e2.addr.address="Mangalore";
		e3.addr.address="Kolkata";
		
		System.out.println(e1);
		System.out.println(e2);
		System.out.println(e3);
		
		

	}

}
