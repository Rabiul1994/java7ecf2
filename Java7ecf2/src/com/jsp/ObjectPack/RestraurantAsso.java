package com.jsp.ObjectPack;

class MenuCard
{
	String biryani;
	String mutton;
	public MenuCard(String biryani, String mutton) {
		super();
		this.biryani = biryani;
		this.mutton = mutton;
	}
	@Override
	public String toString() {
		return "MenuCard [biryani=" + biryani + ", mutton=" + mutton + "]";
	}
	
}

class Cook
{
	String cook;

	public Cook(String cook) {
		super();
		this.cook = cook;
	}

	@Override
	public String toString() {
		return "Cook [cook=" + cook + "]";
	}
	
	
}

class Restraurant
{
	Cook ck;
	static
	{
		System.out.println("Welcome to Food Lover");
	}
	public Restraurant(Cook ck) {
		super();
		this.ck = ck;
	}
	@Override
	public String toString() {
		return "Restraurant [ck=" + ck + "]";
	}
	
	public void useMenu(MenuCard m)
	{
		System.out.println(m);
	}
	
	
}


public class RestraurantAsso {

	public static void main(String[] args) {
		Cook c=new Cook("Food is cooked");
		Restraurant r1=new Restraurant(c);
		
		System.out.println(r1);
		
		r1.useMenu(new MenuCard("Chicken Biryani","Mutton Curry"));

	}

}
