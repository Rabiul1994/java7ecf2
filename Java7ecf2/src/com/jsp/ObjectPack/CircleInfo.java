package com.jsp.ObjectPack;

class Circle
{
	int radius;

	public Circle(int radius) {
		super();
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
	public boolean equals(Object arg)
	{
		if(!(arg instanceof Circle)) return false;
		return radius==((Circle)arg).radius;
	}
	
}

public class CircleInfo {

	public static void main(String[] args) {
		Circle c1=new Circle(12);
		Circle c2=new Circle(12);
		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c1.equals(c2));

	}

}
