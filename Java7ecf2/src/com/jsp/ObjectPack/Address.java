package com.jsp.ObjectPack;

public class Address implements Cloneable
{
	String address;

	public Address(String address) {
		super();
		this.address = address;
	}

	
	
	@Override
	public String toString() {
		return "Address [address=" + address + "]";
	}



	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	
}
