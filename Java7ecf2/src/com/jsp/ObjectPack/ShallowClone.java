package com.jsp.ObjectPack;

class Employees implements Cloneable
{
	int id;
	String name;
	String desg;
	Address addr;
	public Employees(int id, String name, String desg, Address addr) {
		super();
		this.id = id;
		this.name = name;
		this.desg = desg;
		this.addr=addr;
	}
	
	
	@Override
	public String toString() {
		return "Employees [id=" + id + ", name=" + name + ", desg=" + desg + ", addr=" + addr + "]";
	}


	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}

public class ShallowClone {

	public static void main(String[] args) throws CloneNotSupportedException {
		
		Address add=new Address("Mangalore");
		Employees e1=new Employees(1, "Sundar", "Developer", add);
		
		Employees e2=(Employees) e1.clone();
		
		System.out.println(e1);
		System.out.println(e2);
		e2.addr.address="Bangalore";
		System.out.println(e1);
		System.out.println(e2);
	}

}
