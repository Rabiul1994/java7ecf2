package com.jsp.ObjectPack;

class Car
{
	String name;
	double price;
	Engine eng;
	
	
	public Car(String name, double price, Engine eng) {
		this.name = name;
		this.price = price;
		this.eng = eng;
	}


	@Override
	public String toString() {
		return "Car [name=" + name + ", price=" + price + ", eng=" + eng + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Car)) return false;
		Car c=(Car)obj;
		return this.name.equals(c.name) && this.price==c.price && this.eng.equals(c.eng);
		
			
	}
}
