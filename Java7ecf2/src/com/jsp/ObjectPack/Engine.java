package com.jsp.ObjectPack;

class Engine 
{
	String fueltype;
	int durable;
	
	public Engine(String fueltype, int durable) {
		this.fueltype = fueltype;
		this.durable = durable;
	}

	@Override
	public String toString() {
		return "Engine [fueltype=" + fueltype + ", durable=" + durable + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Engine)) return false;
		Engine e=(Engine)obj;
		return this.fueltype.equals(e.fueltype) && this.durable==e.durable;
	}
	

}


