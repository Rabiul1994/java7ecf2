package com.jsp.ArrayProgramTwoDim;

import java.util.Scanner;

public class DynamicInput 
{
	public static int[][] userInput()
	{
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size:");
		int size=sc.nextInt();
		int[][] a=new int[size][size];
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
				System.out.print("Enter["+i+","+j+"] :");
				a[i][j]=sc.nextInt();
			}
		}
		display(a);
		return a;
	}
	public static void display(int[][] a)
	{
		for(int[] temp:a)
		{
			for(int n:temp)
			{
				System.out.print(n+"\t");
			}
			System.out.println();
		}
	}
	public static void main(String[] args) 
	{
		userInput();
	}

}
