package com.jsp.ArrayProgramTwoDim;

public class Transpose {

	public static void main(String[] args) 
	{
		int[][] a=DynamicInput.userInput();
		int[][] b=new int[3][3];
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
				b[j][i]=a[i][j];
			}
		}
		for(int[] temp:b)
		{
			for(int n:temp)
			{
				System.out.print(n+" ");
			}
			System.out.println();
		}
	}

}
