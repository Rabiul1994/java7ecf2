package com.jsp.ArrayProgramTwoDim;

public class SumOfBothDiag {

	public static void main(String[] args) {
	int[][] b=DynamicInput.userInput();
		int sum=0;
		for(int i=0;i<b.length;i++)
		{
			for(int j=0;j<b[i].length;j++)
			{
				if(i==j) sum+=b[i][j];
				if(i+j==b.length-1 && i!=j) sum+=b[i][j];
			}
		}
		System.out.println(sum);
		System.out.println("<<<Using One Loop>>>");
		int sum2=0;
		for(int i=0;i<b.length;i++)
		{
			sum2+=b[i][i];
			if(i!=b.length/2 || b.length%2==0) sum2+=b[i][b.length-1-i];

		}
		System.out.println(sum2);
	}
}
