package com.jsp.ArrayProgramTwoDim;

import java.util.Scanner;

public class SumOfElement {
	
	public static int[][] dynamicInput()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size");
		int size=sc.nextInt();
		int[][] a=new int[size][size];
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
				System.out.print("Enter["+i+","+j+"] :");
				a[i][j]=sc.nextInt();
			}
		}
		return a;
	}

	public static void main(String[] args) 
	{
		int[][] a=dynamicInput();
		int sum=0;
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
			
				 
				sum+=a[i][j];
			}
		}
		System.out.print("Sum= "+sum);
		
	}

}
