package com.jsp.ArrayProgramTwoDim;

public class TwoDimDemo {
	

	public static void main(String[] args) 
	{
		int[][] a= {{1,2,3},{4,5,6},{7,8,9}};
		int[][] b=new int[3][];
		b[0]= new int[3];
		b[1]= new int[] {6,7,9};
		b[2]= new int[] {3,2,8};
		
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("<<<Using For-Each Loop>>>");
		for(int[] temp:b)
		{
			for(int n:temp)
			{
				System.out.print(n+" ");
			}
			System.out.println();
		}
	}

}
