package com.jsp.ArrayProgramTwoDim;

public class SumOfDiagonalElement {

	public static void main(String[] args) 
	{
		int[][] a= DynamicInput.userInput();
		int sum=0;
		for(int i=0;i<a.length;i++)
		{
			sum+=a[i][i];
		}
		System.out.println(sum);
		
		System.out.println("<<<Using Two Loop>>>");
		int sum2=0;
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
				if(i==j) sum2+=a[i][j];
			}
			
		}
		System.out.println(sum2);

	}

}
