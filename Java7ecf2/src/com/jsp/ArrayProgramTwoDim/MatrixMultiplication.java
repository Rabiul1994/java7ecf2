package com.jsp.ArrayProgramTwoDim;

public class MatrixMultiplication {

	public static void main(String[] args) 
	{
		int[][] a=DynamicInput.userInput();
		int[][] b=DynamicInput.userInput();
		int[][] c=new int[3][3];
		
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a.length;j++)
			{
				int sum=0;
				for(int k=0;k<a.length;k++)
				{
					sum+=a[i][k]*b[k][j];
				}
				c[i][j]=sum;
			}
			
		}
		for(int[] temp:c)
		{
			for(int n:temp)
			{
				System.out.print(n+" ");
			}
			System.out.println();
		}
		

	}

}
