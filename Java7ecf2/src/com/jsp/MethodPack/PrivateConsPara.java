package com.jsp.MethodPack;

class Cric
{
	int score;

	private Cric(int score)
	{
		this.score = score;
	}
	public static Cric getInstance(int score)
	{
		return new Cric(score);
	}
	public void display()
	{
		System.out.println("Scored: "+score);
	}
	
}

public class PrivateConsPara 
{
	public static void main(String[] args) 
	{
		Cric c1=Cric.getInstance(2);
		c1.display();
		Cric c2=Cric.getInstance(23);
		c2.display();
		Cric c3=Cric.getInstance(99);
		c3.display();
		
		Cric c4=Cric.getInstance(55);
		c4.display();
		
		
	}
}
