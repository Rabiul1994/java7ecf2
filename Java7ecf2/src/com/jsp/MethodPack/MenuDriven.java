package com.jsp.MethodPack;

import java.util.Scanner;

public class MenuDriven 
{
	

	public static void main(String[] args) 
	{
		while(true) {
		System.out.println("Program Menu");
		Scanner sc=new Scanner(System.in);
		System.out.println("1. Armstrong Number Check\n2. Armstrong Number Range\n3. Automorphic Check\n4. Automorphic Range\n5. Niven Check\n6. Niven Range\n7. Prime Count\n8. Prime Range\n9. Strong Number\n10. Strong Range\n11. Perfect Check\n12. Perfect Range");
		System.out.print("\nSelect a number:");
		
		int n=sc.nextInt();
		
		switch(n)
		{
		case 1:
		{
			System.out.print("Enter Number to check for Armstrong:");
			int n1=sc.nextInt(); 
			int res=ArmstrongNo.armstrong(n1);
			if(n1==res) System.out.println("Armstrong");
			else System.out.println("Not Armstrong");
			break;
		}
		case 2:
		{
			System.out.print("Enter Range:");
			int n1=sc.nextInt(); 
			for(int i=1;i<=n1;i++)
			{
				if(i==ArmstrongRange.armstrong(i)) System.out.println(i);
			}
			break;
		}
		case 3:
		{
			System.out.print("Enter Number to check for Automorphic: ");
			int n1=sc.nextInt();
			
			if(n1<0) System.out.println("No negatives");
			else
			{
				int res=Automorphic.automorphic(n1);
				if(res==n1) System.out.println("Automorphic");
				else System.out.println("Not automorphic");
			}
			break;
		}
		case 4:
		{
			System.out.println("Enter Range:");
			int n1=sc.nextInt(); 
			for(int i=1;i<=n1;i++)
			{
				if(i==AutomorphicRange.automorphic(i)) System.out.println(i);
			}
			break;
		}
		case 5:
		{
			System.out.println("Enter num to check niven:");
			int n1=sc.nextInt();
			
			if(n1<0) System.out.println("No negatives");
			else
			{
				int res=MethodSample.sumOfDigit(n1);
				if(n1%res==0) System.out.println("Niven");
				else System.out.println("Not Niven");
			}
			break;
		}
		case 6:
		{
			System.out.println("Enter Range:");
			int n1=sc.nextInt(); 
			for(int i=1;i<=n1;i++)
			{
				int res=MethodSample.sumOfDigit(i);
				if(i%res==0) System.out.println(i);
			}
			break;
		}
		case 7:
		{
			System.out.println("Enter first n number:");
			int n1=sc.nextInt();
			
			int i=2;
			int count=0;
			while(count<n1)
			{
				if(MethodSample.isPrime(i)) 
				{
					count++;
					System.out.println(i);
					i++;
				}
				else i++;
			}
			break;
		}
		case 8:
		{
			System.out.println("Enter range:");
			int n1=sc.nextInt();
			
			for(int i=1;i<=n1;i++)
			{
				if(MethodSample.isPrime(i)) System.out.println(i);
			}
			break;
		}
		case 9:
		{
			System.out.print("Enter Number to check strong no:");
			int n1=sc.nextInt();
			if(n1<0) System.out.println("No negatives");
			else
			{
				int res=StrongNo.strong(n1);
				if(n1==res) System.out.println("Strong number");
				else System.out.println("Not strong number");
			}
			break;
		}
		case 10:
		{
			System.out.println("Enter range:");
			int n1=sc.nextInt();
			
			for(int i=1;i<=n1;i++)
			{
				if(i==StrongNoRange.strong(n1)) System.out.println(i);
			}
			break;
		}
		case 11:
		{
			System.out.println("Enter Number");
			int n1=sc.nextInt();
			
			int res=PerfectNo.perfect(n1);
			if(n1==res) System.out.println("Perfect Number");
			else System.out.println("Not perfect");
			break;
		}
		case 12:
		{
			System.out.println("Enter Range:");
			int n1=sc.nextInt();
			
			for(int i=1;i<=n1;i++)
			{
				if(i==PerfectNo.perfect(i)) System.out.println(i);
			}
			break;
		}
		default : System.err.println("You have entered an invalid choice\nPlease try again...");
		
		}
		System.out.println("Do you want to go to main menu?? Press Y \nDo you want to exit? Press N");
		char ch=sc.next().charAt(0);
		if(ch=='Y'||ch=='y') continue;
		 else System.out.println("Thank you for using our service..\nPlease visit again"); break;
		}

	}
}
