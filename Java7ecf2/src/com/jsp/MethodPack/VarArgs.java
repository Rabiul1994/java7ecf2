package com.jsp.MethodPack;

public class VarArgs 
{
	public static void grandTotal(int... a)
	{
		int sum=0;
		for(int i=0;i<a.length;i++)
		{
			sum=sum+a[i];
			
		}
		double gst= sum*0.18;
		System.out.println("SGST= "+(sum*0.09));
		System.out.println("CGST= "+(sum*0.09));
		System.out.println("18% TAx= "+gst);
		System.out.println("the grand total is: "+sum);
	}
	
	public static void main(String[] args) 
	{
		grandTotal(5000,3000,8000);
		System.out.println("********************");
		grandTotal(3000);
	}
}
