package com.jsp.MethodPack;

import java.util.Scanner;

public class PrimeNoRange {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter range:");
		int num=sc.nextInt();
		
		for(int i=1;i<=num;i++)
		{
			if(MethodSample.isPrime(i)) System.out.println(i);
		}
	}

}
