package com.jsp.MethodPack;

import java.util.Scanner;

public class NivenNo 
{
	
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num:");
		int num=sc.nextInt();
		
		if(num<0) System.out.println("No negatives");
		else
		{
			int res=MethodSample.sumOfDigit(num);
			if(num%res==0) System.out.println("Niven");
			else System.out.println("Not Niven");
		}
	}
}
