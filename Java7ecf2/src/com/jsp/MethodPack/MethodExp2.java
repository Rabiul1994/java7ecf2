package com.jsp.MethodPack;

import com.jsp.TestMethod.OddEven;
import com.jsp.pack2.StrongNo;

class Test
{
	public static void m1()
	{
	System.out.println("Lion roars");
	}
}

class Test2
{
	public static void m2()
	{
		System.out.println("Tiger is back");
	}
	public static void m3()
	{
		System.out.println("I love Java");
	}
}

public class MethodExp2 
{
	public static void main(String[] args)
	{
		System.out.println("Main started"); 
		Test.m1(); 
		Test2.m2();  //calling method from different class in the same program
		Test2.m3();  //calling method from different class in the same program
		OddEven.checkOddEven();  //calling method from class of different package
		StrongNo.main(args); //calling main method from different class of different package
		System.out.println("main ended");
	}
}