package com.jsp.MethodPack;

import java.util.Scanner;

public class AutomorphicRange 
{
	public static int automorphic(int n)
	{
		int sqrNum=(int)Math.pow(n, 2);
		int mod = sqrNum % (int)Math.pow(10, MethodSample.length(n));
		return mod;
	}
	
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Range ");
		int num=sc.nextInt();
		
		for(int i=1;i<=num;i++)
		{
			if(i==automorphic(i)) System.out.println(i);
		}
		
		
	}
}
