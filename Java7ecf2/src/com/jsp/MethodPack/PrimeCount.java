package com.jsp.MethodPack;

import java.util.Scanner;

public class PrimeCount 
{
		public static void main(String[] args)
		{
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter num:");
			int num=sc.nextInt();
			
			int i=2;
			int count=0;
			while(count<num)
			{
				if(MethodSample.isPrime(i)) 
				{
					count++;
					System.out.println(i);
					i++;
				}
				else i++;
			}
		}
}
