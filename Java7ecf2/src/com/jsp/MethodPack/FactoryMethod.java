package com.jsp.MethodPack;


class Factory
{
	int i,j;
	
	
	private Factory(int i, int j) {
		this.i = i;
		this.j = j;
	}


	public static Factory getInstance(int i, int j) //Factory method
	{
		return new Factory(i,j);
	}


	public void dispaly()
	{
		System.out.println(i+" "+j);
	}
}

public class FactoryMethod {

	public static void main(String[] args) 
	{
		Factory f1=Factory.getInstance(3,5);
		f1.dispaly();
		Factory f2=Factory.getInstance(8,9);
		f2.dispaly();
		
//		Factory.getInstance().dispaly();	
	}

}
