package com.jsp.MethodPack;

import java.util.Scanner;

public class ArmstrongNo 
{
	public static int armstrong(int a)
	{
		int sum=0;
		int length= MethodSample.length(a);
		while(a>0)
		{
			int mod=a%10;
			sum=sum + (int)Math.pow(mod, length);
			a/=10;
		}
		return sum;
	}
	
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num:");
		int num=sc.nextInt();
		
		if(num<0) System.out.println("No negatives");
		else
		{
			int res=armstrong(num);
			if(num==res) System.out.println("Armstrong");
			else System.out.println("Not Armstrong");
		}
	}
}
