package com.jsp.MethodPack;

import java.util.Scanner;

public class StrongNo 
{
	public static int strong(int num)
	{
		int sum=0;
		while(num>0)
		{
		int mod=num%10;
		sum=sum + MethodSample.fatorial(mod);
		num/=10;
		}
		return sum;
		
	}

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Number:");
		int num=sc.nextInt();
		if(num<0) System.out.println("No negatives");
		else
		{
			int res=strong(num);
			if(num==res) System.out.println("Strong number");
			else System.out.println("Not strong number");
		}
	}

}
