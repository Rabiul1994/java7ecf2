package com.jsp.MethodPack;

import java.util.Scanner;

public class ArmstrongRange 
{
	public static int armstrong(int a)
	{
		int sum=0;
		int length= MethodSample.length(a);
		while(a>0)
		{
			int mod=a%10;
			sum=sum+ (int)Math.pow(mod, length);
			a/=10;
		}
		return sum;
	}

	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter range:");
		int num=sc.nextInt();
		
		for(int i=1;i<=num;i++)
		{
			if(i==armstrong(i)) System.out.println(i);
		}
	}

}
