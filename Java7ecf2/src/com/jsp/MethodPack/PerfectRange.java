package com.jsp.MethodPack;

import java.util.Scanner;

public class PerfectRange 
{
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Range:");
		int num=sc.nextInt();
		
		for(int i=1;i<=num;i++)
		{
			if(i==PerfectNo.perfect(i)) System.out.println(i);
		}
	}
}
