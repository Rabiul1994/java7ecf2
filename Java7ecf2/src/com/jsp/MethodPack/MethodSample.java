package com.jsp.MethodPack;

public class MethodSample 
{
	public static int fatorial(int a)
	{
		int fact=1;
		for(int i=1;i<=a;i++)
		{
			fact*=i;
		}
		return fact;
	}
	
	public static int length(int n)
	{
		int length=0;
		while(n>0) 
		{
			length++;
			n/=10;
		}
		return length;
	}
	
	public static int sumOfDigit(int n)
	{
		int s=0;
		while(n>0)
		{
			int mod=n%10;
			s=s+mod;
			n/=10;
		}
		return s;
	}
	
	public static int reverse(int a)
	{
		int rev=0;
		while(a>0)
		{
			int mod=a%10;
			rev=(rev*10)+mod;
			a=a/10;
		}
		return rev;
	}
	
	public static boolean isPrime(int n)
	{
		if(n<=1) return false;
		for(int i=2;i<=n/2;i++)
		{
			if(n%i==0) return false;
		}
		return true;
	}
	
	public static int hcf(int a,int b)
	{
		int hcf=0;
		for(int i=1; i<=a || i<=b;i++)
		{
			if(a%i==0 && b%i==0)
			hcf=i;
		}
		return hcf;
	}
	
	public static int lcm(int a,int b)
	{
		int result;
		int lcm=(a>b)?a:b;
		while(true)
		{
			if(lcm%a==0 && lcm%b==0)
			{
				result=lcm;
				break;
			}
			lcm++;
		}
		return result;
	}
	
	public static int perfect(int n)
	{
		int sum=0;
		for(int i=1;i<=n/2;i++)
		{
			if(n%i==0)
			{
				sum=sum+i;
			}
		}
		return sum;
	}
	
	public static void main(String[] args) 
	{
		System.out.println(length(153));
		System.out.println(sumOfDigit(45));
		System.out.println(reverse(6545));
		System.out.println(isPrime(27));
		System.out.println(hcf(15,24));
		System.out.println(lcm(3,9));
		System.out.println(perfect(6));
	}

}

