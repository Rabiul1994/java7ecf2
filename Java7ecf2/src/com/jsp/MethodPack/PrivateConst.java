package com.jsp.MethodPack;


class Cricket
{
	int i;
	
	public static Cricket getInstance()
	{
		return new Cricket(5);
	}
	private Cricket(int i)
	{
		this.i=i;
	}
	public void display()
	{
		System.out.println(i);
	}
}

public class PrivateConst {

	public static void main(String[] args) 
	{
		Cricket c1=Cricket.getInstance();
		c1.display();
	}

}
