package com.jsp.MethodPack;

import java.util.Scanner;

public class NumberStar 
{
		
	public static void pattern1(int a)
	{
		for(int i=1;i<=a;i++)
		{
			for(int j=a;j>=1;j--)
			{
				if(i==j) System.out.print("*");
				else System.out.print(j);
			}
			System.out.println();
		}
	}
	
	public static void pattern2(int n)
	{
		int t=n-1;
		
		for(int i=1;i<=n;i++)
		{
			
			for(int j=1;j<=i;j++)
			{
				System.out.print("*");
				
			}
			for(int k=t;k>=1;k--)
			{
				System.out.print("#");
			}
			for(int j=1;j<=n;j++)
			{
				if(i==5) System.out.print(" ");
			}
			System.out.println();
			t--;
			
		}
	}
	
	public static void pattern3(int n)
	{
		if(n==5) {
			
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=n;j++)
			{
				if(i==5)
				System.out.print(" ");
				else if(i>=j) System.out.print("*");
				else System.out.print("#");
			}
			System.out.println();
		}
		}
		else
		{
			for(int i=1;i<=n;i++)
			{
				for(int j=1;j<=n;j++)
				{
					if(i>=j)
					System.out.print("*");
					else System.out.print("#");
				}
				System.out.println();
			}
		}
	}
	
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num:");
		int n=sc.nextInt();
		
		pattern1(n);
		System.out.println("---------------");
		pattern2(n);
		System.out.println("---------------");
		pattern3(n);
		
	}
}
