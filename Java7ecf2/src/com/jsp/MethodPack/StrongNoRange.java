package com.jsp.MethodPack;

import java.util.Scanner;

public class StrongNoRange 
{
	public static int strong(int num)
	{
		int sum=0;
		while(num>0)
		{
		int mod=num%10;
		sum=sum + MethodSample.fatorial(mod);
		num/=10;
		}
		return sum;
	}
	
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter range:");
		int num=sc.nextInt();
		
		for(int i=1;i<=num;i++)
		{
			if(i==strong(i)) System.out.println(i);
		}	
	}
}
