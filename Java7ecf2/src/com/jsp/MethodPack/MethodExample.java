package com.jsp.MethodPack;

public class MethodExample //how o/p is printed after calling method
{
	
	
	public static void m1()
	{
		System.out.println("m1 Method call");//3
		m3(); //4
	}

	public static void m2()
	{
		System.out.println("m2 is called"); //7
		m4(); //8
	}
	public static void m3()
	{
		System.out.println("m3 is called"); //5
		m2(); //6
	}
	public static void m4() 
	{
		System.out.println("m4 is called"); //9
	}
	public static void main(String[] args) 
	{
		System.out.println("Main started"); //1
		m1(); //2
		System.out.println("main ended"); //10
	}

}
