package com.jsp.MethodPack;

import java.util.Scanner;

public class PerfectNo 
{
	public static int perfect(int n)
	{
		int sum=0;
		for(int i=1;i<=n/2;i++)
		{
			if(n%i==0)
				{
					sum=sum+i;
				}
		}
		return sum;
	}
	
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number");
		int num=sc.nextInt();
		
		int res=perfect(num);
		if(num==res) System.out.println("Perfect Number");
		else System.out.println("Not perfect");
		 
		 
	}
}
