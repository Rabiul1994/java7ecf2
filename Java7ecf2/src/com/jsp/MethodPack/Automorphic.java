package com.jsp.MethodPack;

import java.util.Scanner;

public class Automorphic 
{
	public static int automorphic(int n)
	{
		int sqrNum=(int)Math.pow(n, 2);
		int mod = sqrNum % (int)Math.pow(10, MethodSample.length(n));
		return mod;
	}
	
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number ");
		int num=sc.nextInt();
		
		if(num<0) System.out.println("No negatives");
		else
		{
			int res=automorphic(num);
			if(res==num) System.out.println("Automorphic");
			else System.out.println("Not automorphic");
		}
	}
}
