package com.jsp.Oops;

class Parent
{
	public void bike()
	{
		System.out.println("TVS 50");
	}
}
class Child extends Parent
{
	public void bike()
	{
		System.out.println("TVS Apache RTR");
	}
}

public class LateBinding {

	public static void main(String[] args) 
	{
		Child c=new Child();
		c.bike();
		
	}

}
