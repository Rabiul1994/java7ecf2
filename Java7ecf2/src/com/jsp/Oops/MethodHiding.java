package com.jsp.Oops;

// Early binding

class Parentt
{
	static public void bike()
	{
		System.out.println("TVS 50");
	}
}
class Childd extends Parentt
{
	static public void bike()
	{
		System.out.println("TVS Apache RTR");
	}
}



public class MethodHiding {

	public static void main(String[] args) {
		Parentt p=new Parentt();
		p.bike();
		Childd c=new Childd();
		c.bike();
		Parentt p1=new Parentt();
		p1.bike();
		
	}

}
