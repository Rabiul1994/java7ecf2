package com.jsp.Oops;

import java.util.Scanner;

public class Factorial // when method is non-static
{
	public void factor(int n) //method is non static
	{
		int fact=1;
		for(int i=1;i<=n;i++)
		{
			fact*=i;
		}
		System.out.println(fact);
	}
	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number");
		int n=sc.nextInt();
		
		Factorial fac = new Factorial();
		fac.factor(n);
		
	}

}
