package com.jsp.Oops;

public class VariableArgs {

	public static int m1(int... a)
	{
		int sum=0;
		for(int i=0;i<a.length;i++)
			{
			sum=sum+a[i];
			System.out.println(a[i]);
			}
		return sum;
	}
	
	public static void m2(int... a)
	{
		int s=0;
		for(int i:a)
		{
			s+=i;
		}
		System.out.println(s);
	}
	
	public static void main(String[] args) 
	{
		System.out.println(m1(20,30,76));
		m2(12,98,76,65);
	}

}
