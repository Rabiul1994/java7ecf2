package com.jsp.Oops;

class DataEn
{
	private int i=34;
	String name="rabi";
	
	public int getI() //getter/accessor
	{
		return this.i;
	}
	public void setI(int i) //setter/mutator
	{
		this.i=i;
	}
	public String getName() { 
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}

public class DataEncap {

	public static void main(String[] args) {
		DataEn d1=new DataEn();
		System.out.println(d1.getI());
		d1.setI(100);
		System.out.println(d1.getI());
		System.out.println(d1.getName());
		d1.setName("saddam");
		System.out.println(d1.getName());
	}

}
