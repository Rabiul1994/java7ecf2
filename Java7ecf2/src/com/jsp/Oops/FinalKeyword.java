package com.jsp.Oops;

class Final
{
	final int i;
	
//	public Final()  //can be initilized using constructor
//	{
//		i=25;
//	}
	
	//OR
	
	{
		i=25;  // between constructor and non static block, ns block gets priority
	}
	public void change()
	{
		
	}
}



public class FinalKeyword {

	public static void main(String[] args) 
	{
		Final f1=new Final();
		System.out.println(f1.i);
	}

}
