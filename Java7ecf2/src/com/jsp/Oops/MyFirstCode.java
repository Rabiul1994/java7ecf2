package com.jsp.Oops;

public class MyFirstCode 
{
	int a=33;
	static int b=45;
	public static void main(String[] args) 
	{
		//System.out.println(a);
		System.out.println(MyFirstCode.b); // for static, memory is allocated when class is loaded
		System.out.println(b);
		
		MyFirstCode m1 = new MyFirstCode(); // for non-static, object is created to allocate memory
		System.out.println(m1.a);
		
		
	}

}
