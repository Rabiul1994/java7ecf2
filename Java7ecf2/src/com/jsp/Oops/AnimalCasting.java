package com.jsp.Oops;

class Animal
{
	public void drink()
	{
		System.out.println("drinks water");
	}
	public void move()
	{
		System.out.println("Movement");
	}
	public void sound()
	{
		System.out.println("sound of animal");
	}
}

class Lion extends Animal
{
	@Override
	public void move()
	{
		System.out.println("Runs");
	}
	@Override
	public void sound()
	{
		System.out.println("roars");
	}
	public void food()
	{
		System.out.println("meat");
	}
}
class Snake extends Animal
{
	@Override
	public void move()
	{
		System.out.println("Crawls");
	}
	@Override
	public void sound()
	{
		System.out.println("Hisssshhh");
	}
	public void food()
	{
		System.out.println("Milk");
	}
}


public class AnimalCasting {

	public static void main(String[] args) 
	{
		Animal a1=new Lion(); // upcasting
		a1.drink();
		a1.sound();
		a1.move();
		
		Animal a2=new Snake(); //upcasting
		a2.drink();
		a2.sound();
		a2.move();
		
		((Lion) a1).food(); //downcasting
		
//		l1.food();
		
		Snake s1= (Snake) a2; //downcasting
		s1.food();
		s1.drink();
		
		
		
		
	}

}
