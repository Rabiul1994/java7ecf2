package com.jsp.Oops;

class A
{
	int a=34;
	int b=45;
	
	public void m1()
	{
		System.out.println(a+" "+b);
	}
}

class B extends A
{
	int c=76;
	
	public void m2()
	{
		System.out.println(c);
	}
	public void m1()
	{
		System.out.println("Overridden");
	}
}

public class Casting {

	public static void main(String[] args) 
	{
		A a1=new B(); //upcasting
		a1.m1();
		System.out.println(a1.a);
		System.out.println(a1.b);
		B b1= (B)a1; //downcasting
		System.out.println(b1.c);
		System.out.println(b1.a);
		b1.m1();
		b1.m2();
	}

}
