package com.jsp.Oops;

import java.util.Scanner;

class EcomCast
{
	public void gatewayChrg()
	{
		System.out.println("Allowed gateway charge between 1% to 5%");
	}
}

class Flipkart extends EcomCast
{
	public void gatewayChrg()
	{
		System.out.println("Flipkart gateway charge is 3%");
	}
}

class Amazon extends EcomCast
{
	public void gatewayChrg()
	{
		System.out.println("Amazon gateway charge is 2%");
	}
}

class Meeso extends EcomCast
{
	public void gatewayChrg()
	{
		System.out.println("Messo gateway charge is 1%");
	}
}

class Customer
{
	public EcomCast buy(String ec)
	{
		if(ec.equalsIgnoreCase("flipkart")) return new Flipkart();
		if(ec.equalsIgnoreCase("amazon")) return new Amazon();
		if(ec.equalsIgnoreCase("Messo")) return new Meeso();
		return null;
	}
}




public class EcomCasting {

	public static void main(String[] args) 
	{
		Customer c=new Customer();
		Scanner sc=new Scanner(System.in);
		System.out.println("Which Ecom would you like to shop?");
		String ec=sc.next();
		
		EcomCast e=c.buy(ec);
		if(e instanceof Flipkart) ((Flipkart)e).gatewayChrg();
		if(e instanceof Amazon)	((Amazon)e).gatewayChrg();
		if(e instanceof Meeso)  ((Meeso)e).gatewayChrg();
	}

}
