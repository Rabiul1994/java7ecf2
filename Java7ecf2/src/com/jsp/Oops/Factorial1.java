package com.jsp.Oops;

import java.util.Scanner;

public class Factorial1 //when both method and variable is non static
{
	Scanner sc=new Scanner(System.in);
	
	int n=sc.nextInt(); // variable is non static
	
	public void factor(int n) // method is non static
	{
		long fact=1;
		for(int i=1;i<=n;i++)
		{
			fact*=i;
		}
		System.out.println(fact);
	}
	public static void main(String[] args) 
	{
		
		System.out.println("Enter Number");
		Factorial1 fac = new Factorial1(); //new object created to call the non static method & non static variable
		fac.factor(fac.n);
		
	}

}
