package com.jsp.Oops;

class Bank
{
	public void getRateOfInterest()
	{
		
	}
}

class SBI extends Bank
{
	public void getRateOfInterest()
	{
		System.out.println("SBI rate of interest is 5%");
	}
	public void cardType()
	{
		System.out.println("SBI provides Platinum Credit Card");
	}
}

class ICICI extends Bank
{
	public void getRateOfInterest()
	{
		System.out.println("ICICI rate of interest is 8%");
	}
	public void cardType()
	{
		System.out.println("ICICI provides Gold Credit Card");
	}
}

class Salesman
{
	public void sellCard(Bank b)
	{
		b.getRateOfInterest();
	}
	public void type(Bank b)
	{
		if(b instanceof SBI) ((SBI)b).cardType();
		if(b instanceof ICICI) ((ICICI)b).cardType();
	}
}

public class BankCasting {

	public static void main(String[] args) 
	{
		Salesman s1=new Salesman();
		s1.sellCard(new SBI());
		s1.type(new SBI());
		System.out.println("------------------");
		s1.sellCard(new ICICI());
		s1.type(new ICICI());
		
	}

}
