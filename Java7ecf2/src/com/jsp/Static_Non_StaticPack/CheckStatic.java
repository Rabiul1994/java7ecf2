package com.jsp.Static_Non_StaticPack;

class CheckFactorial
{
	static int fact;
	static
	{
		fact=1;
	}
	public int m1(int n)
	{
		for(int i=1;i<=n;i++)
		{
			fact*=i;
		}
		return fact;
	}
}


public class CheckStatic 
{

	public static void main(String[] args) 
	{
		CheckFactorial t=new CheckFactorial();
		System.out.println(t.m1(5));
	}

}
