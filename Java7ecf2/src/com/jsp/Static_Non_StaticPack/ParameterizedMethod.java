package com.jsp.Static_Non_StaticPack;

class Parameter
{
	int i,j;
	
	public void addData(int a,int b)
	{
		i=a;
		j=b;
	}
	public void display()
	{
		System.out.println(i+" "+j);
	}
}


public class ParameterizedMethod {

	public static void main(String[] args) 
	{
		Parameter a1=new Parameter();
		a1.display();
		a1.addData(20, 30);
		a1.display();
		
		Parameter a2=new Parameter();
		a2.display();
		a2.addData(2, 11);
		a2.display();
	}

}
