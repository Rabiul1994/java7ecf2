package com.jsp.Static_Non_StaticPack;

import java.util.Scanner;

class Patient
{
	Scanner sc=new Scanner(System.in);
	
	int id;
	String name;
	String address;
	int age;
	static String hospitalName="Help 24X7";
	static String hospitalAddress="Delhi";
	long ph;
	String typeOfDisease;


public void display()
{
	System.out.println("Employee Id: "+id+"\n"
			+ "Employee Name: "+name+"\n"
			+"Employee Address: "+address+"\n"
			+"Age: "+age+"\n"
			+"Suffering from: "+typeOfDisease+"\n"
			+"Hospital Name: "+hospitalName+"\n"
			+"Hospital Address: "+hospitalAddress+"\n"
			+"Phone: "+ph);
}

public void changeHospital()
{
	System.out.print("Enter new Hospital Name: ");
	hospitalName=sc.next();
	
	
}
public void changeHospitalAddress()
{
	System.out.print("Enter Hospital Address: ");
	hospitalAddress=sc.next();
	
}
public void addPatient()
{
	System.out.println("Enter Id: ");
	id=sc.nextInt();
	sc.nextLine();
	System.out.println("Enter Patient Name: ");
	name=sc.nextLine();
	System.out.println("Enter Patient Address: ");
	address=sc.nextLine();
	System.out.println("Enter Patient Age: ");
	age=sc.nextInt();
	sc.nextLine();
	System.out.println("Suffereing from: ");
	typeOfDisease=sc.nextLine();
	System.out.println("Enter Phone no.: ");
	ph=sc.nextLong();
	
	
}
public static void design()
{
	System.out.println("-----------------------------");
}
}




public class PatientInfo {
	

	public static void main(String[] args) 
	{
		Patient s1=new Patient();
		Patient s2=new Patient();
		Patient s3=new Patient();
		
		s1.addPatient();
		s1.display();
		s1.design();
		
		s2.addPatient();
		s2.changeHospital();
		s2.changeHospitalAddress();
		s2.display();
		s2.design();
		
		s3.addPatient();
		s3.display();
		s3.design();
		
		
	}

}
