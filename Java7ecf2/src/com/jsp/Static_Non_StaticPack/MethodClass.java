package com.jsp.Static_Non_StaticPack;

public class MethodClass 
{
	static int i;
	int j;
	
	public static void m1()
	{
		i=34;
		MethodClass obj=new MethodClass();
		System.out.println(obj.j);
	
		System.out.println(i);
	}
	
	public void m2()
	{
		j=10;
		System.out.println(j);
		System.out.println(i);
		i=i+24;
		System.out.println(i);
	}
	
	public static void main(String[] args) 
	{
		System.out.println(MethodClass.i);
		MethodClass.m1();
		System.out.println("------------------");
		MethodClass obj=new MethodClass();
		System.out.println(obj.j);
		obj.m2();
		System.out.println("------------------");
		Test2 obj1=new Test2();
		obj1.m1();
		Test2 obj2=new Test2();
		obj2.m1();
		
		obj1.j=100;
		obj1.i=50;
		obj1.m1();
		obj2.m1();
		System.out.println("------------------");
		
		
		
		
	}
}
	
	class Test2
	{
		static int i;
		int j;
		
		public void m1()
		{
			System.out.println(j);
			System.out.println(i);
		}
		
	}


