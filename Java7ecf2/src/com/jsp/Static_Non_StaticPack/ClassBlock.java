package com.jsp.Static_Non_StaticPack;

class Test1
{
	int j;
	static int i;
	
	static
	{
		i=66;
	}
	{
		j=45;
	}
}


public class ClassBlock {

	public static void main(String[] args) 
	{
		Test1 t=new Test1();
		System.out.println(t.j);
		System.out.println(Test1.i); // either we can use class name for calling static variable
		System.out.println(t.i); //or we can use object to call static variable
	}

}
