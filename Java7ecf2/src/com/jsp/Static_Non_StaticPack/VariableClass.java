package com.jsp.Static_Non_StaticPack;

public class VariableClass 
{
	public static void main(String[] args)
	{
		System.out.println(TryMe.i);
		TryMe.i=45;
		System.out.println(TryMe.i);
		System.out.println("-------------");
		NonStatic s=new NonStatic();
		System.out.println(s.j);
		s.j=550;
		System.out.println(s.j);
		System.out.println("-------------");
		Test s1=new Test();
		System.out.println(s1.i);
		System.out.println(s1.j);
	}

}
 class TryMe 
 {
	static int i;
 }

class NonStatic
{
	int j;
}

class Test
{
	int j=45;
	static int i=7;
}
