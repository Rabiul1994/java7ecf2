package com.jsp.Static_Non_StaticPack;

class ParaPatient
{
	
	int id;
	String name;
	String address;
	int age=26;
	static String hospitalName="Arti";
	static String hospitalAddress="Kolkata";
	long ph;
	String typeOfDisease;


public void display()
{
	System.out.println("Employee Id: "+id+"\n"
			+ "Employee Name: "+name+"\n"
			+"Employee Address: "+address+"\n"
			+"Age: "+age+"\n"
			+"Suffering from: "+typeOfDisease+"\n"
			+"Hospital Name: "+hospitalName+"\n"
			+"Hospital Address: "+hospitalAddress+"\n"
			+"Phone: "+ph);
}

public void changeHospital(String c_name)
{
	
	hospitalName=c_name;
	
}
public void changeHospitalAddress(String c_add)
{
	hospitalAddress=c_add;
	
}
public void addPatient(int i_d,String s_name,String s_add,int s_age,long s_ph,String typ)
{
	id=i_d; name=s_name; address=s_add; age=s_age; ph=s_ph; typeOfDisease=typ;
	
}
public static void design()
{
	System.out.println("-----------------------------");
}
}




public class ParaPatientInfo {
	

	public static void main(String[] args) 
	{
		
		ParaPatient s1=new ParaPatient();
		ParaPatient s3=new ParaPatient();
		s1.addPatient(2,"ravi","mangalore",23,983014730,"cancer");
		s1.display();
		s1.design();
		
		s3.addPatient(2,"saddam","kalna",32,906281,"TB");
		s3.changeHospital("Care");
		s3.changeHospitalAddress("marathalli");
		s3.display();
		s3.design();
		
		
		
		
	}

}

