package com.jsp.Static_Non_StaticPack;

import java.util.Scanner;

class Employee
{
	Scanner sc=new Scanner(System.in);
	
	int id;
	String name;
	String address;
	int age;
	static String companyName="Servo India";
	static String companyAddress="Delhi";
	long ph;


public void display()
{
	System.out.println("Employee Id: "+id+"\n"
			+ "Employee Name: "+name+"\n"
			+"Employee Address: "+address+"\n"
			+"Age: "+age+"\n"
			+"Company Name: "+companyName+"\n"
			+"Company Address: "+companyAddress+"\n"
			+"Phone: "+ph);
}

public void changeCompany()
{
	sc.nextLine();
	System.out.println("Enter new Company Name: ");
	companyName=sc.nextLine();
	
	
}
public void changeCompanyAddress()
{
	sc.nextLine();
	System.out.println("Enter Company Address: ");
	companyAddress=sc.nextLine();
	
}
public void addEmployee()
{
	System.out.println("Enter Id: ");
	id=sc.nextInt();
	sc.nextLine();
	System.out.println("Enter Employee Name: ");
	name=sc.nextLine();
	System.out.println("Enter Employee Address: ");
	address=sc.nextLine();
	System.out.println("Enter Employee Age: ");
	age=sc.nextInt();
	System.out.println("Enter Phone no.: ");
	ph=sc.nextLong();
	
	
}
public void design()
{
	System.out.println("-----------------------------");
}
}




public class EmployeeInfo {
	

	public static void main(String[] args) 
	{
		Employee s1=new Employee();
		Employee s2=new Employee();
		Employee s3=new Employee();
		Employee s4=new Employee();
		Employee s5=new Employee();
		
		s1.addEmployee();
		s1.display();
		s1.design();
		
		s2.addEmployee();
		s2.display();
		s2.design();
		
		s3.addEmployee();
		s3.changeCompany();
		s3.changeCompanyAddress();
		s3.display();
		s3.design();
		
		s4.addEmployee();
		s4.changeCompany();
		s4.changeCompanyAddress();
		s4.display();
		s4.design();
		
		s5.addEmployee();
		s5.display();
		s5.design();
		
	}

}
