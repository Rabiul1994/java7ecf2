package com.jsp.Static_Non_StaticPack;

class This
{
	int i;
	int j;
	
	public void m1(int i,int j) // when local variable and non-static variable are same. local variable is assigned to non static variable
	{
		i=i;			// here we get the default values but not the assigned values of local variable because JVM cannot
		j=j;			// differentiate between local variable and non-static variable
	}
	public void m2(int i, int j) // when local variable and non-static variable are same. Assignment done using (this) keyword. 
	{
		this.i=i;		//here we get the assigned value. 
		this.j=j;		// using (this) keyword we can make JVM differentiate between local variable and non-static variable.
	}
	public void display()
	{
		System.out.println(i);
		System.out.println(j);
	}
}


public class ThisKeyword {

	public static void main(String[] args) 
	{
		This s1=new This();
		s1.m1(2, 3);
		s1.display();
		s1.m2(4, 5);
		s1.display();
	}

}
