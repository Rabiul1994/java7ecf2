package com.jsp.Static_Non_StaticPack;

class ParaEmployee
{
	
	int id;
	String name;
	String address;
	int age=26;
	static String companyName="Adobe";
	static String companyAddress="Bangalore";
	long ph;


public void display()
{
	System.out.println("Employee Id: "+id+"\n"
			+ "Employee Name: "+name+"\n"
			+"Employee Address: "+address+"\n"
			+"Age: "+age+"\n"
			+"Comapany Name: "+companyName+"\n"
			+"Company Address: "+companyAddress+"\n"
			+"Phone: "+ph);
}

public void changeCompany(String c_name)
{
	
	companyName=c_name;
	
}
public void changeCompanyAddress(String c_add)
{
	companyAddress=c_add;
	
}
public void addEmployee(int i_d,String s_name,String s_add,int s_age,long s_ph)
{
	id=i_d; name=s_name; address=s_add; age=s_age; ph=s_ph;
	
}
public static void design()
{
	System.out.println("-----------------------------");
}
}




public class ParaEmployeeInfo {
	

	public static void main(String[] args) 
	{
		
		ParaEmployee s1=new ParaEmployee();
		ParaEmployee s3=new ParaEmployee();
		s1.addEmployee(2,"ravi","mangalore",23,983014730);
		s1.display();
		s1.design();
		
		s3.addEmployee(2,"saddam","kalna",32,906281);
		s3.changeCompany("adamas");
		s3.changeCompanyAddress("marathalli");
		s3.display();
		s3.design();
		
		
		
		
	}

}

