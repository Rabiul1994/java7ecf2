package com.jsp.Static_Non_StaticPack;

class Bike
{
	String color;
	int i;
	static int k=20;
	static {
		k=30;
	}
	public void changeColor(Bike b)
	{
		System.out.println("method 1 "+b.color);
		b.color="Red";
		b.i=32;
		k=24;
		System.out.println("method 2"+b.color);
		System.out.println(i);
		System.out.println(k);
	}
	
	public Bike(String c,int i)
	{
		color=c;
		this.i=i;
	}
}

public class ObjectArgument {

	public static void main(String[] args) 
	{
		Bike b1=new Bike("Black",22);
		System.out.println(b1.color);
		Bike b2=new Bike("Black",44);
		System.out.println(" hh "+Bike.k);
		b1.changeColor(b1);
		
		System.out.println(" ll "+Bike.k);
		
	}

}
