package com.jsp.Static_Non_StaticPack;

public class A {
	
	int i;
	static int j;
	
	public void change()
	{
		System.out.println(i++ +" "+ j++);
		
//		System.out.println(++i +" "+ ++j);
		
	}


	public static void main(String[] args) 
	{
		A obj1=new A();
		A obj2=new A();
		A obj3=new A();
		obj1.change();
		obj2.change();
		obj3.change();
		
		
	}

}
