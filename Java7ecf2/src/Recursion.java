
public class Recursion 
{
	public static void m(int a)
	{
		if(a==5) return;
		System.out.println(a);
		m(a+1);
		m(a+1);
	}

	public static void main(String[] args) {
		m(1);

	}

}
