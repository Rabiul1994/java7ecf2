

class Father
{
	int i;
	
	public Father(int i) {
		this.i = i;
	}

	public boolean equals(Object arg)
	{
//		Father f=(Father)arg;
		return this.i==((Father)arg).i;
		
		
	}
}



public class Tester {

	public static void main(String[] args) 
	{
		Father f1=new Father(10);
		Father f2=new Father(10);
		System.out.println(f1.equals(f2));
	}

}
