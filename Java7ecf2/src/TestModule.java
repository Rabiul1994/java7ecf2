
import java.util.Scanner;

class Welcome
{
	
	static {
		System.out.println("\t\t\t\tJSPIDERS  \n"
				+ "\n\t\t***INDIA'S LARGEST JAVA LEARNING PLATFORM**\n\n");

		
	}
	
	
	public void display()
	
	{
		System.out.println("JSpiders (Unit of Test Yantra Software Solutions (India) Pvt Ltd) is"
				+ "\nthe world’s ace Java development training organization with  an aim to bridge"
				+ "\nthe gap between the demands of the industry and the curriculumof educational"
				+ "\ninstitutions. With centers across the Globe,the institute is a platform where"
				+ "\nyoung minds are given the opportunity to build successful careers."
				+ "\n\n\n***JSpiders is a place where businesses find talent and dreams take flight.***");
	}
}

class CourseDetails
{
	public void viewCourseDetails()
	{
		Scanner sc = new Scanner(System.in);
		while(true) {
		System.out.println("Here are the courses that are being offered in JSPIDERS");
		System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = ");
		System.out.println("1. Java Full Stack");
		System.out.println("2. C C++ Programming");
		System.out.println("3. J2EE");
		System.out.println("4. MEAN Stack");
		System.out.println("5. MERN");
		System.out.println("6. Android Development");
		System.out.println("7. Web & Angular");
		System.out.println("8. Angular");
		System.out.println("9. Big data Hadoop");
		System.out.println("10. Aptitude");
		System.out.println("11. DevOps");
	
		
		
		System.err.println("Press the corresponding number on each course to know more");
		int ch = sc.nextInt();
		switch(ch)
		{
		case 1:
			System.err.println("Java Full Stack :");
			System.out.println("Java Full Stack course is more of job-oriented training which is designed "+"\n"
					+ "as per current industry standards."+"\n"
					+ "You can start your career in IT industry with basic and advance skills."+"\n" 
					+ "Course will be trained by top industry experts who have passion to train students "+"\n"
					+ "and help them get job in IT industry or excel in IT industry.");
			break;
		
		case 2:
			System.err.println("C C++ Programming :");
			System.out.println("This course will teach you the fundamentals of the C & C++ programming language "+"\n"
					+ "with a focus on Modern C & C++ features and the efficient use of tools. "+"\n"
					+ "Start with the fundamentals and work your way up to advanced C++11 and latest concepts and OOP mastery!");
			break;
			
		case 3:
			System.err.println("J2EE :");
			System.out.println("J2EE course is more of job-oriented training which is designed as per current industry standards."+"\n"
					+ "You can start your career in IT industry with basic and advance skills. "+"\n"
					+ "Course will be trained by top industry experts who have passion to train students "+"\n"
					+ "and help them get job in IT industry or excel in IT industry.");
			break;
			
		case 4 :
			System.err.println("MEAN Stack :");
			System.out.println("You will be ready to develop web application, mobile application & automation testing,"+"\n"
					+ " Web designing and full-fledged application developing by end of this training.");
			break;
			
		case 5 :
			System.err.println("MERN Stack :");
			System.out.println("Take your career as a web developer to the next level with this Full-Stack Web Developer Master’s Program, "+"\n"
					+ "where you’ll become an expert at front and back-end JavaScript technologies of the most popular MEAN "+"\n"
					+ "(Mongo DB, Express, Angular and Node.js) Stack or MERN (Mongo DB, Express, React and Node.js) Stack.");
			break;
		
		case 6 :
			System.err.println("Android Development :");
			System.out.println("This Android training course is designed to quickly get you up to speed how to make Android apps for Android devices."+"\n"
					+ " You will be able to write simple GUI applications,"+"\n"
					+ " use built-in widgets and components, work with the database to store data locally, "+"\n"
					+ "and much more by the end of this Android training course.");
			break;
			
		case 7 :	
			System.err.println("Web & Angular :");
			System.out.println("Web Angular course is more of job-oriented training which is designed as per current industry standards."+"\n"
					+ " You can start your career in IT industry with basic and advance skills."+"\n"
					+ " Course will be trained by top industry experts who have passion to train students and "+"\n"
					+ "help them get job in IT industry or excel in IT industry.");
			break;
			
		case 8:
			System.err.println("Angular :");
			System.out.println("Angular course is more of job-oriented training which is designed as per current industry standards. "+"\n"
					+ "You can start your career in IT industry with basic and advance skills. "+"\n"
					+ "Course will be trained by top industry experts who have passion to train students and "+"\n"
					+ "help them get job in IT industry or excel in IT industry.");
			break;
			
		case 9:
			System.err.println("Big data Hadoop :");
			System.out.println("Big data Analytics is the frontier of the IT field and has become crucial for almost every business."+"\n"
					+ " Big data can provide competitive edge advantages and decision making power to the business owners."+"\n"
					+ " Nowadays the ocean of opportunities is available for the big data professionals. "+"\n"
					+ "As there is an enormous amount of data available around the globe,"+"\n"
					+ " no one wants to miss any important or crucial data."+"\n"
					+ " In order to collect, preserve and analyze such a huge amount of data the business owners need a tool like Big data.");
			break;
		
		case 10 :
			System.err.println("Aptitude :");
			System.out.println("The test of general aptitude is the challenging first stage of assessment at almost all recruitment drives across sectors."+"\n"
					+ " It thus crucial for a prospective hire to pass the test to be able to progress further to the interview stages."+"\n"
					+ " The aptitude training program covers topics that are most commonly tested "+"\n"
					+ "while being adaptive to current trends in the area of aptitude testing.");
			break;
			
		case 11 :
			System.err.println("DevOps :");
			System.out.println("DevOps is not a tool or a team, it is the process or a methodology of using various tools"+"\n"
					+ " to solve the problems between Developers and Operations team, hence the term “Dev-Ops”. "+"\n"
					+ "The development team always had the pressure of completing the old, "+"\n"
					+ "pending work that was considered faulty by the operations team. "+"\n"
					+ "With DevOps, there is no wait time to deploy the code and getting it tested. "+"\n"
					+ "Hence, the developer gets instantaneous feedback on the code, and therefore can close the bugs, "+"\n"
					+ "and can make the code production ready faster!");
			break;
			
		default :
			System.out.println("Invalid choice..Please try again...");
			break;			
		}
		System.out.println("\n\nDo you want to check again?\nPress Y to proceed..\nPress N to exit");
		char c = sc.next().charAt(0);
		if(c=='y'||c=='Y') continue;
		else 
		{
			System.err.println("Thank You !!!!");
		    break;
		}	
		}
	}
}

class Locations
{
	Scanner sc=new Scanner(System.in);
	public void locateUs()
	{
		while(true) {
		System.out.println("Branch Locations:\n");
		System.out.println("1.Karnataka\n2.Chennai\n3.Pune\n4.Mumbai\n5.Hyderabad\n6.Others");
		System.out.print("Select location:");
		int n=sc.nextInt();
		
		if(n==1)
		{
			System.out.println("Location->\t    Karnataka\n");
			System.out.println("\t   Branch\t\t Contact\t\tAddress");
			System.out.println("\t__________________________________");
			System.out.println("\tBasavanagudi\t\t9686114422\t\t#13 &14, 3rd floor, Puttanna Chetty Complex,\n\t\t\t\t\t\t\tBull Temple Road, Opposite BSNL Office,\n\t\t\t\t\t\t\tBengaluru. 560004\n\tMarathalli\t\t9611100311"
					+ "\t\t#761/1, 3rd Floor, Outer Ring Road,\n\t\t\t\t\t\t\t next to Chaitanya Techno School,\n\t\t\t\t\t\t\t near KLM Mall, Marathahalli,\n\t\t\t\t\t\t\t Bengaluru, Karnataka-560037\n"
					+ "\tBanaswadi\t\t7760200900\t\t\n\tBTM Layout\t\t9980600900\n\tMysore\t\t\t9686198555");
		}
		else if(n==2)
		{
			System.out.println("Location->\t     Chennai\n");
			System.out.println("\t   Branch\t\t Contact");
			System.out.println("\t__________________________________");
			System.out.println("\tVadapalani\t\t9840611022\n\tChrompet\t\t9677263344\n"
					+ "\tVelachery\t\t6364884447");
		}
		else if(n==3)
		{
			System.out.println("Location->\t\tPune\n");
			System.out.println("\t   Branch\t\t Contact");
			System.out.println("\t__________________________________");
			System.out.println("\tDeccan Gymkhana\t\t9096055557\n\tHadapsar\t\t7028470008");
		}
		else if(n==4)
		{
			System.out.println("Location->\t\tMumbai\n");
			System.out.println("\t   Branch\t\t Contact");
			System.out.println("\t__________________________________");
			System.out.println("\t   Andheri\t\t7899770012");
		}
		else if(n==5)
		{
			System.out.println("Location->\t    Hyderabad\n");
			System.out.println("\t   Branch\t\t Contact");
			System.out.println("\t__________________________________");
			System.out.println("\tPunjagutta\t\t9108993971\n\tJNTU\t\t\t9980300600");
		}
		else if(n==6)
		{
			System.out.println("\n\t   Branch\t\t Contact");
			System.out.println("\t__________________________________");
			System.out.println("\t   Noida\t\t7042498886\n\t   Bhubaneswar\t\t7077702027\n"
					+ "\t   Bhopal\t\t6366110033");
		}
		else System.out.println("Select Between 1 to 6");
		System.out.println("\n\nDo you want to check again?\nPress Y to proceed.\nPress N to exit");
		char ch=sc.next().charAt(0);
		if(ch=='Y'||ch=='y') continue;
		else
		{
			System.out.println("Thank You...");
			break;
		}
	}
	}
}

class Registration
{
	Scanner sc=new Scanner(System.in);
	public void register()
	{
		System.out.print("Enter Full Name: ");
		String name=sc.nextLine();
		System.out.print("Enter e-mail: ");
		String email=sc.nextLine();
		System.out.print("Enter Mobile No.: ");
		long num=sc.nextLong();
		
		System.out.println("\nYour Details:\n"
				+"Name: "+name+"\n"
				+"e-mail: "+email+"\n"
				+"Mobile: "+num);
		System.out.println("\nThank You for registering with us..\nWe will contact soon");
	}
}

class PlacementInfo
{
	public void displayPlacement()
	{
		System.err.println("Students who got placed...");
		System.out.println();
		System.out.println("1. Name: Ampally Shiva prasad\r\n"
				           + "   Degree: BTech\r\n"
				           + "   Degree %: 56.00\r\n"
				           + "   YOP: 2019\r\n"
				           + "   Stream: Mechanical(ME)\r\n"
				           + "   Package : 7 LPA\n");
		
		System.out.println("2. Name: Pooja M Nikam\r\n"
				            + "   Degree: BE\r\n"
				            + "   Degree %: 58.00\r\n"
				            + "   YOP: 2019\r\n"
				            + "   Stream: Electronics & Communication(E&C)\r\n"
				            + "   Package : 6 LPA\n");
		
		System.out.println("3. Name: Vijay Hurali\r\n"
				            + "   Degree: BE\r\n"
				            + "   Degree %: 51.00\r\n"
				            + "   YOP: 2020\r\n"
				            + "   Stream: Electronics & Communication(E&C)\r\n"
				            + "   Package : 5.5 LPA\n");
		System.out.println("4. Name: Dheeraj Shah\r\n"
							+ "   Degree: BCA\r\n"
							+ "   Degree %: 56.00\r\n"
							+ "   YOP: 2021\r\n"
							+ "   Stream: BCA\r\n"
							+ "   Package : 4.5 LPA\n");
		System.out.println("5. Name: T.Vishal\r\n"
							+ "   Degree: BTech\r\n"
							+ "   Degree %: 57.70\r\n"
							+ "   YOP: 2018\r\n"
							+ "   Stream: Electronics & Communication(E&C)\r\n"
							+ "   Package : 9 LPA\n");
		System.out.println("And Many More......");
}
}

public class TestModule {

	public static void main(String[] args) 
	{
		Welcome j1=new Welcome();
		CourseDetails j2=new CourseDetails();
		Locations j3 = new Locations();
		Registration j4=new Registration();
		PlacementInfo j5=new PlacementInfo();
		
		for(; ;)
		{
		System.out.println("SELECT THE CHOICE FROM THE MENU BELOW:-"
				+ " \n\n 1.ABOUT US \n 2.COURSE DETAILS \n 3.LOCATIONS \n "
				+ "4.REGISTRATION \n 5.PLACEMENTS \n 6.RATINGS");
		Scanner scn= new Scanner(System.in);
		int choice = scn.nextInt();
		
		switch(choice)
		{
		
		
		case 1:{
			j1.display();
			break;
			}
		case 2:{
			j2.viewCourseDetails();
			break;
		}
		case 3:{
			j3.locateUs();
		}
		case 4:{
			j4.register();
			break;
		}
		case 5:{
			j5.displayPlacement();
			break;
		}
		case 6:
		{ 
			System.out.println("PLEASE RATE JSPIDERS OUT OF 5.\n");
			int n=scn.nextInt();
			
			for(int i=1;i<=n;i++) {
				System.out.print(" * ");
				
			}
	if(n<=2) System.out.println("\n :( Thank you!! We will investigate and try to make it perfect");
		
	else 	System.out.println(" :)\n\nYOU HAVE GIVEN US " + n + "* ratings \n\n THANK YOU...");
			
	}
		}
	System.out.println(" \n\nDo you want to continue?? \n Y-yes \n N-no");
	char c = scn.next().charAt(0);
	if(c=='y'||c=='Y')continue;
	else break;
	}
	}

}
