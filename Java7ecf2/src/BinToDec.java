import java.util.Scanner;

public class BinToDec {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Binary num:");
		int n=sc.nextInt();
		
		int dec=0;
		int i=1;
		while(n>0)
		{
			int mod=n%10;
			dec= dec+(mod*i);
			i*=2;
			n/=10;
		}
		System.out.println(dec);
	}

}
