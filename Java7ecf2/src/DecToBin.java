import java.util.Scanner;

public class DecToBin {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Decimal No:");
		int n=sc.nextInt();
		
		int bin=0;
		int i=1;
		while(n>0)
		{
			int mod=n%2;
			bin=bin+(mod*i);
			i*=10;
			n/=2;
		}
		System.out.println(bin);
	}

}
